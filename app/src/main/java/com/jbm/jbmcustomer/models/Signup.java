
package com.jbm.jbmcustomer.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Signup {
    @SerializedName("Response")
    @Expose
    private String response;

    @SerializedName("Status")
    @Expose
    private Integer status;

    @SerializedName("Id Number")
    @Expose
    private Integer idNumber;

    @SerializedName("OTP")
    @Expose
    private String oTP;



    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(Integer idNumber) {
        this.idNumber = idNumber;
    }

    public String getOTP() {
        return oTP;
    }

    public void setOTP(String oTP) {
        this.oTP = oTP;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
