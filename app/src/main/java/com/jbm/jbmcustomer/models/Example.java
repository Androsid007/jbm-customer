
package com.jbm.jbmcustomer.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Example{

    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("OTP")
    @Expose
    private Integer oTP;
    @SerializedName("Response")
    @Expose
    private String resposnse;
    @SerializedName("CardName")
    @Expose
    private String cardName;
    @SerializedName("Cardcode")
    @Expose
    private String cardcode;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getOTP() {
        return oTP;
    }

    public void setOTP(Integer oTP) {
        this.oTP = oTP;
    }

    public String getResposnse() {
        return resposnse;
    }

    public void setResposnse(String resposnse) {
        this.resposnse = resposnse;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCardcode() {
        return cardcode;
    }

    public void setCardcode(String cardcode) {
        this.cardcode = cardcode;
    }

}
