package com.jbm.jbmcustomer.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderHis {
      @SerializedName("Status")
        @Expose
        private Integer status;
        @SerializedName("Response")
        @Expose
        private String response;
        @SerializedName("Deatils")
        @Expose
        private List<OrderList> deatils = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public List<OrderList> getDeatils() {
        return deatils;
    }

    public void setDeatils(List<OrderList> deatils) {
        this.deatils = deatils;
    }

}
