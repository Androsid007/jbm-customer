package com.jbm.jbmcustomer.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hp on 1/5/2017.
 */

public class Login {


    @SerializedName("Response")
    @Expose
    private String response;

    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("CARDNAME")
    @Expose
    private String cardname;
    @SerializedName("Id Number")
    @Expose
    private String idNumber;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCardname() {
        return cardname;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }


    public void setCardname(String cardname) {
        this.cardname = cardname;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }
}
