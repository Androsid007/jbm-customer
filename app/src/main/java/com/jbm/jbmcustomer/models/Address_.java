package com.jbm.jbmcustomer.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Address_ {

    @SerializedName("CARDNAME")
    @Expose
    private String cARDNAME;
    @SerializedName("CardCode")
    @Expose
    private String cardCode;
    @SerializedName("Id Number")
    @Expose
    private String idNumber;
    @SerializedName("AdresType")
    @Expose
    private String adresType;
    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("NAme")
    @Expose
    private String nAme;
    @SerializedName("E_Mail")
    @Expose
    private String eMail;
    @SerializedName("Addr ID")
    @Expose
    private String addId;



    private boolean isSelected;
    private boolean mSelect;
    public boolean ismSelect() {
        return mSelect;
    }

    public void setmSelect(boolean select) {
        mSelect = select;
    }


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getAddId() {
        return addId;
    }

    public void setAddId(String addId) {
        this.addId = addId;
    }

    public String getCARDNAME() {
        return cARDNAME;
    }

    public void setCARDNAME(String cARDNAME) {
        this.cARDNAME = cARDNAME;
    }

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getAdresType() {
        return adresType;
    }

    public void setAdresType(String adresType) {
        this.adresType = adresType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNAme() {
        return nAme;
    }

    public void setNAme(String nAme) {
        this.nAme = nAme;
    }

    public String getEMail() {
        return eMail;
    }

    public void setEMail(String eMail) {
        this.eMail = eMail;
    }

}