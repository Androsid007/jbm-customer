package com.jbm.jbmcustomer.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImagesDetail {

    @SerializedName("ImageCode")
    @Expose
    private String imageCode;
    @SerializedName("Path")
    @Expose
    private String path;
    @SerializedName("Img_Cat")
    @Expose
    private String imgCat;

    public String getImageCode() {
        return imageCode;
    }

    public void setImageCode(String imageCode) {
        this.imageCode = imageCode;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getImgCat() {
        return imgCat;
    }

    public void setImgCat(String imgCat) {
        this.imgCat = imgCat;
    }

}