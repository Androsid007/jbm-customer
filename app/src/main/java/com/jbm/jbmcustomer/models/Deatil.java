package com.jbm.jbmcustomer.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Deatil {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("Item Code")
    @Expose
    private String itemCode;
    @SerializedName("Item Desc")
    @Expose
    private String itemDesc;
    @SerializedName("URL")
    @Expose
    private String uRL;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("CartQuantity")
    @Expose
    private String cartQuantity;
    @SerializedName("Net Amt")
    @Expose
    private String netAmt;
    @SerializedName("TaxCode")
    @Expose
    private String taxCode;
    @SerializedName("VatSumLine")
    @Expose
    private String vatSumLine;
    @SerializedName("VatSum")
    @Expose
    private String vatSum;

    public String getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(String basePrice) {
        this.basePrice = basePrice;
    }

    public String getuRL() {
        return uRL;
    }

    public void setuRL(String uRL) {
        this.uRL = uRL;
    }

    @SerializedName("Base Price")
    @Expose
    private String basePrice;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public String getURL() {
        return uRL;
    }

    public void setURL(String uRL) {
        this.uRL = uRL;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCartQuantity() {
        return cartQuantity;
    }

    public void setCartQuantity(String cartQuantity) {
        this.cartQuantity = cartQuantity;
    }

    public String getNetAmt() {
        return netAmt;
    }

    public void setNetAmt(String netAmt) {
        this.netAmt = netAmt;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getVatSumLine() {
        return vatSumLine;
    }

    public void setVatSumLine(String vatSumLine) {
        this.vatSumLine = vatSumLine;
    }

    public String getVatSum() {
        return vatSum;
    }

    public void setVatSum(String vatSum) {
        this.vatSum = vatSum;
    }

}