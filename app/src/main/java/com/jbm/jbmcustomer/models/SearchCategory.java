
package com.jbm.jbmcustomer.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchCategory{

    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("Response")
    @Expose
    private String response;
    @SerializedName("RowNumber")
    @Expose
    private Integer rowNumber;
    @SerializedName("ItmsGrpCod")
    @Expose
    private Integer itmsGrpCod;
    @SerializedName("ItmsGrpNam")
    @Expose
    private String itmsGrpNam;
    @SerializedName("URL")
    @Expose
    private String uRL;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Integer getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(Integer rowNumber) {
        this.rowNumber = rowNumber;
    }

    public Integer getItmsGrpCod() {
        return itmsGrpCod;
    }

    public void setItmsGrpCod(Integer itmsGrpCod) {
        this.itmsGrpCod = itmsGrpCod;
    }

    public String getItmsGrpNam() {
        return itmsGrpNam;
    }

    public void setItmsGrpNam(String itmsGrpNam) {
        this.itmsGrpNam = itmsGrpNam;
    }

    public String getURL() {
        return uRL;
    }

    public void setURL(String uRL) {
        this.uRL = uRL;
    }

}
