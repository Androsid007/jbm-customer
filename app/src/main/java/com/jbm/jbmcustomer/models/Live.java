package com.jbm.jbmcustomer.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 12/23/2016.
 */

public class Live {
    public String id;
    public String price;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Live live = (Live) o;

        if (id != null ? !id.equals(live.id) : live.id != null) return false;
        if (price != null ? !price.equals(live.price) : live.price != null) return false;
        if (title != null ? !title.equals(live.title) : live.title != null) return false;
        if (des != null ? !des.equals(live.des) : live.des != null) return false;
        if (genre != null ? !genre.equals(live.genre) : live.genre != null) return false;
        if (category_id != null ? !category_id.equals(live.category_id) : live.category_id != null)
            return false;
        if (category != null ? !category.equals(live.category) : live.category != null)
            return false;
        if (url != null ? !url.equals(live.url) : live.url != null) return false;
        if (type != null ? !type.equals(live.type) : live.type != null) return false;
        if (thumbnail != null ? !thumbnail.equals(live.thumbnail) : live.thumbnail != null)
            return false;
        if (_package != null ? !_package.equals(live._package) : live._package != null)
            return false;
        if (Subcriptions_ != null ? !Subcriptions_.equals(live.Subcriptions_) : live.Subcriptions_ != null)
            return false;
        if (package_info != null ? !package_info.equals(live.package_info) : live.package_info != null)
            return false;
        if (likes != null ? !likes.equals(live.likes) : live.likes != null) return false;
        if (rating != null ? !rating.equals(live.rating) : live.rating != null) return false;
        if (watch != null ? !watch.equals(live.watch) : live.watch != null) return false;
        return !(keywords != null ? !keywords.equals(live.keywords) : live.keywords != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (des != null ? des.hashCode() : 0);
        result = 31 * result + (genre != null ? genre.hashCode() : 0);
        result = 31 * result + (category_id != null ? category_id.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (thumbnail != null ? thumbnail.hashCode() : 0);
        result = 31 * result + (_package != null ? _package.hashCode() : 0);
        result = 31 * result + (Subcriptions_ != null ? Subcriptions_.hashCode() : 0);
        result = 31 * result + (package_info != null ? package_info.hashCode() : 0);
        result = 31 * result + (likes != null ? likes.hashCode() : 0);
        result = 31 * result + (rating != null ? rating.hashCode() : 0);
        result = 31 * result + (watch != null ? watch.hashCode() : 0);
        result = 31 * result + (keywords != null ? keywords.hashCode() : 0);
        return result;
    }

    public String title;
    public String des;
    public String genre;
    public String category_id;
    public String category;
    public String url;
    public String media_type;    public String type;

    public Thumbnail thumbnail;
    public String _package;
    public String Subcriptions_ ;
    public String package_info;
    public String likes;
    public String rating;
    public String watch;
    public List<String> keywords = new ArrayList<String>();

    public class Thumbnail {
        public String large;
        public String medium;
        public String small;
    }
}
