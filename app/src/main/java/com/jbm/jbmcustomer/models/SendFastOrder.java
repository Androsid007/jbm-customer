package com.jbm.jbmcustomer.models;

import java.util.List;

/**
 * Created by X-cellent on 20-Jan-17.
 */

public class SendFastOrder {
    private List<FastOrderModel> list;
    private String email;
    private String name;
    private String providerEmail;

    public SendFastOrder(List<FastOrderModel> list, String email, String name) {
        this.list = list;
        this.email = email;
        this.name = name;
    }

    public SendFastOrder(List<FastOrderModel> list, String email, String name, String providerEmail) {
        this.list = list;
        this.email = email;
        this.name = name;
        this.providerEmail = providerEmail;
    }

    public List<FastOrderModel> getList() {
        return list;
    }

    public void setList(List<FastOrderModel> list) {
        this.list = list;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProviderEmail() {
        return providerEmail;
    }

    public void setProviderEmail(String providerEmail) {
        this.providerEmail = providerEmail;
    }
}
