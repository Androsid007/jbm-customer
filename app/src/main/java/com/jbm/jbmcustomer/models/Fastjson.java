package com.jbm.jbmcustomer.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Fastjson {

    @SerializedName("articleNo")
    @Expose
    private String articleNo;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("article")
    @Expose
    private String article;

    public String getArticleNo() {
        return articleNo;
    }

    public void setArticleNo(String articleNo) {
        this.articleNo = articleNo;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

}