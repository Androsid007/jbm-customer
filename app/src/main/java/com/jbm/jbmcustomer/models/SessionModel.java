package com.jbm.jbmcustomer.models;

/**
 * Created by hp on 1/21/2017.
 */

public class SessionModel {

    String name , email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
