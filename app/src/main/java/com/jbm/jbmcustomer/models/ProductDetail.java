package com.jbm.jbmcustomer.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetail {

    @SerializedName("Status")
    @Expose
    private int status;
    @SerializedName("Response")
    @Expose
    private String response;
    @SerializedName("itemcode")
    @Expose
    private String itemcode;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("Stock")
    @Expose
    private double stock;
    @SerializedName("URL")
    @Expose
    private String uRL;
    @SerializedName("Images Detail")
    @Expose
    private List<ImagesDetail> imagesDetail = null;
    @SerializedName("product Boxing")
    @Expose
    private List<ProductBoxing> productBoxing = null;
    @SerializedName("Price")
    @Expose
    private List<Price> price = null;
    @SerializedName("Alternate Products")
    @Expose
    private List<AlternateProduct> alternateProducts = null;
    @SerializedName("Shipping")
    @Expose
    private List<Shipping> shipping = null;
    @SerializedName("Discount")
    @Expose
    private List<Discount> discount = null;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getStock() {
        return stock;
    }

    public void setStock(double stock) {
        this.stock = stock;
    }

    public String getURL() {
        return uRL;
    }

    public void setURL(String uRL) {
        this.uRL = uRL;
    }

    public List<ImagesDetail> getImagesDetail() {
        return imagesDetail;
    }

    public void setImagesDetail(List<ImagesDetail> imagesDetail) {
        this.imagesDetail = imagesDetail;
    }

    public List<ProductBoxing> getProductBoxing() {
        return productBoxing;
    }

    public void setProductBoxing(List<ProductBoxing> productBoxing) {
        this.productBoxing = productBoxing;
    }

    public List<Price> getPrice() {
        return price;
    }

    public void setPrice(List<Price> price) {
        this.price = price;
    }

    public List<AlternateProduct> getAlternateProducts() {
        return alternateProducts;
    }

    public void setAlternateProducts(List<AlternateProduct> alternateProducts) {
        this.alternateProducts = alternateProducts;
    }

    public List<Shipping> getShipping() {
        return shipping;
    }

    public void setShipping(List<Shipping> shipping) {
        this.shipping = shipping;
    }

    public List<Discount> getDiscount() {
        return discount;
    }

    public void setDiscount(List<Discount> discount) {
        this.discount = discount;
    }

}