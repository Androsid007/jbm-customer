package com.jbm.jbmcustomer.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Discount {

    @SerializedName("SP_Price")
    @Expose
    private String sPPrice;
    @SerializedName("SP_Disc")
    @Expose
    private String sPDisc;
    @SerializedName("Disc")
    @Expose
    private String disx;

    public String getSPPrice() {
        return sPPrice;
    }

    public void setSPPrice(String sPPrice) {
        this.sPPrice = sPPrice;
    }

    public String getSPDisc() {
        return sPDisc;
    }

    public void setSPDisc(String sPDisc) {
        this.sPDisc = sPDisc;
    }

    public String getDisx() {
        return disx;
    }

    public void setDisx(String disx) {
        this.disx = disx;
    }

}
