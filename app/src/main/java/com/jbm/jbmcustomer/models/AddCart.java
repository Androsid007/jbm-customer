package com.jbm.jbmcustomer.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddCart {

    @SerializedName("Status")
    @Expose
    private int status;
    @SerializedName("Response")
    @Expose
    private String response;
    @SerializedName("CartTotal")
    @Expose
    private String cartTotal;
    @SerializedName("CartQuantityTotal")
    @Expose
    private String cartQuantityTotal;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("MinFrtAmt")
    @Expose
    private String minFrtAmt;
    @SerializedName("ShipCost")
    @Expose
    private String shipCost;
    @SerializedName("Deatils")
    @Expose
    private List<Deatil> deatils = null;



    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getCartTotal() {
        return cartTotal;
    }

    public void setCartTotal(String cartTotal) {
        this.cartTotal = cartTotal;
    }

    public String getCartQuantityTotal() {
        return cartQuantityTotal;
    }

    public void setCartQuantityTotal(String cartQuantityTotal) {
        this.cartQuantityTotal = cartQuantityTotal;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMinFrtAmt() {
        return minFrtAmt;
    }

    public void setMinFrtAmt(String minFrtAmt) {
        this.minFrtAmt = minFrtAmt;
    }

    public String getShipCost() {
        return shipCost;
    }

    public void setShipCost(String shipCost) {
        this.shipCost = shipCost;
    }

    public List<Deatil> getDeatils() {
        return deatils;
    }

    public void setDeatils(List<Deatil> deatils) {
        this.deatils = deatils;
    }





}