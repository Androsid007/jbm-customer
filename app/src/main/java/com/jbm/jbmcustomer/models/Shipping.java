package com.jbm.jbmcustomer.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Shipping {

    @SerializedName("MinFrtAmt")
    @Expose
    private String minFrtAmt;
    @SerializedName("ShipCost")
    @Expose
    private String shipCost;

    public String getMinFrtAmt() {
        return minFrtAmt;
    }

    public void setMinFrtAmt(String minFrtAmt) {
        this.minFrtAmt = minFrtAmt;
    }

    public String getShipCost() {
        return shipCost;
    }

    public void setShipCost(String shipCost) {
        this.shipCost = shipCost;
    }

}
