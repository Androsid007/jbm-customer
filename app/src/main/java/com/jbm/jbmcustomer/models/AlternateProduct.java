package com.jbm.jbmcustomer.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AlternateProduct {

    @SerializedName("ITEMCODE")
    @Expose
    private String iTEMCODE;
    @SerializedName("ITEMNAME")
    @Expose
    private String iTEMNAME;
    @SerializedName("Path")
    @Expose
    private String path;

    public String getITEMCODE() {
        return iTEMCODE;
    }

    public void setITEMCODE(String iTEMCODE) {
        this.iTEMCODE = iTEMCODE;
    }

    public String getITEMNAME() {
        return iTEMNAME;
    }

    public void setITEMNAME(String iTEMNAME) {
        this.iTEMNAME = iTEMNAME;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}