package com.jbm.jbmcustomer.models;

/**
 * Created by X-cellent on 19-Jan-17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FastOrderModel {
    private String articleNo;
    private String qty;
    private String article;
    private String url;



    @SerializedName("Response")
    @Expose
    private String response;
    private Double stock;

    public FastOrderModel() {

    }

    public FastOrderModel(String articleNo, String qty, Double stock, String url) {
        this.articleNo = articleNo;
        this.qty = qty;
        this.stock = stock;
        this.url = url;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getArticleNo() {
        return articleNo;
    }

    public void setArticleNo(String articleNo) {
        this.articleNo = articleNo;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Double getStock() {
        return stock;
    }

    public void setStock(Double stock) {
        this.stock = stock;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}






