package com.jbm.jbmcustomer.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by hp on 1/27/2017.
 */

public class CartDetail {

    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("Response")
    @Expose
    private String response;
    @SerializedName("CartTotal")
    @Expose
    private String cartTotal;
    @SerializedName("CartQuantityTotal")
    @Expose
    private String cartQuantityTotal;
    @SerializedName("Email")
    @Expose
    private String email;

    @SerializedName("Address")
    @Expose
    private List<Cart> cart = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getCartTotal() {
        return cartTotal;
    }

    public void setCartTotal(String cartTotal) {
        this.cartTotal = cartTotal;
    }

    public String getCartQuantityTotal() {
        return cartQuantityTotal;
    }

    public void setCartQuantityTotal(String cartQuantityTotal) {
        this.cartQuantityTotal = cartQuantityTotal;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Cart> getCart() {
        return cart;
    }

    public void setCart(List<Cart> cart) {
        this.cart = cart;
    }



}
