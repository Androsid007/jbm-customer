package com.jbm.jbmcustomer.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderList {


    @SerializedName("DocNum")
    @Expose
    private String docNum;
    @SerializedName("DocDate")
    @Expose
    private String docDate;
    @SerializedName("TotalQty")
    @Expose
    private String totalQty;
    @SerializedName("TotalAmt")
    @Expose
    private String totalAmt;

    public String getOrdStatus() {
        return ordStatus;
    }

    public void setOrdStatus(String ordStatus) {
        this.ordStatus = ordStatus;
    }

    @SerializedName("OrdStatus")
    @Expose
    private String ordStatus;





    @SerializedName("Address")
    @Expose
    private String address;

    public String getDocNum() {
        return docNum;
    }

    public void setDocNum(String docNum) {
        this.docNum = docNum;
    }

    public String getDocDate() {
        return docDate;
    }

    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }

    public String getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(String totalQty) {
        this.totalQty = totalQty;
    }

    public String getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(String totalAmt) {
        this.totalAmt = totalAmt;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}