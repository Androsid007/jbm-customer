package com.jbm.jbmcustomer.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductBoxing {

    @SerializedName("QTY/KG")
    @Expose
    private String qTYKG;
    @SerializedName("QTY/SUBBOX")
    @Expose
    private String qTYSUBBOX;
    @SerializedName("QTY/BOX")
    @Expose
    private String qTYBOX;
    @SerializedName("QTY/PALLET")
    @Expose
    private String qTYPALLET;
    @SerializedName("QTY/CONTAINER")
    @Expose
    private String qTYCONTAINER;
    @SerializedName("Tax")
    @Expose
    private String tax;

    public String getQTYKG() {
        return qTYKG;
    }

    public void setQTYKG(String qTYKG) {
        this.qTYKG = qTYKG;
    }

    public String getQTYSUBBOX() {
        return qTYSUBBOX;
    }

    public void setQTYSUBBOX(String qTYSUBBOX) {
        this.qTYSUBBOX = qTYSUBBOX;
    }

    public String getQTYBOX() {
        return qTYBOX;
    }

    public void setQTYBOX(String qTYBOX) {
        this.qTYBOX = qTYBOX;
    }

    public String getQTYPALLET() {
        return qTYPALLET;
    }

    public void setQTYPALLET(String qTYPALLET) {
        this.qTYPALLET = qTYPALLET;
    }

    public String getQTYCONTAINER() {
        return qTYCONTAINER;
    }

    public void setQTYCONTAINER(String qTYCONTAINER) {
        this.qTYCONTAINER = qTYCONTAINER;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

}