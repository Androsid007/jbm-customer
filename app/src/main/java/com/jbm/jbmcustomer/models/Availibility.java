package com.jbm.jbmcustomer.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by X-cellent on 21-Jan-17.
 */

public class Availibility {
    @SerializedName("Stock")
    @Expose
    private Integer stock;

    @SerializedName("itemcode")
    @Expose
    private String itemcode;

    @SerializedName("Response")
    @Expose
    private String response;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }
}
