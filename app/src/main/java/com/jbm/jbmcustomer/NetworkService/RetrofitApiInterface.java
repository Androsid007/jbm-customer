package com.jbm.jbmcustomer.NetworkService;


import com.jbm.jbmcustomer.models.AddCart;
import com.jbm.jbmcustomer.models.Address;
import com.jbm.jbmcustomer.models.Category;
import com.jbm.jbmcustomer.models.Checkout;
import com.jbm.jbmcustomer.models.Example;
import com.jbm.jbmcustomer.models.FastOrderModel;
import com.jbm.jbmcustomer.models.Fastjson;
import com.jbm.jbmcustomer.models.Login;
import com.jbm.jbmcustomer.models.Nproduct;
import com.jbm.jbmcustomer.models.OrderDetails;
import com.jbm.jbmcustomer.models.OrderHis;
import com.jbm.jbmcustomer.models.Product;
import com.jbm.jbmcustomer.models.ProductDetail;
import com.jbm.jbmcustomer.models.Promotion;
import com.jbm.jbmcustomer.models.Signup;

import java.util.List;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Url;


public interface RetrofitApiInterface<M> {
    @GET
    Call<List<Example>> getForgetData(@Url String url);
    @GET
    Call<List<Signup>> getSignUpData(@Url String url);
    @GET
    Call<List<Login>> getLoginData(@Url String url);
    @GET
    Call<List<Login>> getResetPassword(@Url String url);
    @GET
    Call<List<Category>> getCategoryItem(@Url String url);
    @GET
    Call<List<Product>> getProductItem(@Url String url);
    @GET
    Call<List<Signup>> getOTP(@Url String url);
    @POST
    Call<ProductDetail> getProductDetail(@Url String url);
    @GET
    Call<List<Promotion>> getPromotion(@Url String url);
    @POST
    Call<AddCart> addCart(@Url String url);

    @POST
    Call<List<Nproduct>> getNewProduct(@Url String url);
    @POST
   Call<List<FastOrderModel>>  getFastOrder(@Body List<Fastjson> Fast, @Url String url);
    @POST
    Call<Address>  getAddress(@Url String url);
    @POST
    Call<OrderHis> getOrder(@Url String url);
    @POST
    Call<OrderDetails> getOrderDetails(@Url String url);
    @POST
    Call<AddCart>  getCart(@Url String url);

    @POST
    Call<Checkout>  getOrderPlaced(@Url String url);
    @POST
    Call<AddCart>  delItemCart(@Url String url);
    @POST
    Call<List<Category>> getSearchCategoryItem(@Url String url);
    @POST
    Call<List<Product>> getSearchProductItem(@Url String url);
}
