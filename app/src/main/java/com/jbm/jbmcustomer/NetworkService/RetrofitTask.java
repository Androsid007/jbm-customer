package com.jbm.jbmcustomer.NetworkService;

import android.content.Context;
import android.util.Log;

import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.URLConfig;
import com.jbm.jbmcustomer.models.Fastjson;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;




public class RetrofitTask<M> implements Callback<M> {


    private RetrofitTaskListener<M> _callback;
    private Context _context;

    private CommonUtility.HTTP_REQUEST_TYPE _methodType;
    private CommonUtility.CallerFunction _callerFunction;
    private  Object Fast;

    M genricList;

    private String URL;
    private Object postParamsEntity;
    long preExecutionTime, totalExecutionTime;
    boolean shouldAllowRetry = true;
    int timeOut;

    public RetrofitTask(RetrofitTaskListener<M> callback,
                        CommonUtility.HTTP_REQUEST_TYPE methodType, CommonUtility.CallerFunction callerFunction,
                        String url, Context context) {
        this._callback = callback;
        this._methodType = methodType;
        this.URL = url;
        _context = context;
        this._callerFunction = callerFunction;
        timeOut = CommonUtility.RETROFIT_TIMEOUT;
    }

    public RetrofitTask(RetrofitTaskListener<M> callback,
                        CommonUtility.HTTP_REQUEST_TYPE methodType, CommonUtility.CallerFunction callerFunction,
                        String url, Context context, Object postParamsEntity) {
        this._callback = callback;
        this._methodType = methodType;
        this.URL = url;
        timeOut = CommonUtility.RETROFIT_TIMEOUT;
        _context = context;
        this._callerFunction = callerFunction;
        this.postParamsEntity = postParamsEntity;
    }

    public void execute() {
        executeTask(timeOut);
    }


    private void executeTask(int timeOut) {

        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(timeOut, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(timeOut, TimeUnit.SECONDS);
        okHttpClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder().url(original.url());
                 preExecutionTime = System.currentTimeMillis();
                Request request = requestBuilder.build();
                Response response = chain.proceed(request);

                long totalExecutionTime = System.currentTimeMillis() - preExecutionTime;
                Log.i("okhttp", "preExecutionTime " + preExecutionTime);
                Log.i("okhttp", "postExecutionTime" + "" + System.currentTimeMillis() + "\ntotalExecutionTime " + totalExecutionTime + " ms");
                //GoogleAnalyticsConfig.createUserTimingEvent(_context, _callerFunction.name(), totalExecutionTime);
                return response;
            }
        });
        final Retrofit client = new Retrofit.Builder()
                .baseUrl(URLConfig.getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        RetrofitApiInterface service = client.create(RetrofitApiInterface.class);

        Call<M> call = null;
        switch (_callerFunction) {
            case GET_FORGET_DETAIL:
                call = service.getForgetData(URL);
                break;
            case GET_SIGN_UP:
                call = service.getSignUpData(URL);
                break;
            case GET_LOGIN_DETAIL:
                call = service.getLoginData(URL);
                break;
            case RESET_PASSWORD:
                call = service.getResetPassword(URL);
                break;
         case GET_CATEGORY_ITEM:
                call = service.getCategoryItem(URL);
             break;
            case PRODUCT_ITEM_MASTER:
                call = service.getProductItem(URL);
                break;
            case GET_OTP:
                call = service.getOTP(URL);
                break;
            case GET_PRODUCT_DETAIL:
                call = service.getProductDetail(URL);
                break;

            case PROMOTION_PRODUCT:
                call = service.getPromotion(URL);
                break;
            case NEW_PRODUCT:
                call = service.getNewProduct(URL);
                break;
            case FAST_ORDER:
                call = service.getFastOrder((List<Fastjson>) postParamsEntity, URL);
                break;

            case ADD_CART_FUNCTION:
                call = service.addCart(URL);
                break;
            case GET_ADDRESS:
                call = service.getAddress(URL);
                break;

            case ORDER_HISTORY:
                call = service.getOrder(URL);
                break;

            case GET_CART:
                call = service.getCart(URL);
                break;
            case CHECKOUT:
                call = service.getOrderPlaced(URL);
                break;
            case ORDER_DETAILS:
                call = service.getOrderDetails(URL);
                break;
            case DEL_ITEM_CART:
                call = service.delItemCart(URL);
                break;
            case GET_SEARCH_CATEGORY_ITEM:
                call = service.getSearchCategoryItem(URL);
                break;
            case PRODUCT_ITEM_SEARCH_MASTER:
                call = service.getSearchProductItem(URL);
                break;

        }
        call.enqueue(this);
    }

    @Override
    public void onResponse(retrofit.Response<M> response, Retrofit retrofit) {
        _callback.onRetrofitTaskComplete(response, _context, _callerFunction);
    }

    @Override
    public void onFailure(Throwable t) {

        _callback.onRetrofitTaskFailure(t);
    }
}