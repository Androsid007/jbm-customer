package com.jbm.jbmcustomer.application;

import android.app.Application;

/**
 * Created by hp on 12/23/2016.
 */

public class AppController extends Application {

    public static final String TAG = AppController.class
            .getSimpleName();
    private static AppController mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        /*Fabric.with(this, new Crashlytics());
        mInstance = this;
        // LeakCanary.install(this);
        MultiDex.install(this);
        ConnectionManager.getInstance(this).startConnectionTracking();*/
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

  /*  public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }*/

    /*public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        Log.e(TAG, "AppController.addToRequestQueue() " + req);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public CacheManager getCacheManager() {
        if (cacheManager == null) {
            try {
                File cacheFile = new File(getFilesDir() + File.separator + getPackageName());
                DiskCache diskCache = new DiskCache(cacheFile, BuildConfig.VERSION_CODE, 1024 * 1024 * 10);
                cacheManager = CacheManager.getInstance(diskCache);
            } catch (Exception e) {
                ExceptionUtils.printStacktrace(e);
            }
        }

        return this.cacheManager;
    }

    public FirebaseAnalytics getFirebaseInstance() {
        if (firebaseAnalytics == null)
            firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        return this.firebaseAnalytics;
    }*/

    /*private static RequestQueue prepareSerialRequestQueue(Context context) {
        Cache cache = new DiskBasedCache(context.getCacheDir(), MAX_CACHE_SIZE);
        Network network = getNetwork();
        return new RequestQueue(cache, network, MAX_SERIAL_THREAD_POOL_SIZE);
    }


    private static Network getNetwork() {
        HttpStack stack;
        String userAgent = "volley/0";
        if(Build.VERSION.SDK_INT >= 9) {
            stack = new HurlStack();
        } else {
            stack = new HttpClientStack(AndroidHttpClient.newInstance(userAgent));
        }
        return new BasicNetwork(stack);
    }*/
}
