package com.jbm.jbmcustomer.adapter;

/**
 * Created by hp1 on 1/18/2017.
 */

import android.app.FragmentTransaction;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.models.Promotion;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by hp on 1/5/2017.
 */

public class AdapterPromotion extends RecyclerView.Adapter<AdapterPromotion.ViewHolder> {
    private List<Promotion> promotionList;
    private FragmentTransaction fragmentTransaction;


    Context context;
    int layoutResourceId;

    public AdapterPromotion(Context context, int layoutResourceId, List<Promotion> newsList) {
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.promotionList = newsList;
    }

    @Override
    public AdapterPromotion.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_promotion, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterPromotion.ViewHolder viewHolder, int i) {

        viewHolder.tv_android.setText(promotionList.get(i).getArticalCode());
        viewHolder.descText.setText(promotionList.get(i).getPromoType());
        viewHolder.expDate.setText(promotionList.get(i).getExpiryDate()+" "+"Left");
        Picasso.with(context).load(promotionList.get(i).getImgPath()).resize(240, 120).into(viewHolder.img_android);



    }

    @Override
    public int getItemCount() {

        return promotionList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_android,descText,expDate;
        private ImageView img_android;
        private CardView cardView;

        public ViewHolder(View view) {
            super(view);

            tv_android = (TextView)view.findViewById(R.id.tvTitle);
            descText = (TextView)view.findViewById(R.id.desc_txt);
            expDate = (TextView)view.findViewById(R.id.expDate);
            cardView = (CardView)view.findViewById(R.id.card_view);
            img_android = (ImageView) view.findViewById(R.id.mealImage);
        }
    }

}
