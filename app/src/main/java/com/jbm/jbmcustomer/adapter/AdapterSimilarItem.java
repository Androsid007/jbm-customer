package com.jbm.jbmcustomer.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.activity.ProductActivity;
import com.jbm.jbmcustomer.activity.ProductDetailActivity;
import com.jbm.jbmcustomer.fragment.ProductFragement;
import com.jbm.jbmcustomer.models.AlternateProduct;
import com.jbm.jbmcustomer.models.Category;
import com.jbm.jbmcustomer.models.Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

;



/**
 * Created by hp on 1/5/2017.
 */

public class AdapterSimilarItem extends RecyclerView.Adapter {
    private List<AlternateProduct> alternateProducts=new ArrayList<AlternateProduct>();
    Context context;
    int layoutResourceId;



    public AdapterSimilarItem(Context context, int layoutResourceId, List<AlternateProduct> alternateProduct) {
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.alternateProducts = alternateProduct;
    }




    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
//        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_news_message_board_card, viewGroup, false);
//        return new ViewHolder(view);

        RecyclerView.ViewHolder vh;


            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_similar_item, viewGroup, false);
            vh = new ViewHolder(view);

        return vh;
    }



    @Override
    public int getItemViewType(int position) {
        return  alternateProducts.size();
        // return categoryList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {



        if (viewHolder instanceof ViewHolder) {

            ViewHolder viewHolder1=  ((ViewHolder) viewHolder);

            viewHolder1.tv_android.setText(alternateProducts.get(i).getITEMNAME());
            viewHolder1.tv_article_no.setText("Item Code :" + "" + String.valueOf(alternateProducts.get(i).getITEMCODE()));
            Picasso.with(context).load(alternateProducts.get(i).getPath()).resize(240, 120).into(viewHolder1.img_android);



        } else {
            ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {

        return alternateProducts.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_android, tv_price, tv_article_no;
        private ImageView img_android;
        private int pos;

        public ViewHolder(View view) {
            super(view);


            tv_android = (TextView) view.findViewById(R.id.tvTitle);
            tv_article_no = (TextView) view.findViewById(R.id.articletxt);
            img_android = (ImageView) view.findViewById(R.id.mealImage);


            img_android.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pos = getAdapterPosition();


                    String id = String.valueOf(alternateProducts.get(pos).getITEMCODE());
                    String name = String.valueOf(alternateProducts.get(pos).getITEMNAME());


                    Intent intent = (new Intent(context, ProductDetailActivity.class));
                    intent.putExtra("ItemCode", id);
                    intent.putExtra("ItemName", name);
                    context.startActivity(intent);


                }
            });
        }


    }
    class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            // super();
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }}}

