package com.jbm.jbmcustomer.adapter;

/**
 * Created by hp1 on 1/28/2017.
 */

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.models.OrderDetails;

/**
 * Created by hp1 on 1/27/2017.
 */

public class AdapterOrderDetail extends RecyclerView.Adapter<AdapterOrderDetail.ViewHolder> {
    private OrderDetails addCartList;


    Context context;
    int layoutResourceId;

    public AdapterOrderDetail(Context context, int layoutResourceId,OrderDetails addCartList) {
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.addCartList = addCartList;
    }

    @Override
    public AdapterOrderDetail.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_detail, viewGroup, false);
        return new AdapterOrderDetail.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterOrderDetail.ViewHolder viewHolder, int i) {

        viewHolder.tv_order.setText(addCartList.getDeatils().get(i).getItemCode());
        viewHolder.tv_doc.setText(addCartList.getDeatils().get(i).getColumn1());

        viewHolder.tv_amount.setText(addCartList.getDeatils().get(i).getPrice());
        viewHolder.tvQty.setText(addCartList.getDeatils().get(i).getQuantity());
        viewHolder.tv_android.setText(addCartList.getDeatils().get(i).getLinetotal());
     // viewHolder.tv_date.setText(addCartList.getStatus());




    }

    @Override
    public int getItemCount() {

        return addCartList.getDeatils().size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_android,tv_doc,tv_date,tv_order,tv_amount,tvQty;
        private ImageView img_android;
        private int pos;
        private CardView cardView;
        public ViewHolder(View view) {
            super(view);

            tv_android = (TextView)view.findViewById(R.id.tvTitle);
            cardView = (CardView)view.findViewById(R.id.card_view);
            tv_doc = (TextView)view.findViewById(R.id.tvDoc);
            tv_order = (TextView)view.findViewById(R.id.tvOrder);

            tv_amount = (TextView)view.findViewById(R.id.tvamount);

            tvQty = (TextView)view.findViewById(R.id.tvQty);

            tv_date = (TextView)view.findViewById(R.id.tvdocDate);


            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pos = getAdapterPosition();

                }
            });
        }
    }

}