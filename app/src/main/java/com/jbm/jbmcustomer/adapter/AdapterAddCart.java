package com.jbm.jbmcustomer.adapter;

/**
 * Created by hp1 on 1/11/2017.
 */

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.fragment.CartFragment;
import com.jbm.jbmcustomer.models.AddCart;
import com.squareup.picasso.Picasso;


public class AdapterAddCart extends RecyclerView.Adapter<AdapterAddCart.ViewHolder> {
    private AddCart addCartList;
    private CartFragment cartFragment ;
    private int flagPos;
    private String afterDiscount;
    Context context;
    int layoutResourceId;
     double discount;
    String cost;

    public AdapterAddCart(Context context, int layoutResourceId, AddCart addCarts,CartFragment cartFragment,int flag,double disc,String fcost) {
        this.context = context;
        this.cartFragment=cartFragment;
        this.layoutResourceId = layoutResourceId;
        this.addCartList = addCarts;
        flagPos = flag;
        this.discount=disc;
        cost=fcost;

    }

    @Override
    public AdapterAddCart.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_cart, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterAddCart.ViewHolder viewHolder, int i) {
        double amount = Double.parseDouble(addCartList.getDeatils().get(i).getPrice());

        //afterDiscount = String.format("%.2f", discountPrice);
        double finalPrice = (amount* Integer.parseInt(addCartList.getDeatils().get(i).getCartQuantity()));

        String dPrice = addCartList.getDeatils().get(i).getBasePrice();
        double baPrice = Double.parseDouble(dPrice);
        double disc = (100.00-(amount/baPrice)*100);
        String discou = String.format("%.0f", disc);

        if (discou.equals("0")){
            viewHolder.offText.setVisibility(View.GONE);
            viewHolder.priceTxt.setVisibility(View.GONE);
        }

        viewHolder.tv_android.setText(addCartList.getDeatils().get(i).getItemDesc());
        viewHolder.tv_art_number.setText(addCartList.getDeatils().get(i).getItemCode());
        viewHolder.qtyTxt.setText(addCartList.getDeatils().get(i).getCartQuantity());
        viewHolder.rateTxt.setText("Rate/Unit :"+" "+amount);
        viewHolder.offText.setText(discou+"%"+" "+"Off");
        viewHolder.priceTxt.setText("€"+""+String.format("%.2f", baPrice));
        viewHolder.finalPriceTxt.setText("€"+""+String.format("%.2f", finalPrice));
        if (addCartList.getDeatils().get(i).getURL()==""){
            viewHolder.img_android.setImageResource(R.mipmap.default_image);
        }else {

            Picasso.with(context).load(addCartList.getDeatils().get(i).getURL()).resize(240, 120).into(viewHolder.img_android);
        }


    }

    @Override
    public int getItemCount() {

        if (flagPos==2){
            return addCartList.getDeatils().size();
        }else if(flagPos==1) {
            return addCartList.getDeatils().size();
        }
        else{
            return addCartList.getDeatils().size();

        }
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_android,tv_art_number,tvQty,qtyTxt,rateTxt,offText,priceTxt,finalPriceTxt;
        private ImageView remove;;
        private ImageView img_android;
        private int pos;
        private CardView cardView;
        public ViewHolder(View view) {
            super(view);

            tv_android = (TextView)view.findViewById(R.id.tvTitle);
            rateTxt=(TextView)view.findViewById(R.id.rateTxt);
            tv_art_number=(TextView)view.findViewById(R.id.tvNumber);
            cardView = (CardView)view.findViewById(R.id.card_view);
            img_android = (ImageView) view.findViewById(R.id.mealImage);
            //tvQty=(TextView)view.findViewById(R.id.tvQty);
            remove=(ImageView)view.findViewById(R.id.removeItem);
            qtyTxt=(TextView)view.findViewById(R.id.qtyTxt);
            offText=(TextView)view.findViewById(R.id.offTxt);
            priceTxt=(TextView)view.findViewById(R.id.priceTxt);
            finalPriceTxt=(TextView)view.findViewById(R.id.finalPriceTxt);

           remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pos = getAdapterPosition();


                    new AlertDialog.Builder(context)
                            .setTitle("Delete ?")
                            .setMessage("Are you sure you want to delete?")
                            .setNegativeButton("No", null)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface arg0, int arg1) {
                                    cartFragment.delItemCart1(addCartList.getEmail(),addCartList,2,pos);
                                }
                            }).create().show();


                }
            });
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pos = getAdapterPosition();



                }
            });
        }
    }

}