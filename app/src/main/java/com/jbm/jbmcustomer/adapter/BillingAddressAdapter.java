package com.jbm.jbmcustomer.adapter;

/**
 * Created by hp1 on 1/11/2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.models.Address;

import java.util.List;


public class BillingAddressAdapter extends RecyclerView.Adapter<BillingAddressAdapter.ViewHolder> {
    private Address addressList;


    Context context;
    int layoutResourceId;

    private List<Address> mList;
    private RemoveItemCommunication mCommunicator;
    private Address address;

    public BillingAddressAdapter(Context context, int layoutResourceId, Address addressList) {
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.addressList = addressList;

    }

    @Override
    public BillingAddressAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_address_profile_layout, viewGroup, false);
        return new ViewHolder(view,mCommunicator);
    }

    @Override
    public void onBindViewHolder(BillingAddressAdapter.ViewHolder viewHolder, int i) {

        final int pos = i;

        String addType = addressList.getAddress().get(i).getAdresType();

        viewHolder.txt_address.setText("Address :"+" "+addressList.getAddress().get(i).getAddress());
        viewHolder.txt_card.setText("Card Name :"+" "+addressList.getAddress().get(i).getCARDNAME());
        viewHolder.txt_name.setText("Contact Person :"+" "+addressList.getAddress().get(i).getNAme());
        viewHolder.txt_email.setText("Email :"+" "+addressList.getAddress().get(i).getEMail());


    }

    @Override
    public int getItemCount() {

        return addressList.getAddress().size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txt_address,txt_card,txt_name,txt_email;

        RemoveItemCommunication mComminication;
        public ViewHolder(View view, RemoveItemCommunication Communicator) {
            super(view);

            txt_address = (TextView)view.findViewById(R.id.add_txt);
            txt_card = (TextView)view.findViewById(R.id.txt_card);
            txt_name = (TextView)view.findViewById(R.id.txt_name);
            txt_email = (TextView)view.findViewById(R.id.txt_email);

        }
    }

}