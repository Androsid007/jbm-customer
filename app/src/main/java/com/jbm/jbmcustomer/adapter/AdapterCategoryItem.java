package com.jbm.jbmcustomer.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.activity.ProductActivity;
import com.jbm.jbmcustomer.models.Category;
import com.squareup.picasso.Picasso;

import java.util.List;

;



/**
 * Created by hp on 1/5/2017.
 */

public class AdapterCategoryItem extends RecyclerView.Adapter {
    private List<Category> categoryList;
    Context context;
    int layoutResourceId;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    public void setLoadingMore(boolean loadingMore) {
        isLoadingMore = loadingMore;
    }

    boolean isLoadingMore = true ;

    public AdapterCategoryItem(Context context, int layoutResourceId, List<Category> categoryList) {
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.categoryList = categoryList;
    }




    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
//        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_news_message_board_card, viewGroup, false);
//        return new ViewHolder(view);

        RecyclerView.ViewHolder vh;

        if (viewType == VIEW_ITEM) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_adapter_home, viewGroup, false);
            vh = new ViewHolder(view);
        } else {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(
                    R.layout.progress_item, viewGroup, false);

            vh =  new ProgressViewHolder(v);
        }
        return vh;
    }



    @Override
    public int getItemViewType(int position) {
        return  categoryList.size() > position ? VIEW_ITEM : VIEW_PROG ;
     }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {



        if (viewHolder instanceof ViewHolder) {

            ViewHolder viewHolder1=  ((ViewHolder) viewHolder);

            viewHolder1.tv_android.setText(categoryList.get(i).getItmsGrpNam());
             Picasso.with(context).load(categoryList.get(i).getURL()).resize(240, 120).into(viewHolder1.img_android);



        } else {
            ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
        }
 }

    @Override
    public int getItemCount() {

        return isLoadingMore ? categoryList.size()+1 : categoryList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_android, tv_price, tv_article_no;
        private ImageView img_android;
        private int pos;

        public ViewHolder(View view) {
            super(view);


            tv_android = (TextView) view.findViewById(R.id.tvTitle);
            tv_article_no = (TextView) view.findViewById(R.id.articletxt);
            img_android = (ImageView) view.findViewById(R.id.mealImage);


            img_android.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pos = getAdapterPosition();


                    String id = String.valueOf(categoryList.get(pos).getItmsGrpCod());


                    Intent intent = (new Intent(context, ProductActivity.class));
                    intent.putExtra("categoryID", id);
                    context.startActivity(intent);


                }
            });
        }


    }
     class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
           // super();
             super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }}}

