package com.jbm.jbmcustomer.adapter;

/**
 * Created by hp1 on 1/11/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.activity.ProductDetailActivity;
import com.jbm.jbmcustomer.models.Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class AdapterProductSearchItem extends RecyclerView.Adapter {
    private List<Product> productList;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    Context context;
    int layoutResourceId;
    public void setLoadingMore(boolean loadingMore) {
        isLoadingMore = loadingMore;
    }

    boolean isLoadingMore = false ;


    public AdapterProductSearchItem(Context context, int layoutResourceId, List<Product> productList) {
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.productList = productList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder vh;

        if (viewType == VIEW_ITEM) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_news_message_board_card, viewGroup, false);
            vh = new AdapterProductSearchItem.ViewHolder(view);
        } else {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(
                    R.layout.progress_item, viewGroup, false);

            vh =  new AdapterProductSearchItem.ProgressViewHolder(v);
        }
        return vh;
    }
    @Override
    public int getItemViewType(int position) {
        return productList.size() > position ? VIEW_ITEM : VIEW_PROG ;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {


        if (viewHolder instanceof AdapterProductSearchItem.ViewHolder) {

            AdapterProductSearchItem.ViewHolder viewHolder1=  ((AdapterProductSearchItem.ViewHolder) viewHolder);

            viewHolder1.tv_android.setText(productList.get(i).getItemName());
            viewHolder1.tv_article.setText("Article No :"+""+String.valueOf(productList.get(i).getItemCode()));
            Picasso.with(context).load(productList.get(i).getURL()).resize(240, 120).into(viewHolder1.img_android);



        } else {
            ((AdapterProductSearchItem.ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
        }


    }

    public void setFilter(List<Product> countryModels) {
        productList = new ArrayList<>();
        productList.addAll(countryModels);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return isLoadingMore ? productList.size()+1 : productList.size();



    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_android,tv_price,tv_article;
        private ImageView img_android;
        private int pos;
        private CardView cardView;
        public ViewHolder(View view) {
            super(view);

            tv_android = (TextView)view.findViewById(R.id.tvTitle);
            tv_article = (TextView)view.findViewById(R.id.articletxt);
            cardView = (CardView)view.findViewById(R.id.card_view);
            img_android = (ImageView) view.findViewById(R.id.mealImage);

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pos = getAdapterPosition();
                    String code = String.valueOf(productList.get(pos).getItemCode());
                    String name = productList.get(pos).getItemName();
                    Intent intent = new Intent(context, ProductDetailActivity.class);
                    intent.putExtra("ItemCode", code);
                    intent.putExtra("ItemName",name);
                    intent.putExtra("fragment",3);
                    context.startActivity(intent);


                }
            });
        }
    }

    class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
             super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
           }}}


