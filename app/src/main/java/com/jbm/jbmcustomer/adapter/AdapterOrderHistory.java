package com.jbm.jbmcustomer.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.activity.OrderDetailsActivity;
import com.jbm.jbmcustomer.models.OrderHis;


/**
 * Created by hp1 on 1/27/2017.
 */

public class AdapterOrderHistory extends RecyclerView.Adapter<AdapterOrderHistory.ViewHolder> {
        private OrderHis addCartList;
    String email,orderId;


        Context context;
        int layoutResourceId;

        public AdapterOrderHistory(Context context, int layoutResourceId,OrderHis addCartList, String email) {
            this.context = context;
            this.layoutResourceId = layoutResourceId;
            this.addCartList = addCartList;
            this.email =email;

        }

        @Override
        public AdapterOrderHistory.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_order, viewGroup, false);
            return new AdapterOrderHistory.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(AdapterOrderHistory.ViewHolder viewHolder, int i) {

            viewHolder.tv_android.setText(addCartList.getDeatils().get(i).getAddress());
            viewHolder.tv_doc.setText(addCartList.getDeatils().get(i).getDocNum());
            if(addCartList.getDeatils().get(i).getOrdStatus().equals("O")) {
                viewHolder.tv_order.setText("Open");
            }else if(addCartList.getDeatils().get(i).getOrdStatus().equals("C"))
            {
                viewHolder.tv_order.setText("Close");
            }

            viewHolder.tv_amount.setText(addCartList.getDeatils().get(i).getTotalAmt());
            viewHolder.tvQty.setText(addCartList.getDeatils().get(i).getTotalQty());




        }

        @Override
        public int getItemCount() {

             return addCartList.getDeatils().size();
        }

        class ViewHolder extends RecyclerView.ViewHolder{
            private TextView tv_android,tv_doc,tv_date,tv_order,tv_amount,tvQty;
            private ImageView img_android;
            private int pos;
            private CardView cardView;
            public ViewHolder(View view) {
                super(view);

                tv_android = (TextView)view.findViewById(R.id.tvTitle);
                cardView = (CardView)view.findViewById(R.id.card_view);
                tv_doc = (TextView)view.findViewById(R.id.tvDoc);
                tv_order = (TextView)view.findViewById(R.id.tvOrder);

                tv_amount = (TextView)view.findViewById(R.id.tvamount);

                tvQty = (TextView)view.findViewById(R.id.tvQty);


                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        pos = getAdapterPosition();
                        orderId = addCartList.getDeatils().get(pos).getDocNum();
                        Intent intent = new Intent(context, OrderDetailsActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("email",email);
                        bundle.putString("doctype",orderId);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtras(bundle);
                        context.startActivity(intent);


                    }
                });
            }
        }




    }