package com.jbm.jbmcustomer.adapter;

/**
 * Created by hp1 on 1/11/2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.models.Address;

import java.util.List;


public class ShippingAddressAdapter extends RecyclerView.Adapter<ShippingAddressAdapter.ViewHolder> {
    private Address addressList;


    Context context;
    int layoutResourceId;
    private CheckBox lastChecked = null;
    private static int lastCheckedPos = 0;
    private int selectedPosition = 0;
    private List<Address> mList;
    private RemoveItemCommunication mCommunicator;
    private Address address;

    public ShippingAddressAdapter(Context context, int layoutResourceId, Address addressList) {
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.addressList = addressList;

    }

    @Override
    public ShippingAddressAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_address_layout, viewGroup, false);
        return new ViewHolder(view,mCommunicator);
    }

    @Override
    public void onBindViewHolder(ShippingAddressAdapter.ViewHolder viewHolder, int i) {

        final int pos = i;

        String addType = addressList.getAddress().get(i).getAdresType();

        viewHolder.txt_address.setText("Address :"+" "+addressList.getAddress().get(i).getAddress());
        viewHolder.txt_card.setText("Card Name :"+" "+addressList.getAddress().get(i).getCARDNAME());
        viewHolder.txt_name.setText("Contact Person :"+" "+addressList.getAddress().get(i).getNAme());
        viewHolder.txt_email.setText("Email :"+" "+addressList.getAddress().get(i).getEMail());

        viewHolder.chkSelected.setChecked(addressList.getAddress().get(i).isSelected());

        viewHolder.chkSelected.setTag(addressList.getAddress().get(i));

      viewHolder.chkSelected.setTag(i);
        if (i == selectedPosition) {
            viewHolder.chkSelected.setChecked(true);
        } else {

            viewHolder.chkSelected.setChecked(false);
        }
        if (selectedPosition>=0) {
            addressList.getAddress().get(selectedPosition).setmSelect(true);
        }
        viewHolder.chkSelected.setOnClickListener(onStateChangedListener(viewHolder.chkSelected, pos));




    }

    private View.OnClickListener onStateChangedListener(final CheckBox checkBox, final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()) {
                    selectedPosition = position;
                 } else {
                    selectedPosition = 0;
                }

                for (int i = 0; i < addressList.getAddress().size(); i++) {
                    addressList.getAddress().get(i).setmSelect(false);
                }

                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getItemCount() {

        return addressList.getAddress().size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txt_address,txt_card,txt_name,txt_email;
        public CheckBox chkSelected;

        RemoveItemCommunication mComminication;
        public ViewHolder(View view, RemoveItemCommunication Communicator) {
            super(view);

            txt_address = (TextView)view.findViewById(R.id.add_txt);
            txt_card = (TextView)view.findViewById(R.id.txt_card);
            txt_name = (TextView)view.findViewById(R.id.txt_name);
            txt_email = (TextView)view.findViewById(R.id.txt_email);
            chkSelected = (CheckBox)view.findViewById(R.id.checkbox);

        }
    }

}