package com.jbm.jbmcustomer.adapter;

/**
 * Created by hp on 1/28/2017.
 */

public interface RemoveItemCommunication {
    void removeCartItem(int position);
}
