package com.jbm.jbmcustomer.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.jbm.jbmcustomer.DatabaseHelper.FastOrderHelper;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.activity.ZoomImageActivity;
import com.jbm.jbmcustomer.models.FastOrderModel;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by X-cellent on 19-Jan-17.
 */

public class FastOrderAdapter extends RecyclerView.Adapter<FastOrderAdapter.MyViewHolder> {
    private List<FastOrderModel> list;
    private Context context;
    FastOrderHelper helper;
    private int pos;
    private String url;


    public FastOrderAdapter(Context context, List<FastOrderModel> list) {
        this.list = list;
        this.context = context;


    }

    public void delete(int position) {
        helper = new FastOrderHelper(context);
        pos = position;
        String del = list.get(pos).getArticleNo();
        Log.d("Adapter", " " + del);
        if (list.get(pos).getQty().equals("1")) {
            helper.deleteRow(del);
            list.remove(pos);
            notifyDataSetChanged();
        } else {
            String qty = list.get(pos).getQty();
            String quantity = String.valueOf(Integer.parseInt(qty) - 1);
            helper.updateRow(del, quantity,url);
            list.get(pos).setQty(quantity);
            notifyDataSetChanged();
        }
    }

    public void add(int position) {
        helper = new FastOrderHelper(context);

        String del = list.get(position).getArticleNo();
        Log.d("Adapter", " " + del);
       String qty = list.get(position).getQty();



        Double stock = list.get(position).getStock();
        Log.d("Stock"," "+stock);
       int i = stock.intValue();
        String stk = String.valueOf(i);
        Log.d("Stock"," "+stk);
        int availibility = Integer.parseInt(stk) - Integer.parseInt(qty);
        if (availibility > 0) {
            String quantity = String.valueOf(Integer.parseInt(qty) + 1);
            helper.updateRow(del, quantity,url);
            list.get(position).setQty(quantity);
            notifyDataSetChanged();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("You can add only " + i + " items")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //do things
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.fast_order_view, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final FastOrderModel model = list.get(position);
        if (model != null) {
            holder.showArticle.setText(model.getArticleNo());
            holder.showQty.setText(model.getQty());

                Picasso.with(context).load(model.getUrl()).resize(240, 120).into(holder.prodImg);

            holder.prodImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ZoomImageActivity.class);
                    intent.putExtra("imageUri",model.getUrl());
                    context.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView showArticle, showQty;
        Button delete, add;
        ImageView prodImg;


        public MyViewHolder(View itemView) {
            super(itemView);
            showArticle = (TextView) itemView.findViewById(R.id.showarticle);
            showQty = (TextView) itemView.findViewById(R.id.showqty);
            delete = (Button) itemView.findViewById(R.id.delete);
            prodImg = (ImageView)itemView.findViewById(R.id.prod_img);
            delete.setOnClickListener(this);
            add = (Button) itemView.findViewById(R.id.addmore);
            add.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == delete) {
                delete(getAdapterPosition());
              } else if (view == add) {
                add(getAdapterPosition());
            }
        }
    }
}
