package com.jbm.jbmcustomer.common;

public class ServerConfigStage {

    public static String GET_USER_INFO() {
        return "http://216.218.224.237/jbmapi/JBMForgetPwd.ashx?emailid=%s";
    }
    public static String GET_USER_SIGNUP() {
        return "http://216.218.224.237/jbmapi/JBMCustomerSignUP.ashx?emailid=%s&pwd=%s&name=%s&phone1=%s&phone2=%s&mobile=%s&fedralID=%s";
    }
    public static String USER_LOGIN(){
        return "http://216.218.224.237/jbmapi/logincustomer.ashx?emailid=%s&pwd=%s";
    }
    public static String RESET_PASSWORD(){
        return "http://216.218.224.237/jbmapi/CustomerResetPwd.ashx?emailid=%s&pwd=%s";
    }
    public static String ITEM_CATEGORY_API(){
        return "http://216.218.224.237/jbmapi/productcategory.ashx?pindex=%s&psize=%s&pcount=1";
    }
    public static String PRODUCT_MASTER_API(){
        return "http://216.218.224.237/jbmapi/PRODUCTMASTER.ASHX?pindex=%s&psize=%s&CatID=%s&PCOUNT=1&cid=%s";
    }
    public static String GET_OTP() {
        return "http://216.218.224.237/jbmapi/gettoken.ashx?emailid=%s&name=%s";
    }
    public static String GET_PRODUCT_DETAIL(){
        return "http://216.218.224.237/jbmapi/jbmproductdetail.ashx/?%s/%s";
    }
    public static String GET_PROMOTION(){
        return "http://216.218.224.237/jbmapi/customerpromoget.ashx";
    }

    public static String GET_NEW_PRODUCT(){
        return "http://216.218.224.237/jbmapi/jbmnewprodcust.ashx/?%s/20/1";
    }
    public static String GET_FAST_ORDER(){
        return "http://216.218.224.237/jbmapi/FastOrderCust.ashx/?%s/%s/%s";
        }

    public static String GET_ADDRESS(){
        return "http://216.218.224.237/jbmapi/jbmcustgetaddr.ashx/?%s";
    }


    public static String ADD_CART(){
        return "http://216.218.224.237/jbmapi/jbmcustaddcart.ashx/?%s/%s/%s/%s";
    }

    public static String ORDER_HISTORY(){
        return "http://216.218.224.237/jbmapi/JbmCustOrderHstryM.ashx/?%s";}

    public static String GET_CART(){
        return "http://216.218.224.237/jbmapi/JbmCustGetNotification.ashx/?%s";
    }

    public static String DEL_ITEM_CART(){
        return "http://216.218.224.237/jbmapi/jbmcartitmdel.ashx/?%s/%s";
    }

    public static String CHECKOUT(){
        return "http://216.218.224.237/jbmapi/JbmCustCheckOut.ashx/?%s/%s";

    }
    public static String GET_ORDER_DETAIL(){
        return "http://216.218.224.237/jbmapi/JbmCustOrderHstryD.ashx/?%s/%s";
           }

    public static String ITEM_SEARCH_CATEGORY_API(){
        return "http://216.218.224.237/jbmapi/JBMProdCatSerch.ashx/?%s/20/1/%s";
    }
    public static String PRODUCT_MASTER_SEARCH_API(){
        return "http://216.218.224.237/jbmapi/JBMProductSerchWise.ashx/?%s/10/%s/10/%s/%s";
    }
}