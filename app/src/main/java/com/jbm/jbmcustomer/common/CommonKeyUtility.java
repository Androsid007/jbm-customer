package com.jbm.jbmcustomer.common;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class CommonKeyUtility {

    //SharedPref keys
    public static final String HOTLINE_INIT = "hotlineInit";
    public static final String CHECK_ALREADY_LOGGED_IN = "alreadyLoggedIn";
    public static final String CHECK_LOGGEDIN_TYPE = "loginType";
    public static final String CHECK_SKIPPED_OR_ALREADY_LOGGED_IN = "skippedOrAlreadyLoggedIn";
    public static final String CHECK_FOR_AUTO_LIVE_STATUS_NOTIFICATION = "auto_status";
    public static final int LIVE_STATUS_NOTIFICATION_TAG_VALUE = 8;
    public static final int SHOWCASE_CART = 2;
    public static final int SHOWCASE_MENU = 1;
     public static int TRAFIC_ITEMS_LIMIT = 10;
    public static int RETROFIT_TIMEOUT = 30;
    public static long QGRAPH_ATTRIBUTION_WINDOW_CLICKED = 3600;//seconds
    public static int REQ_CODE_FOOD_MENU_CACHING = 24022016;
    public static int YATRACHEF_REDIRECTION_FROM_RY = 8;//ORDER_TYPE
    public static final String ACTION_NOTIFICATION_OPENED = "ACTION_NOTIFICATION_OPENED";
    public static final String YES_ACTION = "YES_ACTION";
    public static final String MAYBE_ACTION = "MAYBE_ACTION";
    public static final String APPTAG = "Trainenquiry Mobile";


    //Hotel citylist file name
    public static final String HOTEL_CITY_NAME_FILE="HOTEL_CITIES";
    public static final String HOTEL_EVENT_NAME_FILE="HOTEL_EVENT";
    public static final String HOTEL_LOCALITY_FILTER="HOTEL_LOCALITY";
    public static final String HOTEL_AMEINITY_FILTER="HOTEL_AMINITY";
    public static final String HOTEL_TEMPLE_FILTER="HOTEL_TEMPLE";
    public static final String HOTEL_CHECKIN="HOTEL_CHECKIN";
    public static final String HOTEL_CHECKOUT="HOTEL_CHECKOUT";
    public static final String HOTEL_ORDER_HISTORY="HOTEL_ORDER_HISTORY";
    public static final String HOTEL_BOOKING_CONFIRMATION="HOTEL_BOOKING_CONFIRMATION";
    public static final String HOTEL_FILTER="HOTEL_FILTER";
    public static final String HOTEL_MORE="HOTEL_MORE";
    public static final int START_PAGE_INDEX = 1;
    public static final int END_PAGE_INDEX = 50;
    public static final int HOTEL_ADULT_COUNT = 2;
    public static final int HOTEL_CHILD_COUNT = 0;
    public static final String TEMPLE_DISTANCE_FROM_HOTEL="%s km from %s";


    public static int Default_data_count = 3;
    public static final String ACTION_PUSH_RECEIVED = "ACTION_PUSH_RECEIVED";

    public static final String JSON_KEY_TRAIN_STATIONS = "route";
    public static final String JSON_KEY_TRAIN_STATIONS_CODE = "station_code";
    public static final String JSON_KEY_TRAIN_STATIONS_NAME = "station_name";
    public static final String JSON_KEY_TRAIN_STATIONS_STOP = "stop";
    public static final String JSON_KEY_TRAIN_STATIONS_lat = "lat";
    public static final String JSON_KEY_TRAIN_STATIONS_lng = "lng";
    public static final String JSON_KEY_WEBSERVICE_RESULT_STATUS = "status";
    public static final String JSON_KEY_WEBSERVICE_RESULT_ERROR = "error";
    public static final String NO_INTERNET_CONNECTION = "No Internet Connection";

 public static final String QUERY_FOR_lOCAL_SAVE_LIST = "select count(*) from CouponLootoKhoob where City=";
    //public static final String QUERY_GET_CouponLootoKhoob = "select couponId,couponCode,couponTitle,couponLongDescription,couponShortDescription,couponWebsite,imagesURL,couponCategory,categoryId,couponStartDate,,addedDate,couponEndtDate,couponIconURL,couponCity,companyName,companyLink,likes,favorites from CouponLootoKhoob";
    public static final String QUERY_GET_CouponLootoKhoob = "select * from CouponLootoKhoob";
    public static final String QUERY_FOR_ALLREADY_DATA_EXIST = "select * from CouponLootoKhoob where couponId=";





    public enum USER_LOGIN_TYPE {
        FACEBOOK(1), GMAIL(2);

        private int value;

        USER_LOGIN_TYPE(int v) {
            value = v;
        }

        public int getUserLoginValue() {
            return value;
        }
    }

    public enum TRANSITION_TYPE {
        NONE, ENTER, EXIT
    }

    public enum DEEPLINK_APP_TYPE {
        TIMETABLE, LIVETRAINSTATUS, GPS_TRAIN_LOCATOR
    }

    public static final String CONFIGURE_UPCOMING_JOURNEY = "configure_upcoming_journey";

    public enum PUSH_TYPE {
        NONE, TRIP_SHARE, Alert, URI_ALERT, SILENT, SEND_LOCATION, GET_MESSAGE, BUS_CITY_UPDATE, ADD_TO_CART,PULL_RECOMMENDED_BUS_ROUTES, UPDATE_TRAINS,
        UPDATE_STATIONS, UPDATE_CITY_STATIONS, HOTEL_CITY_UPDATE,HOME_PAGE_DATA_UPDATE
    }

    public enum HTTP_REQUEST_TYPE {
        GET, POST
    }

    public enum CAMPAGION {
        NONE, PAYTM
    }

    public enum WISDOM_ALERT_TYPE {
        search_url, numofAlert, Fid, ALERT_ID, stnCode, PUSH_ALERTS, SPECIALIZE_TAG, SPECIAL_TRAIN
    }

    public enum DB_QUERY_TYPE {
        GET_ALL_TICKETS, GET_FAVORITES, INSERT_FAV, DELETE_FAV, GET_TRAINS, INSERT_TRAIN,
        DELETE_TRAIN, GET_PNRS, INSERT_PNRS, DELETE_PNRS, INSERT_STATION, GET_STATION,
        DELETE_STATION, INSERT_HOMEFACTORY, INSERT_RECENT_SEARCH, GET_RECENT_SEARCH,
        GET_PAST_TRIPS, GET_FUTURE_TRIPS, INSERT_REGULAR_ROUTE, DELETE_DAILY_COMMUTER,
        INSERT_PASSENGER_DETAIL, INSERT_MESSAGE_PNR, INSERT_FACILITY_TYPE, CREATE_GROUP,
        UPDATESTATUS, CREATE_USER, APPROVE_MEMBERLIST, GET_TRIP, SAVED_TIMETABLE, CHECK_PNR_STATUS,
        UPDATE_UPCOMING_JOARNEY_DATA, RETREIVE_PNR_TRIP_DATA, DELETE_PNR_FROM_PASSENGER_DETAILS,
        INSERT_UPCOMING_JOARNEY, INSERT_USERS_LOCAL_ROUTE_DATA, INSERT_LOCAL_ROUTE_STATION,
        GET_STATION_SAVED_FOR_LOCAL_ROUTE_CITY,/*INSERT_USER_PROFILE_DATA,*/INSERT_LOCATION_SHARE,
        UPDATE_USER_CONFIGURED_JOURNEY_ADD_TINY_URL, CHECK_TINY_URL_IN_USER_CONFIGURED_JOURNEY,
        UPDATE_USER_CONFIGURED_JOURNEY_ADD_TRIP_COMPLETED, INSERT_METRO_ROUTE_STATION,

        GET_STATION_SAVED_FOR_METRO_ROUTE_CITY, INSERT_USERS_METRO_ROUTE_DATA, REMOVE_SAVED_TIMETABLE,
        REMOVE_LOCAL_TRAINLIST, REMOVE_TIME_TableLIST, SEARCH_TRAINS_FROM_LOCAL, INSERT_TRAINS_LOCAL_DB,

        SEARCH_STATIONS_FROM_LOCAL, INSERT_STATIONS_LOCAL_DB, INSERT_FOOD_ORDER, GET_FOOD_ORDERS, INSERT_FOOD_ORDER_DETAILS,
        GET_ALL_CART_ITEM, FETCH_NOTIFICATIONS, REMOVE_CART_ITEM, REMOVE_CART_ORDER, RETREIVE_WITHOUT_PNR_TRIP_DATA,
        GET_BUS_TRIP, GET_BUS_TRIP_PASSENGERS, SEARCH_STATIONS_FROM_LOCAL_WITH_CITY, GET_ALL_TEMP_CART_ITEM

    }


    public enum CallerFunction {

        GET_TRAIN_LIVE_INFO, GET_STATIONS, GET_USER_RELATED_WISDOMS, GET_SEARCHED_TRAINS,
        GET_WISDOMS, GET_WISDOMS1, GET_NEARBY_STNS, GET_UPCOMING_STNS, GET_TRAIN_SCHEDULE,
        GET_UPDATED_FEATURE, GET_ON_THE_GO, GET_AT_STATION, GET_WHERE_AM_I, GET_TimeTableSchedule,
        UPDATE_TIME_TABLE, GET_PNR_STATUS, GET_HOMEFACTORY, GET_PNR_MESSAGE_DATA,
        GET_WISDOMFACTORY, GET_TRAINS_BETWEEN_STATION, GET_RELATED_TRAINS, GET_UPCOMING_TRIPS,
        DAILY_COMMUTER, TRIP_SUMMARY, DELETE_DAILY_COMMUTER, TRIP_COACH_DETAIL,
        GET_SEAT_AVAILABILITY, GET_STATION_OPTION_FOR_FOOD_AVAILABILITY, GET_TRAFIC_DETAIL,
        CREATE_USER, GET_CALLBACK_FOOD_DATA, POST_CALLBACK_RESPONSE, GET_PUSH_NOTIFICATION_ALERT,
        SHARE_CONTENT, GET_PENDING_REQUEST, Request_for_OTP, USER_STATUS, DELETE_USER,
        POST_SAVE_TRIP_LOCATION, SUBMIT_REVIEW, GET_REVIEWS, GET_SEARCHED_TRAINS_NEW, GET_FARE,
        MARK_FAVORITES, GET_COACH_POSITION, GET_UPDATE_PLATFORM_DATA, GET_TAXI_FOR_CITY,
        LOCAL_TRAINS_CARD, LOCAL_TRAINS_HOME, GET_CITY_LIST, GET_SEARCHED_LOCAL_STATIONS,
        GET_LOCAL_TRAIN_ROUTE_SAVE_DATA, DELETE_LOCAL_TRAIN_ROUTE_DATA, GET_SHARE_LOCATION,
        POST_LOGIN_RESPONSE, GET_DELAYED_TRAINS, GET_SEARCHED_CITY,/*GET_LOGIN_DETAIL,*/
        GET_SHARED_JOURNEY_DETAIL, GET_MISS_CALL_NUMBER, GET_MISS_CALL_RESPONSE,
        GET_LOCAL_TRAIN_LIST, GET_TIMELINE_INSIGHTS, GET_ROUTE_DATA, PING_USER_FOR_LOCATION,
        GET_CITY_LIST_WITH_SERVICE, METRO_TRAINS_CARD, METRO_TIME_TABLE, GET_METRO_TRAIN_BETWEEN_STATION,
        GET_METRO_TRAIN_ROUTE_SAVE_DATA, DELETE_METRO_TRAIN_ROUTE_DATA, REFERRAL_URL_VERIFY,
        GET_NEWS_FEED, PAYTM_WALLET_BALANCE, SEND_LOCATION_TO_SERVER, GET_TRAINS_BETWEEN_STATION_DATA, GET_CAMPAIGN_STATUS, GET_USER_CITY_PREFERENCES,
        SEND_USER_CITY_PREFERENCES, GET_CITIES_BY_STATE_ID, INSERT_USER_ON_UI, POST_ERROR_LOG, GET_UPDATE_PLATFORM_DATA_NEW, CREATE_ALERT, GET_ALL_ALERTS,
        GET_SEAT_LAYOUT, GET_EVENT, GET_PANDALS_AT_STATION, VERIFY_PASSWORD_SERVER, GET_SECOND_EVENT,
        SEND_SOCIAL_LOGIN_DATA_TO_SERVER, PROCEED_TRANSACTION_CITRUS, BOOK_A_MEAL, GET_COUPON_LISTING,
        APPLY_COUPON, PLACE_FOOD_ORDER, FETCH_ORDER_HISTORY, PARTIAL_ORDER_CANCELLATION,
        GET_FOOD_MENU_LIST, GENERATE_TRANSACTION_SIGNATURE, CONFIRM_COD_ORDER, SEND_INVOICE_ON_EMAIL,
        GET_MEDICAL_EMERGENCY_FOR_TRAIN, GET_MEDICAL_EMERGENCY_FOR_STATION, GET_MEDICAL_EMERGENCY_FOR_PNR,
        GET_STATION_CITY_AMBULANCE, GET_NEAREST_STATION, POST_MEDICAL_EMERGENCY_CONTACT_NUM,
        GET_RY_WALLET, DO_PAYMENT_BY_WALLET, SEND_LANGUAGETAG_SERVER, SANCTUM_LIST_DATA, SANCTUM_ITEM_DATA, CURRENT_SEAT_AVAILABILITY,
        GET_BUS_SOURCE_CITIES, GET_BUS_DESTINATION_CITIES, GET_BUS_AVAILABLE_TRIPS, POST_BUS_FILTER_LIST, GET_BUS_TRIP_DETAILS, GET_STATUS_FOR_BLOCK, GET_ORDER_HISTORY_FOR_TICKET,
        POST_STATUS_FOR_BLOCK, GET_BUS_CONFIRM_TICKET, GET_BUS_TRIP_DETAIL, GET_BUS_CANCEL_TICKET, FOOD_REVIEW_LIST, ABOUT_RESTAURANT, GENERIC_USER_REVIEWS,
        GET_BUS_TICKET_CANCEL_POLICY, GET_RUSH_ALERTS_FOR_PNR, GET_PNR_PHONE_NUMBER_DELETION,
        ON_THE_GO, TRAIN_BETWEEN_STATIONS, GET_PNR_DATA, POST_ON_THE_GO_CAPTCHA, GET_PNR_RESPONSE_FROM_SERVER_POST_HTML, GET_UTILITY_DATA,
        TOP_FIVE_DESTINATION_FROM_STN, GET_SEAT_AVAILABILITY_FROM_SERVER_POST_HTML, FETCHING_LOADER_CONTENT, INFORM_FOR_INCOMPLETE_CART, ADD_ITEMS_TO_CART_FROM_SERVER,
        GET_RECOMMENDED_BUS_ROUTES, FETCH_TRIPS_FROM_SERVER, SEND_MONKEY_SERVEY_RESPONSE,
        SHOWCASEMENU_CART_PREPARATION,GET_BUS_EVENT_DETAIL,GET_CANCELLATION_FARE_DETAILS,
        HOTEL_LIST,HOTEL_ITEM_DETAIL,PROVISIONAL_HOTEL_BOOKING,HOTEL_REVIEWS,GET_HOTEL_CITIES,GET_HOTEL_CAL_EVENT,HOTEL_CONFIRMATION,
        GET_HOTEL_ORDER_HISTORY,INITIATE_HOTEL_CANCELLATION,GET_RECOMMENDED_BUS_ROUTES_AGAINST_JOURNEY, SETTING_CANCELLATION_FARE_ALARM,CANCEL_HOTEL_BOOKING,
        GET_STATIONS_WITH_CITY,GET_REWARD_HISTORY,GET_WALLET_SLIDER_IMAGES, SEND_PAYMENT_ERRORS,
        GET_BUS_CITIES_PREFERENCES, GET_NEAREST_BUS_CITY, POST_OFFLINE_BUS_ROUTES,METRO_CARD_LOCATION,HOTEL_LATEST_PRICE_LIST,GET_MEDICAL_EMERGENCY,CREATE_VIRTUAL_JOURNEY_FOR_FOOD




    }

    public enum HomeCardType {
        ALERT_UPDATE_COUNT, ALERT_NOT_FOUND, MY_NEATEST_STATION, RECENT_SEARCH_CARD,
        VERSION_CARD, ARRIVAL_DEPARTURE_CARD, IMAGE_WISDOM, SEASONS_CARD, RANDOM_STATION_CARD,
        SWIPE_CARD, UPCOMING_TRIP_CARD, DAILY_COMMUTER, TRIP_SUMMARY, SHARED_TRIP_SUMMARY,
        WHERE_AMI_SUMMARY, SHOW_GROUP_CARD, PENDINGREQUEST_CARD, POPUP_CARD, LOWEST_FOOD,
        FORCE_VERSION_UPDATE, TRIVANDRUM_CLEARNESS_CARD, FOOD_ORDER_CARD, TRIP_SUMMARY_ONE_DAY_BEFORE,
        CITY_FOOD_TEXI_CARD, LOCAL_TRAIN_HOME_CARD, LOCAL_TRAIN_CARD, SHARE_TINY_URL, RAILWAY_TIMELINE_CARDS,
        TRACK_YOUR_TRIP, METRO_TRAIN_HOME_CARD, NO_LOCATION_FOUND_CARD, RY_BULLETIN_CARD, APP_WIDGET_CARD, TEMPLE_CARD, SEARCH_CARD,
        BOOK_BUS_CARD, SLIDER_CARD, SHOWCASE_MENU_CARD, DYNAMIC_CARD

    }
    public enum PNRCardType {
        TRAIN_DETAILS_CARD, CURRENT_STATUS, BUS_ROUTES_CARD, BOOK_MEAL, SHARE_PNR_INFO, TEMPLE_CARD, TRAIN_PERFORMANCE_CARD, TRAIN_PROBABILITY_CARD, BUS_BOOKING_CARD,
        WISDOM_CARD, SMS_PNR_STATUS_CARD, MEDICAL_EMERGENCY_CARD, CANCELLATION_FARE_CARD
    }


    public enum WisdomCardType {
        WISDOMS_TYPE, SEARCH_STATION, THREE_NEATEST_STATION
    }

    public enum FoodType {MORNING_TEA, BREAKFAST, LUNCH, EVENING_TEA, DINNER}

    public enum GroupType {
        USER_GROUP
    }

    public enum RatingType {
        TRAVEL_RATING, FOOD_RATING
    }

    public enum StoreType {
        NUN, GOOGLE_PLAY, UC_WEB, SAMSUNG, GET_JAR, NOKIA_X, AMAZON_KINDLE, OPERA
    }

    public enum ShareType {
        TRIP, PNR_STATUS, TRAIN_STATUS
    }

    public enum TripType {
        SELF, SHARED
    }

    public enum MemberStatus {
        PENDING, APPROVE
    }

    public enum TripFacility {
        NONE,
        TAXI, TAXI_AT_SOURCE, TAXI_AT_DESTINATION
    }

    public enum LeadType {
        NONE,
        TAXI, FOOD
    }

    public enum PNR_SERVICE_METHOD_TYPE {
        SAVEPNR, GETPNRDATA, SAVEJOURNEYUPDATE
    }

    public enum Months {
        Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec
    }

    public enum savedType {
        TimeTable
    }

    public enum LocalSaveType {
        LOCAL, METRO
    }

    public enum LocalTrainType {
        TRAINS_CARD, TRAIN_CARD_IMAGE, LOCAL_TRAIN_TAXI_CARD, LOCAL_TRAIN_EVENTS_CARD, LOCAL_TRAIN_EVENTS_SECOND_CARD

    }

    public enum MetroTrainType {
        TRAINS_CARD, TRAIN_CARD_IMAGE, METRO_TRAIN_TAXI_CARD, METRO_TRAIN_EVENTS_CARD, METRO_TRAIN_EVENTS_SECOND_CARD

    }

    public enum PNR_SRC {
        IR, TOURISM, E_CATERING
    }

    public static final int MILLISECONDS_PER_SECOND = 1000;

    public static final int TIME_INTERVAL_30_SECONDS = 30;

    // The update interval
    public static final int UPDATE_INTERVAL_IN_SECONDS = 60;

    // Update interval in milliseconds
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = (MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS);

   /* public static int get_Data_Count() {
        int data_count;
        if (MainApplication.getTripLocationConfig() != null)
            data_count = MainApplication
                    .getTripLocationConfig().getDataCount();
        else
            data_count = Default_data_count;
        return data_count;
    }
*/
    public static final String FB_PACKAGE_NAME = "com.facebook.katana";
    public static final String GMAIL_PACKAGE_NAME = "com.google.android.gm";
    public static final String WHATSAPP_PACKAGE_NAME = "com.whatsapp";
    public static final String GOOGLE_PLUS_PACKAGE_NAME = "com.google.android.apps.plus";

    /**
     * For sharedPreferenceKeys
     */
    public static String fromOptionsMenuIRCTC = "fromOptionsMenuIRCTC";

    // for type of local and metro
    public static String Local = "local";
    public static String Metro = "metro";

    public enum PAYMENT_OPTIONS {
        ONLINE, COD, ONLINE_COD
    }

    public enum FOOD_TYPE {
        NON_VEG, VEG, ADD_ON_NON_VEG, ADD_ON_VEG
    }

    /* (1, 'pending'),
             (2, 'cancelled'),
             (3, 'failed'),
             (4, 'fake'),
             (5, 'completed'),
             (6, 'processing'),
             (7, 'cancelled by restaurant'),
             (8, 'cancelled by customer'),
             (9, 'failed by restaurant'),
             (10,'failed by customer');
     */
    public enum ORDER_STATUS {
        PENDING, CANCELLED, FAILED, FAKE, COMPLETED, PROCESSING, CANCELLED_BY_REST, CANCELLED_BY_USER, FAILED_BY_REST,
        FAILED_BY_USER, INCOMPLETE, DISPATCHED, DELIVERED
    }

    public enum MEAL_TYPE {
        BREAKFAST, LUNCH, DINNER
    }

    public enum REDUCTION_TYPE {
        NONE, ABSOLUTE, PERCENTAGE
    }

    public enum LANGUAGE_TYPE {
        ENLISH, HINDI, BENGALI, MARATHI, GUJRATI, URDU, PUNJABI
    }

    // random int for request code
    public static int PG_PAYMENT_REQUEST_CODE = 2;
    public static int SHOW_COUPONS_REQUEST_CODE = 5;
    public static int WALLET_PAYMENT_REQUEST_CODE = 6;
    public static int BOOK_A_MEAL_ADD_TRIP_REQUEST_CODE = 4;
    public static int CANCEL_ORDER = 111;

    public static int WALLET_PAYMENT_FAILED = -1;
    public static int WALLET_PAYMENT_CANCELLED = 0;
    public static int WALLET_PAYMENT_SUCCESS = 1;

    public static int MISSCALL_VERIFY = 3;

    public static int PG_PAYMENT_FAILED = -1;
    public static int PG_PAYMENT_CANCELLED = 0;
    public static int PG_PAYMENT_SUCCESS = 1;


    //request codes for Marshmellow permission request actions
    public static int LOCATION_REQCODE = 0;
    public static int CONTACTS_REQCODE = 1;
    public static int READ_SMS_REQCODE = 2;
    public static int CALLING_REQCODE = 3;
    public static int EXTERNAL_STORAGE_REQCODE = 4;
    public static int TELEPHONY_REQCODE = 5;
    public static String FOOD_TERMS_AND_CONDITIONS = "http://www.railyatri.in/order-food/terms-conditions";


    public enum CHOSEN_PAYMENT_OPTION {
        RY_WALLET, DEBIT, CREDIT, NETBANKING, CASH, CITRUS, PAYTM
    }

    public enum ECOMM_TYPE {
        FOOD, BUS, HOTEL
    }

    public static long MIN_TIME_FOR_LOADER = 3000;
    public static long TIME_FOR_HOTLINE_INIT = 90000;
    public static long TIMEOUT_VALUE_FOR_SA_RETRY = 15 * 1000;
    public static long TIMEOUT_VALUE_FOR_PNR_RETRY = 15 * 1000;


    public static final int BOOK_A_MEAL = 0;
    public static final int HOME_PAGE_SLIDER = 1;
    public static final int NOTIFICATION = 2;

    @IntDef({BOOK_A_MEAL, HOME_PAGE_SLIDER, NOTIFICATION})
    @Retention(RetentionPolicy.SOURCE)
    public @interface RestaurantProfileReferrer {
    }

    ;
}
