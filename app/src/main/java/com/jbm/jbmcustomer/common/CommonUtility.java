package com.jbm.jbmcustomer.common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


public class  CommonUtility {
    public enum CallerFunction {
        GET_LOGIN_DETAIL,GET_SIGN_UP,GET_FORGET_DETAIL,RESET_PASSWORD,GET_CATEGORY_ITEM,PRODUCT_ITEM_MASTER,GET_OTP,PROMOTION_PRODUCT,GET_PRODUCT_DETAIL,ADD_CART_FUNCTION
        ,NEW_PRODUCT,FAST_ORDER,GET_ADDRESS,GET_CART,CHECKOUT,ORDER_HISTORY,ORDER_DETAILS,DEL_ITEM_CART,GET_SEARCH_CATEGORY_ITEM,PRODUCT_ITEM_SEARCH_MASTER


    }
    public enum HTTP_REQUEST_TYPE{
        GET,POST
    }

    public static final int RETROFIT_TIMEOUT=30000;

    public static boolean isNetworkAvailable(Context _context) {
        ConnectivityManager cm = (ConnectivityManager) _context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }
}
