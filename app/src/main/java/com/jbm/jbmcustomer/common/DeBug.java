package com.jbm.jbmcustomer.common;

import android.support.design.BuildConfig;
import android.util.Log;


public class DeBug {
    static boolean toShowLog = BuildConfig.DEBUG;
    public static void showLog(String TAG,String Message){
        if(TAG!=null)
            if(toShowLog)
                Log.i(TAG, Message);
    }
}
