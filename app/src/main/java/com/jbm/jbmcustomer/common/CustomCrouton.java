package com.jbm.jbmcustomer.common;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jbm.jbmcustomer.R;


public class CustomCrouton {
    public static final int LENGTH_SHORT = 2000;
    public static final int LENGTH_LONG = 5000;

	public static final int ALERT = R.color.angry_red;
	public static final int INFO = R.color.dark_sky_blue;

	public static void showView(Activity activity, final View view){
	    Animation animation = AnimationUtils.loadAnimation(activity, R.anim.slide_out_down);
	    animation.setAnimationListener(new AnimationListener() {
	        @Override
	        public void onAnimationStart(Animation animation) {}

	        @Override
	        public void onAnimationRepeat(Animation animation) {}

	        @Override
	        public void onAnimationEnd(Animation animation) {
	        }
	    });

	    view.startAnimation(animation);
	}

	public static void hideView(Activity activity, final View view){
	    Animation animation = AnimationUtils.loadAnimation(activity, R.anim.slide_out_up);

	    animation.setAnimationListener(new AnimationListener() {
	        @Override
	        public void onAnimationStart(Animation animation) {}

	        @Override
	        public void onAnimationRepeat(Animation animation) {}

	        @Override
	        public void onAnimationEnd(Animation animation) {
	        	view.setVisibility(View.GONE);
	        }
	    });

	    view.startAnimation(animation);
	}

	public static void make(final Activity activity, String text, int color) {
		final LinearLayout linlyt = new LinearLayout(activity);

		linlyt.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		linlyt.setBackgroundColor(activity.getResources().getColor(color));
		linlyt.setOrientation(LinearLayout.HORIZONTAL);
		linlyt.setGravity(Gravity.CENTER);

		TextView tv1 = new TextView(activity);
		tv1.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		tv1.setTextColor(Color.WHITE);
		tv1.setText(text);
		tv1.setTextSize(14);
		tv1.setTypeface(Typeface.DEFAULT_BOLD);
		tv1.setGravity(Gravity.CENTER);
		tv1.setPadding(5, 5, 5, 5);
		linlyt.addView(tv1);

		final ViewGroup viewGroup = (ViewGroup) activity.getWindow().getDecorView().findViewById(android.R.id.content);
		viewGroup.addView(linlyt);

		showView(activity, linlyt);
		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
		  @Override
		  public void run() {
				hideView(activity, linlyt);
		  }
		}, LENGTH_LONG);

	}
}

