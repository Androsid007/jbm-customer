package com.jbm.jbmcustomer.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jbm.jbmcustomer.DatabaseHelper.SQLiteHandler;
import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.Login;
import com.jbm.jbmcustomer.sharedpreference.SessionManager;

import java.util.List;

import retrofit.Response;

/**
 * Created by hp1 on 1/5/2017.
 */

public class OtpScreen extends AppCompatActivity implements RetrofitTaskListener<List<Login>> {
int otpNumber;
    private String cardName,cardCord,password,confirmPassword,email;
    EditText otpVeryfy,pass,confirmPass;
    private Button verifyBtn,verifyBtn1;
    LinearLayout lyt;
    RelativeLayout ryt;
    private ProgressDialog progressDialog;
    private Gson gson = new Gson();
    private SessionManager session;
    private SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_screen);
        session = new SessionManager(OtpScreen.this);
        //this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

         session = new SessionManager(getApplicationContext());



        Bundle bundle = getIntent().getExtras();
        if (getIntent().hasExtra("otpNumber")) {
            otpNumber = bundle.getInt("otpNumber");
        }
        if (getIntent().hasExtra("email")) {
            email = bundle.getString("email");
        }
        if (getIntent().hasExtra("cardName")) {
            cardName = bundle.getString("cardName");
        }
        if (getIntent().hasExtra("cardCord")) {
            cardCord = bundle.getString("cardCord");
        }

        unique();

    }
    public void  unique() {
        otpVeryfy = (EditText) findViewById(R.id.otpVeryfy);
        verifyBtn = (Button) findViewById(R.id.verifyBtn);
        verifyBtn1 = (Button) findViewById(R.id.verifyBtn1);
        pass = (EditText)findViewById(R.id.password);
        confirmPass = (EditText)findViewById(R.id.confirm_password);
        lyt = (LinearLayout) findViewById(R.id.credential_layout);
        ryt = (RelativeLayout) findViewById(R.id.confirmLayout);
        verifyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                String otpText = otpVeryfy.getText().toString();

                if (otpText.equals("" + otpNumber))

                {
                    lyt.setVisibility(view.GONE);
                    ryt.setVisibility(view.VISIBLE);


                } else {
                    otpVeryfy.setError("wrong otp");

                }


            }
        });

        verifyBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 password = pass.getText().toString();
                confirmPassword = confirmPass.getText().toString();
                if (validate()){
                if (password.equals(confirmPassword)){
                    callLoginService(email,confirmPassword);

                }
                else
                {
                    Toast.makeText(OtpScreen.this,"Your password and confirmation password do not match.",Toast.LENGTH_LONG).show();
                }}
            }
        });
    }

    public boolean validate() {
        boolean valid = true;



            if (password.isEmpty()||password.length()<7){
                pass.setError("Please enter 8 characters Password");
                pass.requestFocus();
                valid = false;
            }else if(confirmPassword.isEmpty()||(confirmPassword.length()<7)){
                confirmPass.setError("Please enter same password");
                confirmPass.requestFocus();
                valid = false;
            }


         else {
                confirmPass.setError(null);
        }

        return valid;
    }

    public void callLoginService(String email, String confirmpassword ) {

        showProgreass();
        String url = String.format(ServerConfigStage.RESET_PASSWORD(),email,confirmpassword);
        RetrofitTask task = new RetrofitTask<List<Login>>(OtpScreen.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.RESET_PASSWORD, url, OtpScreen.this);
        task.execute();

    }


    public void showProgreass() {
        progressDialog = new ProgressDialog(OtpScreen.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    @Override
    public void onRetrofitTaskComplete(Response<List<Login>> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {
            ;
            if (response.body() != null) {
                if(response.body().size()>0) {
                    if(response.body().get(0).getStatus()>0)
                    {
                        String json = gson.toJson(response);
                        System.out.println(json);
                        logoutUser();
                        Toast.makeText(context, response.body().get(0).getResponse(), Toast.LENGTH_SHORT).show();

                    }
                    else {
                        String json = gson.toJson(response);
                        System.out.println(json);
                        Toast.makeText(context, response.body().get(0).getResponse(), Toast.LENGTH_SHORT).show();

                    }
                }

            }
        }

    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(OtpScreen.this,"Fail to load Data",Toast.LENGTH_LONG).show();

        finish();
    }
    @Override
    public void onBackPressed()
    {
        // d
        super.onBackPressed();
        Intent intent = new Intent(OtpScreen.this, LoginActivity.class);
        intent.putExtra("fragment",3);
        startActivity(intent);
        finish();

    }

    private void logoutUser() {
        session.setLogin(false);

        db.deleteUsers();

        // Launching the login activity
        Intent intent = new Intent(OtpScreen.this, LoginActivity.class);
        intent.putExtra("fragment",3);
        startActivity(intent);
        finish();
    }

    }












