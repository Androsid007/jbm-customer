package com.jbm.jbmcustomer.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.jbm.jbmcustomer.DatabaseHelper.SQLiteHandler;
import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.AddCart;
import com.jbm.jbmcustomer.models.SessionModel;
import com.jbm.jbmcustomer.sharedpreference.SessionManager;

import java.util.List;

import io.fabric.sdk.android.Fabric;
import retrofit.Response;

public class SplashActivity extends AppCompatActivity implements RetrofitTaskListener<AddCart> {

    private final int SPLASH_DISPLAY_LENGTH = 3000;
    private ProgressDialog progressDialog;
    private AddCart cartDetail;
    private SessionManager session;
    private int cart;
    private String email;
    private SharedPreferences sharedPreference;
    private SharedPreferences.Editor editor;

     @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);

        sharedPreference = getSharedPreferences("CART",0);
         editor = sharedPreference.edit();

        SQLiteHandler sqLiteHandler = new SQLiteHandler(SplashActivity.this);
        List<SessionModel> sessionModelList = sqLiteHandler.getUserDetails();

        for (SessionModel sm : sessionModelList){
            email = sm.getEmail();
        }

        session = new SessionManager(getApplicationContext());

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {

                if (isNetworkConnected()){
                    if (session.isLoggedIn()){
                   // callGetCart(email);
                        Intent mainIntent = new Intent(SplashActivity.this,MainActivity.class);
                        SplashActivity.this.startActivity(mainIntent);
                        SplashActivity.this.finish();

                }else {
                 Intent mainIntent = new Intent(SplashActivity.this,MainActivity.class);
                SplashActivity.this.startActivity(mainIntent);
                SplashActivity.this.finish();
            }
                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
                    builder.setMessage("Please check your internet connection")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    public void showProgreass(){
        progressDialog=new ProgressDialog(SplashActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    public void stopProgress(){
        if(progressDialog!=null && progressDialog.isShowing())
            progressDialog.cancel();
    }
    public void callGetCart(String email){
        showProgreass();
        String url = String.format(ServerConfigStage.GET_CART(),email);
        RetrofitTask task = new RetrofitTask<AddCart>(SplashActivity.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.GET_CART, url, SplashActivity.this);
        task.execute();


    }
    public static void save_Cart_To_Shared_Prefs(Context context, AddCart cartDetail) {
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(cartDetail);
        prefsEditor.putString("cartDetail", json);
        prefsEditor.commit();

    }

    @Override
    public void onRetrofitTaskComplete(Response<AddCart> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {
            if (response.body() != null) {
                if(response.body().getStatus()>0) {

                    cartDetail = response.body();

                    cart = response.body().getDeatils().size();

                    editor.putInt("CART_ITEMS",cart);
                    editor.commit();
                    save_Cart_To_Shared_Prefs(SplashActivity.this,cartDetail);



                    Intent mainIntent = new Intent(SplashActivity.this,MainActivity.class);
                    SplashActivity.this.startActivity(mainIntent);
                    SplashActivity.this.finish();




                }
                else {
                    editor.putInt("CART_ITEMS",0);
                    editor.commit();
                    Intent mainIntent = new Intent(SplashActivity.this,MainActivity.class);
                    SplashActivity.this.startActivity(mainIntent);
                    SplashActivity.this.finish();
                }
            }

        }
    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(SplashActivity.this,"Fail to load Data",Toast.LENGTH_LONG).show();

        finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
