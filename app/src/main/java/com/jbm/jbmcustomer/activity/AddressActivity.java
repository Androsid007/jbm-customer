package com.jbm.jbmcustomer.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import com.jbm.jbmcustomer.DatabaseHelper.SQLiteHandler;
import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.adapter.BillingAddressAdapter;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.Address;
import com.jbm.jbmcustomer.models.SessionModel;
import com.jbm.jbmcustomer.sharedpreference.SessionManager;

import java.util.List;

import retrofit.Response;

public class AddressActivity extends AppCompatActivity implements RetrofitTaskListener<Address> {

    ProgressDialog progressDialog;
    private RecyclerView shiprecyclerView;
    private BillingAddressAdapter billingAddressAdapter;
    private Address addressList;
    private  String email1,addressId;
    int pageIndex;
    private LinearLayoutManager shiplinearLayoutManager;
    private static final String STARTING_TEXT = "Four Buttons Bottom Navigation";
    private SessionManager session;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        session = new SessionManager(this);

        toolbar = (Toolbar)findViewById(R.id.tool_bar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Address");
        supportInvalidateOptionsMenu();
        toolbar.setTitleTextColor(Color.WHITE);

        pageIndex = 0;
        shiprecyclerView = (RecyclerView)findViewById(R.id.ship_recycler);

        shiplinearLayoutManager = new LinearLayoutManager(this);
        shiprecyclerView.setLayoutManager(shiplinearLayoutManager);
        SQLiteHandler sqLiteHandler = new SQLiteHandler(this);
        List<SessionModel> sessionModelList = sqLiteHandler.getUserDetails();

        for (SessionModel sm : sessionModelList){
            email1 = sm.getEmail();
        }

        callLoginService(email1);
    }

    public void sendAddId(){
     }

    public void callLoginService(String email){
        showProgreass();
        pageIndex = pageIndex + 1;
        String url = String.format(ServerConfigStage.GET_ADDRESS(),email);
        RetrofitTask task = new RetrofitTask<Address>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.GET_ADDRESS, url,this);
        task.execute();
        }
    public void showProgreass(){
        progressDialog=new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    public void stopProgress(){
        if(progressDialog!=null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    @Override
    public void onRetrofitTaskComplete(Response<Address> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        int i=0;
        if (response.isSuccess()) {
            if (response.body() != null) {
                if(response.body().getStatus()>0) {
                    addressList = response.body();
                    if (addressList.getAddress().size()==0){
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setMessage("No shipping address found.Please contact JBM")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent intent = new Intent(AddressActivity.this, HomeActivity.class);
                                        intent.putExtra("fragment",3);
                                        startActivity(intent);
                                        startActivity(intent);
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                    else
                        {
             if (billingAddressAdapter == null) {
                            billingAddressAdapter = new BillingAddressAdapter(context, R.layout.row_address_profile_layout, addressList);
                            shiprecyclerView.setAdapter(billingAddressAdapter);

                        }
                        billingAddressAdapter.notifyDataSetChanged();
      }
                }
                else {
       Toast.makeText(context, response.body().getResponse(), Toast.LENGTH_SHORT).show();
   }
            }

        }

    }


    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(AddressActivity.this,"Fail to load Data",Toast.LENGTH_LONG).show();
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(AddressActivity.this, Profile.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("position",3);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
