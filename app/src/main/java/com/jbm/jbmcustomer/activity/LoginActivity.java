package com.jbm.jbmcustomer.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jbm.jbmcustomer.DatabaseHelper.SQLiteHandler;
import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.Login;
import com.jbm.jbmcustomer.sharedpreference.SessionManager;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.Response;

public class LoginActivity extends AppCompatActivity implements RetrofitTaskListener<List<Login>> {

    private Button  login;
    private int randomPIN;
    private TextView register;
    private String PINString, inputEmail, inputPassword, hashPassword,userName;
    ProgressDialog progressDialog;
    private TextView forgetPassword;
    private EditText email, password;
    public static final String KEY_PASSWORD="password";
    private SessionManager session;
    private SQLiteHandler db;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private Gson gson = new Gson();
    private int posi;
    private Toolbar toolbar;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Pattern pattern;
    private Matcher matcher;
    private String blockCharacterSet = "@_-.qwertyuiopasdfghjklzxcvbnm1234567890QWERTYUIOPASDFGHJKLZXCVBNM";

    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && !blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        toolbar = (Toolbar)findViewById(R.id.tool_bar);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Login");
            supportInvalidateOptionsMenu();
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.home:
                            Intent profileIntent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(profileIntent);
                            finish();
                            return true;

                    }
                    return false;
                }
            });

        }
        session = new SessionManager(this);
        sharedPreferences = getSharedPreferences("NAME",0);
        editor = sharedPreferences.edit();

        db = new SQLiteHandler(getApplicationContext());

        session = new SessionManager(getApplicationContext());
        posi = getIntent().getExtras().getInt("fragment");

       if (session.isLoggedIn()) {
            posi = getIntent().getExtras().getInt("fragment");
            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
            intent.putExtra("fragment",posi);
            startActivity(intent);
            finish();
        }



        randomPIN = (int) (Math.random() * 9000) + 1000;
        PINString = String.valueOf(randomPIN);

        register = (TextView) findViewById(R.id.register_button);
        login = (Button) findViewById(R.id.send_otp);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        password.setFilters(new InputFilter[]{filter});


        forgetPassword = (TextView) findViewById(R.id.forgetPassword);

        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this, ForgetPassword.class);
                startActivity(i);


            }

        });
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                Intent i = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(i);

            }

        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                try {
                    inputEmail = email.getText().toString().trim();
                    inputPassword = password.getText().toString();
                    if (validate()) {
                        callLoginService(inputEmail, inputPassword);
                        Log.d("Password", hashPassword);
                    }else{
                     }

                } catch (Exception ex) {
                 }

                  }
        });
    }

    public boolean validate() {
        boolean valid = true;

        inputEmail = email.getText().toString().trim();
        inputPassword = password.getText().toString();

        if (inputEmail.isEmpty()||!android.util.Patterns.EMAIL_ADDRESS.matcher(inputEmail).matches() ) {
            email.setError("Please enter valid email id");
            email.requestFocus();

            valid = false;
        } else if (inputPassword.isEmpty()){
            password.setError("Please enter password");
            password.requestFocus();
            valid = false;
        } else if (!inputPassword.isEmpty()){
            if (!isValidPassword(inputPassword)){
                password.setError("Please enter valid password");
                password.requestFocus();
                valid = false;
            }else
            {
                valid = true;
            }
        }

      return valid;
    }

    public  boolean isValidPassword(final String password) {

          final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=\\S+$).{8,16}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    public void callLoginService(String matchEmail, String password ) {

        showProgreass();
        String url = String.format(ServerConfigStage.USER_LOGIN(),matchEmail,password);
        RetrofitTask task = new RetrofitTask<List<Login>>(LoginActivity.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.GET_LOGIN_DETAIL, url, LoginActivity.this);
        task.execute();

    }
    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

             StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public void onRetrofitTaskComplete(Response<List<Login>> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {

             if (response.body() != null) {
                if(response.body().size()>0) {

                    if (response.body().get(0).getStatus() > 0) {

                        session.setLogin(true);

                        String json = gson.toJson(response);
                        System.out.println(json);
                        String name = response.body().get(0).getCardname();
                        //String cardcode=response.body().get(0).get
                        String id = String.valueOf(response.body().get(0).getIdNumber());
                        Intent intent = new Intent(context, HomeActivity.class);
                        intent.putExtra("cardName", name);
                        intent.putExtra("idNumber",id );
                        intent.putExtra("fragment",posi);
                        startActivity(intent);
                        startActivity(intent);
                        finish();

                        editor.putString("USER_NAME",name);
                        editor.putString("ID_NUMBER",id);
                        //editor.putString("CARD_CODE",)
                        editor.commit();

                        db.addUser(name,inputEmail);
                        Toast.makeText(context, "Login Successful", Toast.LENGTH_SHORT).show();

                    }
                    else {

                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setMessage(response.body().get(0).getResponse())
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //do things
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();

                    }
                }


            }
        }

    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(LoginActivity.this,"Fail to load Data",Toast.LENGTH_LONG).show();

        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        Intent setIntent = new Intent(LoginActivity.this,MainActivity.class);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
    }




}
