package com.jbm.jbmcustomer.activity;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.gson.Gson;
import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.adapter.AdapterSimilarItem;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.fragment.CartFragment;
import com.jbm.jbmcustomer.models.AlternateProduct;
import com.jbm.jbmcustomer.models.ProductDetail;
import com.jbm.jbmcustomer.sharedpreference.SessionManager;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.util.ArrayList;
import java.util.List;

import retrofit.Response;

public class ProductDetailActivity extends AppCompatActivity implements RetrofitTaskListener<ProductDetail>, BaseSliderView.OnSliderClickListener {

    private String itemCode, itemName, qty, artCode, image_url,cost;
    private TextView code;
    private TextView price,afterDiscountPrice,off;
    private TextView name;
    private TextView stock,wtTxt,subBoxTxt,boxTxt,palletTxt,recommended;
    private TextView offTxt;
    private EditText quantity_txt;
    private Button addCart;
    private RelativeLayout relativeLayout;
    //private ImageView stockAvailibility;
    ProgressDialog progressDialog;
    Gson gson = new Gson();
    private ExpandableTextView productDescription;
    private FragmentTransaction fragmentTransaction;
    private FragmentManager fm;
    private SessionManager session;
    private Toolbar toolbar;
    private int position,discount,aftrDisc;
    private SliderLayout mDemoSlider;
    private String idNum,aftrDscntCost,uri;
    private SharedPreferences sharedPreferences;
    private double dP1;
    private RecyclerView similarRecycler;
    private AdapterSimilarItem adapterSimilarItem;
    private List<AlternateProduct> alternateProductList;
    private RelativeLayout offTxtLayout;
    private String selectedQty;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        mDemoSlider = (SliderLayout)findViewById(R.id.slider);

        sharedPreferences = getSharedPreferences("NAME",0);


        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Product Detail");
        supportInvalidateOptionsMenu();
        toolbar.setTitleTextColor(Color.WHITE);

        session = new SessionManager(getApplicationContext());

        similarRecycler= (RecyclerView)findViewById(R.id.similarRecycler);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(ProductDetailActivity.this, LinearLayoutManager.HORIZONTAL, false);
        similarRecycler.setLayoutManager(horizontalLayoutManagaer);

        if (session.isLoggedIn()){
            idNum = sharedPreferences.getString("ID_NUMBER","");
        }else{
            idNum = sharedPreferences.getString("ID_NUMBER","0");
        }


        itemCode = getIntent().getExtras().getString("ItemCode");
        itemName = getIntent().getExtras().getString("ItemName");

        code = (TextView) findViewById(R.id.item_code);
        price = (TextView) findViewById(R.id.actual_price_txt);
        name = (TextView) findViewById(R.id.product_name);
        stock = (TextView) findViewById(R.id.stock);
        quantity_txt = (EditText) findViewById(R.id.unit_number);
        wtTxt=(TextView)findViewById(R.id.weightText);
        subBoxTxt=(TextView)findViewById(R.id.subBoxTxt);
        boxTxt=(TextView)findViewById(R.id.boxTxt);
        palletTxt=(TextView)findViewById(R.id.palletTxt);
        offTxt=(TextView)findViewById(R.id.offTxt);
        afterDiscountPrice=(TextView)findViewById(R.id.price_txt);
        off=(TextView)findViewById(R.id.textview1);
        recommended=(TextView)findViewById(R.id.recommended);
        offTxtLayout = (RelativeLayout)findViewById(R.id.offTxtLayout);
        relativeLayout=(RelativeLayout)findViewById(R.id.relativeLayout);




        addCart = (Button) findViewById(R.id.btn_add_cart);



        callProductDetail(itemCode,idNum);

        addCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 selectedQty = quantity_txt.getText().toString();

                if (session.isLoggedIn()) {

                    if (!selectedQty.isEmpty() && Integer.parseInt(selectedQty) > Integer.parseInt(qty)) {
                       AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailActivity.this);
                        builder.setMessage("Product out of stocks.Delivery may be deleyed\n Do you wish to continue ?")
                                .setCancelable(false)
                                .setNegativeButton("No", null)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        hideKeyboard(ProductDetailActivity.this);
                                        fm = getSupportFragmentManager();
                                        fragmentTransaction = fm.beginTransaction();
                                        CartFragment cartFragment = new CartFragment();
                                        fragmentTransaction.replace(R.id.fragmentContainer,cartFragment,"fragment_tag_String");
                                        Bundle bundle = new Bundle();
                                        bundle.putString("artCode", artCode);
                                        bundle.putString("artName",itemName);
                                        bundle.putString("image",image_url);
                                        bundle.putString("quantity",selectedQty);
                                        bundle.putDouble("discount",dP1);
                                        bundle.putString("afterDiscount",aftrDscntCost);
                                        bundle.putString("cost",cost);
                                        bundle.putInt("fragment",6);
                                        cartFragment.setArguments(bundle);
                                        fragmentTransaction.addToBackStack(null).commit();
                                        //do things
                                    }
                                }

                                );

                        AlertDialog alert = builder.create();
                        alert.show();


                    } else if (selectedQty.isEmpty() || Integer.parseInt(selectedQty) == 0) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailActivity.this);
                        builder.setMessage("Please Add Quantity")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //do things
                                    }
                                });

                        AlertDialog alert = builder.create();
                        alert.show();

                    } else if (!selectedQty.isEmpty() && Integer.parseInt(selectedQty) != 0 && Integer.parseInt(selectedQty) <= Integer.parseInt(qty)) {
                        // addCart.setVisibility(View.GONE);
                        hideKeyboard(ProductDetailActivity.this);
                        fm = getSupportFragmentManager();
                        fragmentTransaction = fm.beginTransaction();
                        CartFragment cartFragment = new CartFragment();
                        fragmentTransaction.replace(R.id.fragmentContainer,cartFragment,"fragment_tag_String");
                        Bundle bundle = new Bundle();
                        bundle.putString("artCode", artCode);
                        bundle.putString("artName",itemName);
                        bundle.putString("image",image_url);
                        bundle.putString("quantity",selectedQty);
                        bundle.putDouble("discount",dP1);
                        bundle.putString("afterDiscount",aftrDscntCost);
                        bundle.putString("cost",cost);
                        bundle.putInt("fragment",6);
                        cartFragment.setArguments(bundle);
                        fragmentTransaction.addToBackStack(null).commit();
                    }
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailActivity.this);
                    builder.setMessage("Please Login To Continue")
                            .setCancelable(false)
                            .setNegativeButton("NOT NOW", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            })
                            .setPositiveButton("LOGIN", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    position = getIntent().getExtras().getInt("fragment");
                                    Intent intent = new Intent(ProductDetailActivity.this, LoginActivity.class);
                                    intent.putExtra("fragment", position);
                                    startActivity(intent);
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        });
    }




    public void showProgreass() {
        progressDialog = new ProgressDialog(ProductDetailActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    public void callProductDetail(String productId,String cid) {
        showProgreass();

        String url = String.format(ServerConfigStage.GET_PRODUCT_DETAIL(), productId,cid);
        RetrofitTask task = new RetrofitTask<ProductDetail>(ProductDetailActivity.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.GET_PRODUCT_DETAIL, url, ProductDetailActivity.this);
        task.execute();


    }

    @Override
    public void onRetrofitTaskComplete(Response<ProductDetail> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {
            if (response.body() != null) {
                if (response.body().getStatus() > 0) {
                    relativeLayout.setVisibility(View.VISIBLE);


                    String json = gson.toJson(response);
                    System.out.println(json);

                    artCode = response.body().getItemcode();
                    String price1 = response.body().getPrice().get(0).getPrice();
                    double amount = Double.parseDouble(price1);
                    cost = String.format("%.2f", amount);
                    String desc = response.body().getDescription();
                    image_url = response.body().getURL();
                    Double d = response.body().getStock();
                    Double aDouble = new Double(d);
                    int i = aDouble.intValue();
                    qty = String.valueOf(i);

                    if (qty.equals("0")) {
                        //stock_status.setText("Not In Stock");
                        //stockAvailibility.setImageResource(R.mipmap.stockunavailable);
                       // quantity_txt.setVisibility(View.GONE);
                        //qtyText.setVisibility(View.GONE);
                       // addCart.setVisibility(View.GONE);


                    } else {
                        //stockAvailibility.setImageResource(R.mipmap.stock_image);
                    }

                    String sP = response.body().getDiscount().get(0).getSPPrice();
                    double sP1= Double.parseDouble(sP);
                    String specialPrice = String.format("%.2f", sP1);
                    /*String sD = response.body().getDiscount().get(0).getSPDisc();
                    double sD1= Double.parseDouble(sD);
                    String specialDiscount = String.format("%.2f", sD1);*/
                    String dP = response.body().getDiscount().get(0).getDisx();
                    //int dp2= Integer.parseInt(dP);
                    dP1= Double.parseDouble(dP);
                    String discountPercent = String.format("%.0f", dP1);
                    if (discountPercent.equals("0")){
                        price.setVisibility(View.GONE);
                        offTxtLayout.setVisibility(View.GONE);
                    }
                    discount = Integer.parseInt(discountPercent);

                    double discountPrice = amount-(dP1/100)*amount;
                    aftrDscntCost = String.format("%.2f", discountPrice);


                    if (sP1!=0.00&&sP1<discountPrice){
                        offTxt.setText("Special Price");
                        offTxt.setTextSize(15);
                        afterDiscountPrice.setText("Euro" + " " +specialPrice);
                        off.setVisibility(View.GONE);
                    }else {
                        offTxt.setText(discountPercent+""+"%");
                        afterDiscountPrice.setText("Euro" + " " +aftrDscntCost);
                        off.setVisibility(View.VISIBLE);
                    }code.setText(artCode);
                    price.setText("Euro" + " " + cost);
                    productDescription = (ExpandableTextView) findViewById(R.id.expand_text_view);
                    productDescription.setText(desc);
                    name.setText(itemName);
                    if (qty.equals("0")) {
                        stock.setText("Available in 15 days");
                    }else{
                        stock.setText("In stock");

                    }
                    wtTxt.setText(response.body().getProductBoxing().get(0).getQTYKG());
                    subBoxTxt.setText(response.body().getProductBoxing().get(0).getQTYSUBBOX());
                    boxTxt.setText(response.body().getProductBoxing().get(0).getQTYBOX());
                    palletTxt.setText(response.body().getProductBoxing().get(0).getQTYPALLET());

                    if (response.body().getAlternateProducts().size()>0&&!response.body().getAlternateProducts().get(0).getITEMCODE().equals("0")){
                        alternateProductList = new ArrayList<>();
                        alternateProductList = response.body().getAlternateProducts();
                        adapterSimilarItem = new AdapterSimilarItem(ProductDetailActivity.this,R.layout.row_similar_item,alternateProductList);
                        similarRecycler.setAdapter(adapterSimilarItem);
                    }else{
                        recommended.setText("No Similar Items Found");
                    }

                    for (int j=0;j<response.body().getImagesDetail().size();j++){
                        TextSliderView textSliderView=new TextSliderView(this);
                        final String url = response.body().getImagesDetail().get(j).getPath();
                        uri=url;

                        if (url != null && !url.equals(null) && !url.equals("")) {
                            textSliderView
                                    .description(itemName)
                                    .image(url)
                                    .setScaleType(BaseSliderView.ScaleType.Fit)
                                    .setOnSliderClickListener(this);
                        } else {
                            textSliderView
                                    .description("No Product")
                                    .setScaleType(BaseSliderView.ScaleType.Fit)
                                    .setOnSliderClickListener(this);

                        }

                        mDemoSlider.addSlider(textSliderView);

                        textSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                            @Override
                            public void onSliderClick(BaseSliderView slider) {

                                if (!uri.equals("0")){

                                Intent intent = new Intent(ProductDetailActivity.this,ZoomImageActivity.class);
                                intent.putExtra("imageUri",uri);
                                startActivity(intent);
                                }else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailActivity.this);
                                    builder.setMessage("No image found for this item")
                                            .setCancelable(false)
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    //do things
                                                }
                                            });
                                    AlertDialog alert = builder.create();
                                    alert.show();
                                }

                            }
                        });
                    }




                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(response.body().getResponse())
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();


                }
            }

        }
    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        //Toast.makeText(ProductDetailActivity.this,"Fail to load Data",Toast.LENGTH_LONG).show();

        //finish();
    }

    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }


    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onBackPressed(){
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");
            fm.popBackStack();
            Fragment fragment = getSupportFragmentManager().findFragmentByTag("fragment_tag_String");
            if (fragment instanceof CartFragment) {
                toolbar.setTitle("Product Detail");
                Log.i("MainActivity", "Product backstack");
            }
        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here

                FragmentManager fm = getSupportFragmentManager();
                if (fm.getBackStackEntryCount() > 0) {
                    Log.i("MainActivity", "popping backstack");
                    fm.popBackStack();
                    Fragment fragment = getSupportFragmentManager().findFragmentByTag("fragment_tag_String");
                    if (fragment instanceof CartFragment) {
                        toolbar.setTitle("Product Detail");
                        Log.i("MainActivity", "Product backstack");
                    }
                } else{
                    finish();
                    return true;
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}