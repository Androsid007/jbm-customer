package com.jbm.jbmcustomer.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jbm.jbmcustomer.DatabaseHelper.SQLiteHandler;
import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.adapter.RemoveItemCommunication;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.Address;
import com.jbm.jbmcustomer.models.Checkout;
import com.jbm.jbmcustomer.models.SessionModel;
import com.jbm.jbmcustomer.sharedpreference.SessionManager;

import java.util.List;

import retrofit.Response;

public class OrderPlacedActivity extends AppCompatActivity implements RetrofitTaskListener<Checkout> {

    ProgressDialog progressDialog;
    private Address addressList;
    private  String email1,addressId,orderNo,name;
    private static final String STARTING_TEXT = "Four Buttons Bottom Navigation";
    private boolean mHasMenu;

    private RemoveItemCommunication communication;
    private SessionManager session;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private TextView orderPlacedTxt,orderNoTxt;
    private Button continueBtn;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_placed);

        toolbar = (Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Order");
        supportInvalidateOptionsMenu();
        toolbar.setTitleTextColor(Color.WHITE);
        orderPlacedTxt = (TextView)findViewById(R.id.orderPlacedTxt);
        continueBtn=(Button)findViewById(R.id.continueBtn);
        orderNoTxt = (TextView)findViewById(R.id.orderNoTxt);
        sharedPreferences = getSharedPreferences("CART",0);
        editor = sharedPreferences.edit();
        session = new SessionManager(this);
        addressId = getIntent().getExtras().getString("AddresID");
        SQLiteHandler sqLiteHandler = new SQLiteHandler(this);
        List<SessionModel> sessionModelList = sqLiteHandler.getUserDetails();
        for (SessionModel sm : sessionModelList){
            email1 = sm.getEmail();
            name = sm.getName();
        }
        getOrderPlaced(email1,addressId);
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OrderPlacedActivity.this, HomeActivity.class);
                intent.putExtra("fragment",3);
                startActivity(intent);
            }
        });
    }
        public void getOrderPlaced(String email,String addId){
        showProgreass();
        String url = String.format(ServerConfigStage.CHECKOUT(),email,addId);
        RetrofitTask task = new RetrofitTask<Checkout>(OrderPlacedActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.CHECKOUT, url,OrderPlacedActivity.this);
        task.execute();
        }
   public void showProgreass(){
        progressDialog=new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    public void stopProgress(){
        if(progressDialog!=null && progressDialog.isShowing())
        progressDialog.cancel();
    }

    @Override
    public void onRetrofitTaskComplete(Response<Checkout> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        int i=0;
        if (response.isSuccess()) {
            if (response.body() != null) {
                if(response.body().getStatus()==0) {
                    orderNo  = response.body().getAppOrderNo();
                    editor.putInt("CART_ITEMS",i);
                    editor.commit();
                    Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
                    orderPlacedTxt.setText("Hello"+" "+name+" "+"Thank you for your order.We'll send a confirmation when your order processed.We hope to see you again soon.Thanks");
                    orderNoTxt.setText("Your orderId is :"+" "+orderNo);

                }
                else {
                    orderPlacedTxt.setText(response.body().getResponse());
                    Toast.makeText(context, response.body().getResponse(), Toast.LENGTH_SHORT).show();
                     }
            }
    }
  }
    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(this,"Fail to load Data",Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        Intent setIntent = new Intent(OrderPlacedActivity.this,HomeActivity.class);
        setIntent.putExtra("fragment",3);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                Intent intent = new Intent(OrderPlacedActivity.this, HomeActivity.class);
                intent.putExtra("fragment",3);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;
                default:
                return super.onOptionsItemSelected(item);
        }
    }
}
