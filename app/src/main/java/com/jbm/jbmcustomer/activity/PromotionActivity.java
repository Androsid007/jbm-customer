package com.jbm.jbmcustomer.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.adapter.AdapterPromotion;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.Promotion;

import java.util.ArrayList;
import java.util.List;

import retrofit.Response;

public class PromotionActivity extends AppCompatActivity implements RetrofitTaskListener<List<Promotion>> {

    ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    AdapterPromotion adapter;
    List<Promotion> categoryList = new ArrayList<Promotion>();
    private static final String STARTING_TEXT = "Four Buttons Bottom Navigation";
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion);

        toolbar = (Toolbar)findViewById(R.id.tool_bar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Promotions");
            supportInvalidateOptionsMenu();
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.profile:
                            Intent profileIntent = new Intent(PromotionActivity.this, Profile.class);
                            profileIntent.putExtra("position",10);
                            startActivity(profileIntent);
                            finish();
                            return true;

                    }
                    return false;
                }
            });

        recyclerView = (RecyclerView)findViewById(R.id.card_recycler_view);



        initViews();
        callLoginService();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                // todo: goto back activity from here

                Intent intent = new Intent(PromotionActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);


    }

    private void initViews() {
    }

    public void callLoginService() {
        showProgreass();

        String url = String.format(ServerConfigStage.GET_PROMOTION());
        RetrofitTask task = new RetrofitTask<List<Promotion>>(this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.PROMOTION_PRODUCT, url, this);
        task.execute();
    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    @Override
    public void onRetrofitTaskComplete(Response<List<Promotion>> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {
            if (response.body() != null) {
                if (response.body().get(0).getStatus() > 0) {
                        categoryList.addAll(response.body());
                        if (adapter == null) {
                        adapter = new AdapterPromotion(context, R.layout.row_promotion, categoryList);
                        recyclerView.setAdapter(adapter);
                        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                        recyclerView.setLayoutManager(layoutManager);
                    }
                  adapter.notifyDataSetChanged();
                } else {
                  Toast.makeText(context, response.body().get(0).getResponse(), Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                Toast.makeText(context, "No Product Found", Toast.LENGTH_SHORT).show();
                finish();

            }
        }
    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(this, "Check Internet connection", Toast.LENGTH_LONG).show();
        finish();
    }


}
