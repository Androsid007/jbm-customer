package com.jbm.jbmcustomer.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jbm.jbmcustomer.R;

import java.util.Objects;

public class OtpActivity extends AppCompatActivity {

    private String systemOtp,userOtp;
    private EditText etOtp;
    private Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        etOtp = (EditText)findViewById(R.id.etOtp);
        login = (Button)findViewById(R.id.btnLogin);

        systemOtp = getIntent().getExtras().getString("OTP");


        login.setOnClickListener(new View.OnClickListener() {

            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                userOtp = etOtp.getText().toString();
                if (Objects.equals(userOtp, systemOtp)){
                    Toast.makeText(OtpActivity.this,"Login Successfull",Toast.LENGTH_SHORT).show();
                }

                else{
                    Toast.makeText(OtpActivity.this,"Invalid OTP",Toast.LENGTH_SHORT).show();
                }
            }
        });




    }
    @Override
    public void onBackPressed()
    {
        // d
        super.onBackPressed();
        Intent intent = new Intent(OtpActivity.this, LoginActivity.class);
        intent.putExtra("fragment",3);

        startActivity(intent);
        finish();

    }
}
