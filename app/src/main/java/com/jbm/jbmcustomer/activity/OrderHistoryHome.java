package com.jbm.jbmcustomer.activity;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.jbm.jbmcustomer.DatabaseHelper.SQLiteHandler;
import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.adapter.AdapterOrderHistory;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.OrderHis;
import com.jbm.jbmcustomer.models.SessionModel;
import com.jbm.jbmcustomer.sharedpreference.SessionManager;

import java.util.List;

import retrofit.Response;


public class OrderHistoryHome extends AppCompatActivity implements RetrofitTaskListener<OrderHis> {

    private RecyclerView recyclerView;
    private AdapterOrderHistory adapter;
    private LinearLayoutManager linearLayoutManager;


    OrderHis addCartList;
    ProgressDialog progressDialog;
    String email,orderId;
    Toolbar  toolbar;
    private SessionManager session;
    private String matchEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        session = new SessionManager(getApplicationContext());


        toolbar.setTitleTextColor(Color.WHITE);
        getSupportActionBar().setTitle("Order History");
        SQLiteHandler sqLiteHandler = new SQLiteHandler(OrderHistoryHome.this);
        List<SessionModel> sessionModelList = sqLiteHandler.getUserDetails();
        if (session.isLoggedIn()) {
            for (SessionModel sm : sessionModelList) {
                matchEmail = sm.getEmail();
                callLoginService(matchEmail);

            }

        }else{
            Toast.makeText(OrderHistoryHome.this, "You are not login", Toast.LENGTH_SHORT).show();
        }
            recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);

            linearLayoutManager = new LinearLayoutManager(OrderHistoryHome.this);
            recyclerView.setLayoutManager(linearLayoutManager);



    }

    public void callLoginService(String matchEmail) {
        String url = String.format(ServerConfigStage.ORDER_HISTORY(),matchEmail);


        url = url.replace(" ", "%20");

        RetrofitTask task = new RetrofitTask<OrderHis>(OrderHistoryHome.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.ORDER_HISTORY, url, getApplicationContext());
        task.execute();


    }
    public void showProgress() {
        progressDialog = new ProgressDialog(OrderHistoryHome.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }




    @Override
    public void onRetrofitTaskComplete(Response<OrderHis> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {
            if (response.body() != null) {
                addCartList    =response.body();

                if(addCartList.getStatus()>0)
                {
                    if (adapter == null) {
                        adapter = new AdapterOrderHistory(context, R.layout.row_order, addCartList,matchEmail);

                        recyclerView.setAdapter( adapter );;

                    }

                    adapter.notifyDataSetChanged();


                }
                else {

                    Toast.makeText(context, addCartList.getResponse(), Toast.LENGTH_SHORT).show();

                }
            }

        }

    }



    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        Log.e("Failed", "Failed");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here

                Intent intent = new Intent(OrderHistoryHome.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        Intent setIntent = new Intent(this,MainActivity.class);

        startActivity(setIntent);
    }




}