package com.jbm.jbmcustomer.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.common.TouchImageView;
import com.squareup.picasso.Picasso;

public class ZoomImageActivity extends AppCompatActivity {

    private String image_url;
    private TouchImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom_image);
        imageView = (TouchImageView)findViewById(R.id.imgview);
        image_url = getIntent().getExtras().getString("imageUri");

        TouchImageView imageView = new TouchImageView(this);
        if (image_url==null ){
            imageView.setImageResource(R.mipmap.def);
        }else {

            Picasso.with(this).load(image_url).resize(240, 120).into(imageView);
        }
        imageView.setMaxZoom(4f);
        setContentView(imageView);
    }
}

