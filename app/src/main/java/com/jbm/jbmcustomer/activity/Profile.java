package com.jbm.jbmcustomer.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.fragment.ProfileSetting;

public class Profile extends AppCompatActivity {
    private Fragment fragment;
    private FragmentTransaction ft;
    private Toolbar toolbar;
    int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Profile");
        toolbar.setTitleTextColor(Color.WHITE);


        fragment = new ProfileSetting();
        ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.addToBackStack(null);
        ft.commit();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:


                // todo: goto back activity from here
                pos = getIntent().getExtras().getInt("position");
                Intent intent = new Intent(Profile.this, HomeActivity.class);
                intent.putExtra("fragment", pos);
                startActivity(intent);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
            if (getFragmentManager().getBackStackEntryCount() == 0) {
                this.finish();
            } else {
                getFragmentManager().popBackStack();
            }
        pos = getIntent().getExtras().getInt("position");
        if (pos<10){
        Intent intent=new Intent(Profile.this,HomeActivity.class);
        intent.putExtra("fragment",pos);
        startActivity(intent);
        }else if (pos==10){
            Intent intent=new Intent(Profile.this,PromotionActivity.class);
            startActivity(intent);
        }
        super.onBackPressed();
    }

}

