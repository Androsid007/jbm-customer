package com.jbm.jbmcustomer.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jbm.jbmcustomer.DatabaseHelper.SQLiteHandler;
import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.Example;
import com.jbm.jbmcustomer.models.SessionModel;
import com.jbm.jbmcustomer.sharedpreference.SessionManager;

import java.util.List;

import retrofit.Response;

public class ChangePassword extends AppCompatActivity implements RetrofitTaskListener<List<Example>> {

    private Button otp;
    ProgressDialog progressDialog;
    EditText email;
    private String confirmedEmail;
    Gson gson = new Gson();
    private Toolbar toolbar;
    private SessionManager session;
    String matchEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forget_password);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Change Password");
        supportInvalidateOptionsMenu();
        toolbar.setTitleTextColor(Color.WHITE);


        session = new SessionManager(ChangePassword.this);
        email = (EditText) findViewById(R.id.email);
        otp = (Button) findViewById(R.id.send_otp);

        SQLiteHandler sqLiteHandler = new SQLiteHandler(ChangePassword.this);
        List<SessionModel> sessionModelList = sqLiteHandler.getUserDetails();
        if (session.isLoggedIn()) {
            for (SessionModel sm : sessionModelList) {
                matchEmail = sm.getEmail();
                email.setText(matchEmail);

                otp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        matchEmail = email.getText().toString();
                        if (validate()) {
                            confirmedEmail = matchEmail;

                            callLoginService(matchEmail);
                        }
                    }
                });
            }
        } else {
            otp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    matchEmail = email.getText().toString();
                    if (validate()) {
                        confirmedEmail = matchEmail;

                        callLoginService(matchEmail);
                    }
                }
            });
        }
    }

    public boolean validate() {
        boolean valid = true;

        matchEmail = email.getText().toString().trim();




        if (matchEmail.isEmpty()||!android.util.Patterns.EMAIL_ADDRESS.matcher(matchEmail).matches() ) {
            email.setError("Please enter valid email id");
            email.requestFocus();

            valid = false;
        }

        return valid;
    }

    public void showProgreass(){
        progressDialog=new ProgressDialog(ChangePassword.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    public void stopProgress(){
        if(progressDialog!=null && progressDialog.isShowing())
            progressDialog.cancel();
    }
    public void callLoginService(String matchEmail){
        showProgreass();
        String url = String.format(ServerConfigStage.GET_USER_INFO(),matchEmail);
        RetrofitTask task = new RetrofitTask<List<Example>>(ChangePassword.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.GET_FORGET_DETAIL, url, ChangePassword.this);
        task.execute();
    }


    @Override
    public void onRetrofitTaskComplete(Response<List<Example>> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {
            ;
            if (response.body() != null) {
                if(response.body().size()>0) {
                    if(response.body().get(0).getStatus()>0)
                    {

                        String json = gson.toJson(response);
                        System.out.println(json);
                        Intent intent = new Intent(context, OtpScreen.class);
                        intent.putExtra("otpNumber", response.body().get(0).getOTP());
                        intent.putExtra("cardName", response.body().get(0).getCardName());
                        intent.putExtra("cardCord", response.body().get(0).getCardcode());
                        intent.putExtra("email", confirmedEmail);
                        startActivity(intent);

                    }
                    else {
                        String json = gson.toJson(response);
                        System.out.println(json);

                        Toast.makeText(context, response.body().get(0).getResposnse(), Toast.LENGTH_SHORT).show();

                    }
                }

            }
        }

    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(ChangePassword.this,"Fail to load Data",Toast.LENGTH_LONG).show();

        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here

                Intent intent = new Intent(ChangePassword.this, Profile.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("position",3);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
