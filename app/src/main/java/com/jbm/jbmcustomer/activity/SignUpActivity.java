package com.jbm.jbmcustomer.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.Signup;
import com.jbm.jbmcustomer.sharedpreference.SessionManager;

import java.util.List;

import retrofit.Response;

    public class SignUpActivity extends AppCompatActivity implements RetrofitTaskListener<List<Signup>> {

        private Button btn_signup;
        ProgressDialog progressDialog;
        private TextView link_login;
        Gson gson = new Gson();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        EditText input_name,inputPassword,phOne,phTwo,inputMobile,inputEmail,fedralId;
        private String inputPass,inputName,pOne,pTwo,inputMobileNo,email,fedral;
        private SessionManager session;
        private Toolbar toolbar;
        private SharedPreferences sharedPreference;
        private String blockCharacterSet = "@_-.qwertyuiopasdfghjklzxcvbnm1234567890QWERTYUIOPASDFGHJKLZXCVBNM";

        private InputFilter filter = new InputFilter() {

            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

                if (source != null && !blockCharacterSet.contains(("" + source))) {
                    return "";
                }
                return null;
            }
        };



        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_sign_up);

            sharedPreference = getSharedPreferences("NAME",0);


            toolbar = (Toolbar) findViewById(R.id.tool_bar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Register");
            supportInvalidateOptionsMenu();
            toolbar.setTitleTextColor(Color.WHITE);

            session = new SessionManager(getApplicationContext());

            if (session.isLoggedIn()) {
                Intent intent = new Intent(SignUpActivity.this,
                        HomeActivity.class);
                startActivity(intent);
                finish();
            }
            init();

        }

        public void init()
        {
            input_name =(EditText)findViewById(R.id.input_name);
            input_name.setFilters(new InputFilter[] {
                    new InputFilter() {
                        @Override
                        public CharSequence filter(CharSequence cs, int start,
                                                   int end, Spanned spanned, int dStart, int dEnd) {
                            // TODO Auto-generated method stub
                            if(cs.equals("")){ // for backspace
                                return cs;
                            }
                            if(cs.toString().matches("[a-zA-Z ]+")){
                                return cs;
                            }
                            return "";
                        }
                    }
            });
            inputPassword =(EditText)findViewById(R.id.inputPassword);
            inputPassword.setFilters(new InputFilter[]{filter});
            phOne =(EditText)findViewById(R.id.phOne);

            inputMobile =(EditText)findViewById(R.id.inputMobile);
            inputEmail =(EditText)findViewById(R.id.inputEmail);
            link_login =(TextView)findViewById(R.id.link_login);

            fedralId =(EditText)findViewById(R.id.fedralId);
            btn_signup =(Button) findViewById(R.id.btn_signup);
            link_login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(SignUpActivity.this, LoginActivity.class);
                    i.putExtra("fragment",3);
                    startActivity(i);
                    finish();
                }
            });
            btn_signup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
             try {
                 inputPass = inputPassword.getText().toString().trim();
                 inputName = input_name.getText().toString().trim();
                 pOne = phOne.getText().toString().trim();
                 inputMobileNo = inputMobile.getText().toString().trim();
                 email = inputEmail.getText().toString().trim();
                 fedral = fedralId.getText().toString().trim();
                 if (validate()) {

                     callSignUpService(email,inputName);
                 }
             }catch (Exception ex) {
                 stopProgress();
                 Toast.makeText(getApplicationContext(), "Please Enter Correct Password", Toast.LENGTH_SHORT).show();
             }
                }
            });
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case android.R.id.home:
                    // todo: goto back activity from here

                    Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                    return true;

                default:
                    return super.onOptionsItemSelected(item);
            }
        }
        public boolean validate() {
            boolean valid = true;

           String name = inputPassword.getText().toString();
            String inputName = input_name.getText().toString();

            String pOne = phOne.getText().toString();
            String inputMobileNo = inputMobile.getText().toString();
            String email = inputEmail.getText().toString();
            String fedral = fedralId.getText().toString();

            if (inputName.isEmpty() || (!inputName.matches("[a-zA-Z.? ]*"))) {
                input_name.setError("Please enter your name");
                input_name.requestFocus();
                valid = false;
            } else if (name.isEmpty() || name.length() < 7) {
                    inputPassword.setError("Enter at least 8 characters");
                    inputPassword.requestFocus();

                    valid = false;
                } else if (pOne.isEmpty() ) {
                phOne.setError("Enter valid phone number");
                phOne.requestFocus();
                valid = false;
            }  else   if (inputMobileNo.isEmpty()) {
                inputMobile.setError("Enter valid mobile number");
                inputMobile.requestFocus();

                valid = false;
            } else  if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                inputEmail.setError("Enter a valid email address");
                inputEmail.requestFocus();
                valid = false;
            } else if (fedral.isEmpty()) {
                fedralId.setError("Input some value");
                fedralId.requestFocus();
                valid = false;
            } else {
                fedralId.setError(null);


        }
            return valid;
        }

        public void showProgreass(){
            progressDialog=new ProgressDialog(SignUpActivity.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
        public void stopProgress(){
            if(progressDialog!=null && progressDialog.isShowing())
                progressDialog.cancel();
        }
        public void callSignUpService(String email,String inputName){
            showProgreass();
            String url = String.format(ServerConfigStage.GET_OTP(),email,inputName);
            RetrofitTask task = new RetrofitTask<List<Signup>>(SignUpActivity.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.GET_OTP, url, SignUpActivity.this);
            task.execute();


        }


        @Override
        public void onRetrofitTaskComplete(Response<List<Signup>> response, Context context, CommonUtility.CallerFunction _callerFunction) {
            stopProgress();
            if (response.isSuccess()) {
                if (response.body() != null) {
                    if(response.body().get(0).getStatus()>0) {

                            String json = gson.toJson(response);
                            System.out.println(json);
                        Intent intent = new Intent(context, OtpSignUp.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        intent.putExtra("otpNumber", response.body().get(0).getOTP());
                        intent.putExtra("response", response.body().get(0).getResponse());
                        intent.putExtra("NAME", inputName);
                        intent.putExtra("PASSWORD",inputPass);
                        intent.putExtra("P1",pOne);
                        intent.putExtra("P2","123456789000");
                        intent.putExtra("MOBILE",inputMobileNo);
                        intent.putExtra("EMAIL",email);
                        intent.putExtra("FEDERAL",fedral);

                        startActivity(intent);



                        }
                        else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setMessage(response.body().get(0).getResponse())
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //do things
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();

                        }
                    }

                }
            }
        @Override
        public void onRetrofitTaskFailure(Throwable t) {
            stopProgress();
            Toast.makeText(SignUpActivity.this,"Fail to load Data",Toast.LENGTH_LONG).show();

            finish();
        }



    }
