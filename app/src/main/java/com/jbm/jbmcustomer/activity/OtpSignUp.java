package com.jbm.jbmcustomer.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jbm.jbmcustomer.DatabaseHelper.SQLiteHandler;
import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.Signup;

import java.util.List;

import retrofit.Response;

/**
 * Created by hp1 on 1/5/2017.
 */

public class OtpSignUp extends AppCompatActivity implements RetrofitTaskListener<List<Signup>> {

    String message,email,pwd,name,p1,p2,mobile,fedId,otpNumber;
    EditText otpVeryfy;
    Button verifyBtn;
    LinearLayout lyt;
    private ProgressDialog progressDialog;
    private SQLiteHandler db;
    private Gson gson;
    RelativeLayout ryt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_screen);
        Bundle bundle = getIntent().getExtras();

        email = getIntent().getExtras().getString("EMAIL");
        pwd = getIntent().getExtras().getString("PASSWORD");
        name = getIntent().getExtras().getString("NAME");
        p1 = getIntent().getExtras().getString("P1");
        p2 = getIntent().getExtras().getString("P2");
        mobile = getIntent().getExtras().getString("MOBILE");
        fedId = getIntent().getExtras().getString("FEDERAL");
        otpNumber = getIntent().getExtras().getString("otpNumber");
        db = new SQLiteHandler(getApplicationContext());


        unique();
    }


    public void  unique() {
        otpVeryfy = (EditText) findViewById(R.id.otpVeryfy);
        verifyBtn = (Button) findViewById(R.id.verifyBtn);
        lyt = (LinearLayout) findViewById(R.id.credential_layout);

        verifyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                String otpText = otpVeryfy.getText().toString();

                if (otpText.equals(otpNumber))

                {
                    lyt.setVisibility(view.GONE);
                      callSignUpService(email,pwd,name,p1,p2,mobile,fedId);


                } else {
                    otpVeryfy.setError("Wrong Otp");

                }


            }
        });
    }

    public void showProgreass(){
        progressDialog=new ProgressDialog(OtpSignUp.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    public void stopProgress(){
        if(progressDialog!=null && progressDialog.isShowing())
            progressDialog.cancel();
    }
    public void callSignUpService(String email,String pswd,String inputName,String ph1,String ph2,String mob,String fed){
        showProgreass();
        String url = String.format(ServerConfigStage.GET_USER_SIGNUP(),email,pswd,inputName,ph1,ph2,mob,fed);
        RetrofitTask task = new RetrofitTask<List<Signup>>(OtpSignUp.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.GET_OTP, url, OtpSignUp.this);
        task.execute();


    }

    @Override
    public void onRetrofitTaskComplete(Response<List<Signup>> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {
            if (response.body() != null) {
                if(response.body().get(0).getStatus()>0) {

                    db.addUser(name,email);

                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(response.body().get(0).getResponse())
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent i = new Intent(OtpSignUp.this, LoginActivity.class);
                                    i.putExtra("fragment",3);

                                    startActivity(i);
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(response.body().get(0).getResponse())
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();


                }
            }

        }
    }



    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(OtpSignUp.this,"Fail to load Data",Toast.LENGTH_LONG).show();

        finish();
    }
    @Override
    public void onBackPressed()
    {
        // d
        super.onBackPressed();
        Intent intent = new Intent(OtpSignUp.this, LoginActivity.class);
        intent.putExtra("fragment",3);
        startActivity(intent);
        finish();

    }

}












