package com.jbm.jbmcustomer.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.fragment.CartFragment;
import com.jbm.jbmcustomer.fragment.FastOrderFragment;
import com.jbm.jbmcustomer.fragment.HomeFragment;
import com.jbm.jbmcustomer.fragment.NewProduct;
import com.jbm.jbmcustomer.fragment.NotificationFragment;
import com.jbm.jbmcustomer.sharedpreference.SessionManager;

public class HomeActivity extends AppCompatActivity {
    private TabLayout mTabLayout;
    private Toolbar toolbar;
    private String code,name;
    private int position;
    private int index;
    private CustomViewPager viewPager;
    private SessionManager session;

    private int[] mTabsIcons = {
            R.drawable.fast_order,
            R.drawable.new_products,
            R.drawable.products,
            R.drawable.cart,
            R.drawable.notification,
            R.drawable.ic_home_selector,
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar);

        session = new SessionManager(getApplicationContext());

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        supportInvalidateOptionsMenu();
        toolbar.setTitleTextColor(Color.WHITE);

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                } else {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                }
            }
        });

     position = getIntent().getExtras().getInt("fragment");

        viewPager = (CustomViewPager) findViewById(R.id.view_pager);
        MyPagerAdapter pagerAdapter = new MyPagerAdapter(getSupportFragmentManager());

        if (viewPager != null) {
            viewPager.setPagingEnabled(false);
            viewPager.setAdapter(pagerAdapter);
        }

        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);

        if (mTabLayout != null) {
            mTabLayout.setupWithViewPager(viewPager);

            for (int i = 0; i < mTabLayout.getTabCount(); i++) {
                TabLayout.Tab tab = mTabLayout.getTabAt(i);
                if (tab != null)
                    tab.setCustomView(pagerAdapter.getTabView(i));
            }

            mTabLayout.getTabAt(0).getCustomView().setSelected(true);
        }

        if (position == 1) {
            index = 1;
            viewPager.setCurrentItem(0, false);
            getSupportActionBar().setTitle("Fast Order");
        } else if (position == 2) {
            index = 2;
            viewPager.setCurrentItem(1, false);
            getSupportActionBar().setTitle("New Products");
        } else if (position == 3) {
            index = 3;
            viewPager.setCurrentItem(2, false);
            getSupportActionBar().setTitle("Products Category");
        } else if (position == 4) {
            index = 4;
            viewPager.setCurrentItem(3, false);
            getSupportActionBar().setTitle("Cart");
        } else if (position == 5) {
            index = 5;
            viewPager.setCurrentItem(4, false);
            getSupportActionBar().setTitle("Notification");
        } else if (position == 7) {
            index = 3;
            viewPager.setCurrentItem(2, false);
            getSupportActionBar().setTitle("Products Category");
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                switch (position) {
                    case 0:
                        index = 1;
                        getSupportActionBar().setTitle("Fast Order");
                        break;
                    case 1:
                        index = 2;

                        getSupportActionBar().setTitle("New Products");
                        break;
                    case 2:
                        index = 3;
                        getSupportActionBar().setTitle("Products Category");
                        break;
                    case 3:
                        index = 4;

                        getSupportActionBar().setTitle("Cart");
                        break;
                    case 4:
                        index = 5;
                        getSupportActionBar().setTitle("Notification");
                        break;
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.profile:
                        Intent profileIntent = new Intent(HomeActivity.this, Profile.class);
                        profileIntent.putExtra("position", index);
                        startActivity(profileIntent);
                        finish();
                        return true;

                }
                return false;
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                // todo: goto back activity from here

                Intent intent = new Intent(HomeActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.profile).setEnabled(true);

        return super.onPrepareOptionsMenu(menu);
    }

    private class MyPagerAdapter extends FragmentStatePagerAdapter {

        public final int PAGE_COUNT = 5;
        private final String[] mTabsTitle = {"Home", "New Product", "Category", "Cart", "Notification"};

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public View getTabView(int position) {
             View view = LayoutInflater.from(HomeActivity.this).inflate(R.layout.custom_tab, null);
            TextView title = (TextView) view.findViewById(R.id.title);
             ImageView icon = (ImageView) view.findViewById(R.id.icon);
            icon.setImageResource(mTabsIcons[position]);
            return view;
        }

        @Override
        public Fragment getItem(int pos) {

            switch (pos) {
                case 0:
                    return FastOrderFragment.newInstance(0);
                case 1:
                    return NewProduct.newInstance(1);
                case 2:
                    return HomeFragment.newInstance(HomeActivity.this,2);
                case 3:
                    return CartFragment.newInstance(3);
                case 4:
                    return NotificationFragment.newInstance(4);
            }
            return null;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTabsTitle[position];
        }
    }

    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        Intent setIntent = new Intent(HomeActivity.this,MainActivity.class);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
    }
    @Override
    public void onResume() {
        super.onResume();
        if (index == 1) {
            getSupportActionBar().setTitle("Fast Order");
        } else if (index == 2) {
            getSupportActionBar().setTitle("New Products");
        } else if (index == 3) {
            getSupportActionBar().setTitle("Product Category");
        } else if (index == 4) {
            getSupportActionBar().setTitle("Cart");
        } else if (index == 5) {
            getSupportActionBar().setTitle("Notification");
        }
    }
}