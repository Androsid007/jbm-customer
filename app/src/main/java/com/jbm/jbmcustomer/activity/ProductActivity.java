package com.jbm.jbmcustomer.activity;


import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.adapter.AdapterProductMaster;
import com.jbm.jbmcustomer.adapter.AdapterProductSearchItem;
import com.jbm.jbmcustomer.adapter.ProductListAdapter;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.Product;
import com.jbm.jbmcustomer.sharedpreference.SessionManager;

import java.util.ArrayList;
import java.util.List;

import retrofit.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductActivity extends AppCompatActivity implements RetrofitTaskListener<List<Product>> {

    ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    AdapterProductMaster adapter;
    AdapterProductSearchItem searchAdapter;
    private boolean loading = true;
    ArrayList<Product> productList = new ArrayList<Product>();
    ArrayList<Product> productListSearch = new ArrayList<Product>();
    int pageIndex, pastVisiblesItems, visibleItemCount, totalItemCount,searchPageIndex;
    private GridLayoutManager gridLayoutManager;
    String categoryID;
    private static final String STARTING_TEXT = "Four Buttons Bottom Navigation";
    private Toolbar toolbar;
    private ProductListAdapter productListAdapter;
    private ListView productListView;
    private SearchView searchView;
    private String sbmtQry;
    private String idNum;
    private SessionManager session;
    private SharedPreferences sharedPreferences;
    private String blockCharacterSet = "qwertyuiopasdfghjklzxcvbnm1234567890QWERTYUIOPASDFGHJKLZXCVBNM";
    boolean mbool = false;


    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && !blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };


    public ProductActivity() {
        // Required empty public constructor
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Products");
        supportInvalidateOptionsMenu();
        toolbar.setTitleTextColor(Color.WHITE);
        session = new SessionManager(getApplicationContext());
        sharedPreferences = getSharedPreferences("NAME",0);
        if (session.isLoggedIn()){
        idNum = sharedPreferences.getString("ID_NUMBER","");
        }else{
            idNum = sharedPreferences.getString("ID_NUMBER","0");
        }

        productListView = (ListView)findViewById(R.id.friend_list);
        productListView.setVisibility(View.GONE);

        pageIndex = 0;
        recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        gridLayoutManager = new GridLayoutManager(ProductActivity.this, 2);
        initViews();
        initViews();


            categoryID = getIntent().getExtras().getString("categoryID");
            callLoginService(20,categoryID,idNum);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    //Change the ImageView image source depends on menu item click
                    case R.id.profile:
                        Intent profileIntent = new Intent(ProductActivity.this, Profile.class);
                        profileIntent.putExtra("position", "2");
                        startActivity(profileIntent);
                        finish();
                        return true;

                }
                return false;
            }
        });

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.profile).setEnabled(true);

        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public void onStart(){
        super.onStart();
        productListView.setVisibility(View.GONE);
    }


    private void initViews() {


        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setLayoutManager(gridLayoutManager);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    final LinearLayoutManager mLayoutManager = (LinearLayoutManager) recyclerView
                            .getLayoutManager();
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            callLoginService(20,categoryID,idNum);

                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });


    }

    public void callLoginService(int size,String categoryID,String cid) {

        if (!mbool) {
            showProgreass();
            mbool=true;

        } else {
            // other time your app loads
        }
        pageIndex = pageIndex + 1;
        String url = String.format(ServerConfigStage.PRODUCT_MASTER_API(), pageIndex + "", size,categoryID,cid);
        RetrofitTask task = new RetrofitTask<List<Product>>(ProductActivity.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.PRODUCT_ITEM_MASTER, url, this);
        task.execute();


    }

    public void callSearchProduct(String categoryID,String query,String cid) {
        // showProgreass();
        searchPageIndex = searchPageIndex + 1;
        String url = String.format(ServerConfigStage.PRODUCT_MASTER_SEARCH_API(), searchPageIndex + "",categoryID,query,cid);
        RetrofitTask task = new RetrofitTask<List<Product>>(ProductActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.PRODUCT_ITEM_SEARCH_MASTER, url, this);
        task.execute();


    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    @Override
    public void onRetrofitTaskComplete(Response<List<Product>> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {
            if (response.body() != null) {
                if (_callerFunction ==CommonUtility.CallerFunction.PRODUCT_ITEM_MASTER) {
                    if (response.body().get(0).getStatus() > 0) {

                        if (response.body().size() == 20) {
                            loading = true;


                            productList.addAll(response.body());
                            if (adapter == null) {
                                adapter = new AdapterProductMaster(context, R.layout.row_news_message_board_card, productList);
                                recyclerView.setAdapter(adapter);
                            }
                            adapter.setLoadingMore(true);
                            adapter.notifyDataSetChanged();

                        } else {
                            loading = false;
                            productList.addAll(response.body());
                          if (adapter==null){
                            adapter = new AdapterProductMaster(context, R.layout.row_news_message_board_card, productList);
                            recyclerView.setAdapter(adapter);
                              adapter.setLoadingMore(false);
                              adapter.notifyDataSetChanged();
                          }

                        }
                    }

                }
                else if (_callerFunction == CommonUtility.CallerFunction.PRODUCT_ITEM_SEARCH_MASTER){
                   if (response.body().get(0).getStatus() > 0) {

                        if (response.body().size() == 20) {
                            loading = true;
                        } else {
                            loading = false;
                        }

                       if(!productListSearch.isEmpty()){
                           productListSearch.clear();
                       }
                        productListSearch.addAll(response.body());
//

                        if (productListSearch==null) {
                            Toast.makeText(ProductActivity.this,"No Result Found",Toast.LENGTH_LONG).show();
                        }else{
                            searchAdapter = new AdapterProductSearchItem(context, R.layout.row_news_message_board_card, productListSearch);
                            recyclerView.setAdapter(searchAdapter);
                        }
                       searchAdapter.setLoadingMore(loading);
                       searchAdapter.notifyDataSetChanged();


                    } else {

                        Toast.makeText(context, response.body().get(0).getResponse(), Toast.LENGTH_SHORT).show();

                    }
                }
            }
            else{
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("No product to display")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                                //do things
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();



            }
        }

        }


    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(this, "Fail to load Data", Toast.LENGTH_LONG).show();

        finish();
    }
    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        Intent setIntent = new Intent(this,HomeActivity.class);
        setIntent.putExtra("fragment",3);
        startActivity(setIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here

                Intent intent = new Intent(ProductActivity.this, HomeActivity.class);
                intent.putExtra("fragment",3);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_category, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchItem.getActionView();
        SearchView.SearchAutoComplete searchEditText = (SearchView.SearchAutoComplete) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchView.findViewById(android.support.v7.appcompat.R.id.search_plate)
                .setBackgroundColor(getResources().getColor(android.R.color.white));
        searchEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps(),new InputFilter.LengthFilter(10)});
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (productList==null){
                    callLoginService(500,categoryID,idNum);
                }
            }
        });


        ImageView closeButton = (ImageView)searchView.findViewById(R.id.search_close_btn);
        closeButton.setBackgroundColor(getResources().getColor(R.color.bottombar));
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchView.setIconified(false);
                pageIndex=0;
                productListView.setVisibility(View.GONE);
                productList.clear();
                searchView.onActionViewCollapsed();
                searchAdapter=null;
                adapter=null;
                productListSearch.clear();
                callLoginService(20,categoryID,idNum);
            }
        });


        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                productListView.setVisibility(View.GONE);
                //productListSearch.clear();
                searchPageIndex=0;
                callSearchProduct(categoryID,query,idNum);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                sbmtQry=newText;
                if (newText.equals("")){
                    productListView.setVisibility(View.GONE);
                }else {
                    productListAdapter.getFilter().filter(newText);
                    productListView.setVisibility(View.VISIBLE);
                }

                return true;

            }
        });
        //categoryListView.setVisibility(View.VISIBLE);
        productListAdapter = new ProductListAdapter(ProductActivity.this, productList);
        productListView.setAdapter(productListAdapter);
        productListView.setTextFilterEnabled(false);

        productListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                productListView.setVisibility(View.GONE);
                productListSearch.clear();
                searchPageIndex=0;
                callSearchProduct(categoryID,sbmtQry,idNum);


            }
        });


        return true;
    }



}
