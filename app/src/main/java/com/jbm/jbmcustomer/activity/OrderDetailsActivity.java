package com.jbm.jbmcustomer.activity;

/**
 * Created by hp1 on 1/28/2017.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.adapter.AdapterOrderDetail;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.OrderDetails;

import retrofit.Response;



public class OrderDetailsActivity extends AppCompatActivity implements RetrofitTaskListener<OrderDetails> {
    ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    AdapterOrderDetail adapter;

    OrderDetails addCartList;

    String email, orderId;
    Toolbar toolbar;

    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_fragment);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        supportInvalidateOptionsMenu();
        toolbar.setTitleTextColor(Color.WHITE);
        getSupportActionBar().setTitle("Order Detail");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            email = bundle.getString("email");
            orderId = bundle.getString("doctype");
        }


        recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        linearLayoutManager = new LinearLayoutManager(OrderDetailsActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);


        callLoginService(email, orderId);


    }

    public void callLoginService(String email, String orderId) {
        showProgreass();

        String url = String.format(ServerConfigStage.GET_ORDER_DETAIL(), email, orderId);
        RetrofitTask task = new RetrofitTask<OrderDetails>(OrderDetailsActivity.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.ORDER_DETAILS, url, OrderDetailsActivity.this);
        task.execute();


    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(OrderDetailsActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    @Override
    public void onRetrofitTaskComplete(Response<OrderDetails> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {
            if (response.body() != null) {
                addCartList = response.body();

                if (addCartList.getStatus() > 0) {
                    if (adapter == null) {
                        adapter = new AdapterOrderDetail(context, R.layout.row_detail, addCartList);

                        recyclerView.setAdapter(adapter);
                        ;

                    }

                    adapter.notifyDataSetChanged();


                } else {

                    Toast.makeText(context, addCartList.getResponse(), Toast.LENGTH_SHORT).show();

                }
            }

        }

    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(OrderDetailsActivity.this, "Fail to load Data", Toast.LENGTH_LONG).show();

        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                Intent intent = new Intent(OrderDetailsActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }


    }
    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        Intent setIntent = new Intent(this,MainActivity.class);

        startActivity(setIntent);
    }
}