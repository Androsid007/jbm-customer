package com.jbm.jbmcustomer.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.common.GPSTracker;
import com.jbm.jbmcustomer.sharedpreference.SessionManager;

import java.util.HashMap;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    private Toolbar toolbar;

    private Button fastOrder, btnProduct, btnNotification, btnNewProduct,btnCart,btnPromotion;
    private TextView count;
    private int cart;
    private SessionManager session;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private SliderLayout mDemoSlider;
    GPSTracker gpsTracker;
    String stringLatitude;
    String stringLongitude;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
      gpsTracker = new GPSTracker(this);

        if (gpsTracker.canGetLocation())
        {
             stringLatitude = String.valueOf(gpsTracker.getLatitude());
            stringLongitude = String.valueOf(gpsTracker.getLongitude());

           // Toast.makeText(MainActivity.this,stringLatitude+" "+stringLongitude,Toast.LENGTH_LONG).show();
        } else
        {

            gpsTracker.showSettingsAlert();
        }

        mDemoSlider = (SliderLayout)findViewById(R.id.slider);

         HashMap<String,Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("7 DRAWER EMPTY CABINET - RED",R.drawable.tempone);
        file_maps.put("AIR IMPACT WRENCH 1/2 COMPOSITE 552NM",R.drawable.temptwo);
        file_maps.put("BATTERY CHARGER 12V 50-140AH",R.drawable.tempthree);
        file_maps.put("PROFESSIONAL DIGITAL CAMP MULTIMETER", R.drawable.tempfour);
        file_maps.put("AIR HOSE REEL WITH RETRACTABLE 20M", R.drawable.tempfive);

        for(String name : file_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

             textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(4000);
        mDemoSlider.addOnPageChangeListener(this);


        sharedPreferences = getSharedPreferences("CART", 0);

        editor = sharedPreferences.edit();

        session = new SessionManager(getApplicationContext());

        btnNewProduct = (Button) findViewById(R.id.promotionBtn);
        fastOrder = (Button) findViewById(R.id.btnFastOrder);
        btnProduct = (Button) findViewById(R.id.btnProduct);
        btnNotification = (Button) findViewById(R.id.button_notification);
        btnCart = (Button) findViewById(R.id.button_cart);
        count = (TextView) findViewById(R.id.count);
        btnPromotion=(Button)findViewById(R.id.promotionBtn1);

        if (session.isLoggedIn()) {
            cart = sharedPreferences.getInt("CART_ITEMS", 0);
            if (cart==0){
                count.setVisibility(View.GONE);
            }else {

                count.setVisibility(View.VISIBLE);
                count.setText(String.valueOf(cart));
            }
        }else{
            editor.putString("ID_NUMBER","0");
            editor.commit();
        }

        if (!isNetworkConnected()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Please check your internet connection")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //do things
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }


        fastOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkConnected()) {
                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    i.putExtra("fragment", 1);
                    startActivity(i);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("Please check your internet connection")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }

            }
        });
        btnNewProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkConnected()) {
                    Intent i = new Intent(MainActivity.this, HomeActivity.class);
                    i.putExtra("fragment", 2);
                    startActivity(i);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("Please check your internet connection")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            }
        });

        btnProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkConnected()) {
                    Intent i = new Intent(MainActivity.this, HomeActivity.class);
                    i.putExtra("fragment", 3);
                    startActivity(i);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("Please check your internet connection")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                 }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        });
        btnCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkConnected()) {
                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    i.putExtra("fragment", 4);
                    startActivity(i);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("Please check your internet connection")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                  }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        });

        btnNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkConnected()) {
                    Intent i = new Intent(MainActivity.this, HomeActivity.class);
                    i.putExtra("fragment", 5);
                    startActivity(i);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("Please check your internet connection")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            }
        });

        btnPromotion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkConnected()) {
                    Intent i = new Intent(MainActivity.this, PromotionActivity.class);
                    i.putExtra("fragment", 1);
                    startActivity(i);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("Please check your internet connection")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }

            }
        });
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                MainActivity.this);
        alertDialog.setTitle("SETTINGS");
        alertDialog.setMessage("Enable Location Provider! Go to settings menu?");
        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        MainActivity.this.startActivity(intent);
                    }
                });
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Exit ?")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton("No", null)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                }).create().show();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


}
