package com.jbm.jbmcustomer.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jbm.jbmcustomer.DatabaseHelper.SQLiteHandler;
import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.adapter.AdapterAddCart;
import com.jbm.jbmcustomer.adapter.AdapterShowCart;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.AddCart;
import com.jbm.jbmcustomer.models.SessionModel;
import com.jbm.jbmcustomer.sharedpreference.SessionManager;

import java.util.List;

import retrofit.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CartFragment extends Fragment implements RetrofitTaskListener<AddCart> {


    private ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    private AdapterAddCart adapter;
    private AdapterShowCart adapterShowCart;
    private boolean loading = true;
    AddCart addCartList;
    int pageIndex;
    private LinearLayoutManager linearLayoutManager;
    private LinearLayout linearLayout;
    private String  email1,articlCode,image,quantity,cost,afterDiscount;
    private static final String STARTING_TEXT = "Four Buttons Bottom Navigation";
    private Button btnCheckout;
    private FragmentTransaction fragmentTransaction;
    private int frag,cartSize;
    private TextView cartEmptyTxt,totalCartQty,totalCartAmt,finalCheckoutAmt,totalVatAmt,shippingText;
    private Context context;
    private SharedPreferences sharedPreference;
    private SharedPreferences.Editor editor;
    private SessionManager session;
    private EditText cmntTxt;
    private int flag=0;
    private int position;
    AddCart addCart,delCart;
    private double discount;

    public CartFragment() {

    }

   public static CartFragment newInstance(int text) {
        Bundle args = new Bundle();
        args.putInt(STARTING_TEXT, text);

       CartFragment sampleFragment = new CartFragment();
        sampleFragment.setArguments(args);
        return sampleFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        articlCode = getArguments().getString("artCode");
        image = getArguments().getString("image");
        quantity = getArguments().getString("quantity");
        cost = getArguments().getString("cost");
        frag = getArguments().getInt("fragment");
        discount=getArguments().getDouble("discount");
        afterDiscount=getArguments().getString("afterDiscount");


        View view =  inflater.inflate(R.layout.fragment_cart, container, false);
        cmntTxt = (EditText)view.findViewById(R.id.comment_txt);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        totalCartQty=(TextView)view.findViewById(R.id.totalCartQty);
        totalCartAmt=(TextView)view.findViewById(R.id.totalCartAmt);
        finalCheckoutAmt=(TextView)view.findViewById(R.id.finalCheckoutAmt);
        totalVatAmt=(TextView)view.findViewById(R.id.totalVatAmt);
        linearLayout=(LinearLayout)view.findViewById(R.id.abc);
        shippingText=(TextView)view.findViewById(R.id.shippingText);

        session = new SessionManager(getActivity().getApplicationContext());

        sharedPreference = getActivity().getSharedPreferences("CART",0);
        editor = sharedPreference.edit();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Cart");

        btnCheckout = (Button)view.findViewById(R.id.btnCheckout);

        cartEmptyTxt = (TextView)view.findViewById(R.id.emptytxt);
        cartEmptyTxt.setVisibility(View.GONE);

        if (!session.isLoggedIn()) {
            cartEmptyTxt.setVisibility(View.VISIBLE);
            btnCheckout.setVisibility(View.GONE);
            cmntTxt.setVisibility(View.GONE);

        }

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
     /*FragmentManager fm = getFragmentManager();
                fragmentTransaction = fm.beginTransaction();
                AddressFragment addressFragment = new AddressFragment();
        fragmentTransaction.replace(R.id.fragmentContainer,addressFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit(); */
                Intent intent= new Intent(getActivity(),AddressFragment.class);
                 startActivity(intent);
            }
        });
        pageIndex = 0;
        recyclerView = (RecyclerView)view.findViewById(R.id.cart_recycler_view);
         linearLayoutManager=new LinearLayoutManager(getActivity());
   recyclerView.setLayoutManager(linearLayoutManager);
    SQLiteHandler sqLiteHandler = new SQLiteHandler(getActivity());
        List<SessionModel> sessionModelList = sqLiteHandler.getUserDetails();
        Bundle arguments = getArguments();
        if (arguments != null) {
            for (SessionModel sm : sessionModelList){
                email1 = sm.getEmail();
            }

            if (frag == 6){
            callCartService(email1,articlCode,afterDiscount,quantity);
            }else if (email1!=null){
                callGetCart(email1);
            }
     }


        return view;
    }

 /*   @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Products Category");
    }*/
  public void callCartService(String email,String artCode,String price,String qty){
        showProgreass();
        pageIndex = pageIndex + 1;
        String url = String.format(ServerConfigStage.ADD_CART(),email,artCode,price,qty);
        RetrofitTask task = new RetrofitTask<>(CartFragment.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.ADD_CART_FUNCTION, url,getActivity());
        task.execute();
  }

    public void callGetCart(String email){
        showProgreass();
        pageIndex = pageIndex + 1;
        String url = String.format(ServerConfigStage.GET_CART(),email);
        RetrofitTask task = new RetrofitTask<>(CartFragment.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.GET_CART, url,getActivity());
        task.execute();
    }

    public void delItemCart(String email,AddCart item,int flagPos,int pos){

        showProgreass();
        addCart = item;
        position = pos;
        flag=flagPos;
        String url = String.format(ServerConfigStage.DEL_ITEM_CART(),email,item.getDeatils().get(position).getId());
        RetrofitTask task = new RetrofitTask<>(CartFragment.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.DEL_ITEM_CART, url,getActivity());
        task.execute();
    }

    public void delItemCart1(String email,AddCart item,int flagPos,int pos){

        showProgreass();
        addCart = item;
        flag=flagPos;
        position = pos;
        String url = String.format(ServerConfigStage.DEL_ITEM_CART(),email,item.getDeatils().get(position).getId());
        RetrofitTask task = new RetrofitTask<>(CartFragment.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.DEL_ITEM_CART, url,getActivity());
        task.execute();
    }


 public void showProgreass(){
     progressDialog =new ProgressDialog(getActivity());

        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    public void stopProgress(){
        if(progressDialog!=null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    @Override
    public void onRetrofitTaskComplete(Response<AddCart> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {
            if (response.body() != null) {


              if (_callerFunction == CommonUtility.CallerFunction.ADD_CART_FUNCTION){
                  addCartList    = response.body();

                  if (addCartList.getStatus() == 0){
                      notifyAddCart();
                      cartSize = addCartList.getDeatils().size();
                      editor.putInt("CART_ITEMS",cartSize);
                      editor.commit();

                      Toast.makeText(context, addCartList.getResponse(), Toast.LENGTH_SHORT).show();
                      if (adapter == null) {
                          adapter = new AdapterAddCart(context, R.layout.row_cart, addCartList,this,flag,discount,cost);
                          recyclerView.setAdapter(adapter);
                      }
                  }else if (addCartList.getStatus()<0){
                      cartSize = addCartList.getDeatils().size();
                      editor.putInt("CART_ITEMS",cartSize);
                      editor.commit();
                      AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                      builder.setMessage(addCartList.getResponse())
                              .setCancelable(false)
                              .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                  public void onClick(DialogInterface dialog, int id) {
                                      getActivity().finish();
                                  }
                              });
                      AlertDialog alert = builder.create();
                      alert.show();

                  }
                  else if(addCartList.getStatus()>0) {
                        notifyAddCart();
                        cartSize = addCartList.getDeatils().size();
                        editor.putInt("CART_ITEMS",cartSize);
                        editor.commit();


          if (adapter == null) {
                        adapter = new AdapterAddCart(context, R.layout.row_cart, addCartList,this,flag,discount,cost);
                  recyclerView.setAdapter(adapter);

             }

                    adapter.notifyDataSetChanged();
                }
                }else if (_callerFunction == CommonUtility.CallerFunction.GET_CART){
                  addCartList    =response.body();
                  if (addCartList.getStatus() == 0){
                      notifyAddCart();
                      if (addCartList.getDeatils().size()>0){
                      cartSize = addCartList.getDeatils().size();
                      editor.putInt("CART_ITEMS",cartSize);
                      editor.commit();

                      }else{
                          editor.putInt("CART_ITEMS",0);
                          editor.commit();
                      }
                  }if(addCartList.getStatus()>0) {
                        notifyAddCart();
                        if (addCartList.getDeatils().size()>0){
                        cartSize = addCartList.getDeatils().size();
                        editor.putInt("CART_ITEMS",cartSize);
                        editor.commit();}else{
                            editor.putInt("CART_ITEMS",0);
                            editor.commit();
                        }
                        if (adapterShowCart == null) {
                            adapterShowCart = new AdapterShowCart(context, R.layout.row_cart, addCartList,this,flag,discount,cost);

                            recyclerView.setAdapter(adapterShowCart);
                            ;

                        }
                        adapterShowCart.notifyDataSetChanged();
                }else {
                        cartEmptyTxt.setVisibility(View.VISIBLE);
                        btnCheckout.setVisibility(View.GONE);
                        cmntTxt.setVisibility(View.GONE);

                    }

                 }else if (_callerFunction == CommonUtility.CallerFunction.DEL_ITEM_CART){
                  delCart=response.body();
                  if (delCart.getStatus()==-15)
                  {
                      editor.putInt("CART_ITEMS",0);
                      editor.commit();
                      recyclerView.setVisibility(View.GONE);
                      linearLayout.setVisibility(View.GONE);
                      cartEmptyTxt.setVisibility(View.VISIBLE);
                  }
                  else if (delCart.getStatus()>0) {
                      notifyDelCart();
                      if (flag == 1) {
                          if (addCartList.getDeatils().contains(addCart.getDeatils().get(position))) {
                              addCartList.getDeatils().remove(addCart.getDeatils().get(position));
                              if (addCartList.getDeatils().size()>0){
                              cartSize = addCartList.getDeatils().size();
                              editor.putInt("CART_ITEMS", cartSize);
                              editor.commit();}else{
                                  editor.putInt("CART_ITEMS",0);
                                  editor.commit();
                              }
                          }
                          if (adapterShowCart == null) {
                              recyclerView.setAdapter(new AdapterShowCart(CartFragment.this.getActivity(), R.layout.row_cart, addCartList, CartFragment.this, flag,discount,cost));

                          }
                          adapterShowCart.notifyDataSetChanged();
                      } else if (flag == 2) {
                          if (addCartList.getDeatils().contains(addCart.getDeatils().get(position))) {
                              addCartList.getDeatils().remove(addCart.getDeatils().get(position));
                              if (addCartList.getDeatils().size()>0){
                              cartSize = addCartList.getDeatils().size();
                              editor.putInt("CART_ITEMS", cartSize);
                              editor.commit();
                              }else{
                                  editor.putInt("CART_ITEMS",0);
                                  editor.commit();
                              }

                          }
                          if (adapter == null) {
                              recyclerView.setAdapter(new AdapterAddCart(CartFragment.this.getActivity(), R.layout.row_cart, addCartList, CartFragment.this, flag,discount,cost));

                          }
                          adapter.notifyDataSetChanged();


                      }
                  }
                  }
              }
            }

        }



    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();

        if (getActivity()!= null)
        {
        }
    }

    public void notifyDelCart(){
        double vatAmt = ((21.00/100)*Double.parseDouble(delCart.getCartTotal()));
        double ship = Double.parseDouble(delCart.getShipCost());
        double minFrt = Double.parseDouble(delCart.getMinFrtAmt());
        String vat = String.format("%.2f", vatAmt);
        String shipCost = String.format("%.2f",ship );
        double cartTot = Double.parseDouble(delCart.getCartTotal())+vatAmt+ship;
        double freeCartTot = Double.parseDouble(delCart.getCartTotal())+vatAmt;
        String totalCheckout = String.format("%.2f",cartTot);
        String freeTotalCheckout = String.format("%.2f",freeCartTot);

        totalCartQty.setText(delCart.getCartQuantityTotal());
        totalCartAmt.setText("€"+""+delCart.getCartTotal());
        finalCheckoutAmt.setText("€"+""+totalCheckout);
        totalVatAmt.setText("€"+""+vat);

        if (cartTot>minFrt){
            shippingText.setText("€"+""+0);
            finalCheckoutAmt.setText("€"+""+freeTotalCheckout);
        }else{
            shippingText.setText("€"+""+shipCost);
            finalCheckoutAmt.setText("€"+""+totalCheckout);
        }


    }

    public void notifyAddCart(){
        linearLayout.setVisibility(View.VISIBLE);
        double vatAmt = ((21.00/100)*Double.parseDouble(addCartList.getCartTotal()));
        double ship = Double.parseDouble(addCartList.getShipCost());
        double minFrt = Double.parseDouble(addCartList.getMinFrtAmt());
        String vat = String.format("%.2f", vatAmt);
        String shipCost = String.format("%.2f",ship );
        double cartTot = Double.parseDouble(addCartList.getCartTotal())+vatAmt+ship;
        double freeCartTot = Double.parseDouble(addCartList.getCartTotal())+vatAmt;
        String totalCheckout = String.format("%.2f",cartTot);
        String freeTotalCheckout = String.format("%.2f",freeCartTot);

        totalCartQty.setText(addCartList.getCartQuantityTotal());
        totalCartAmt.setText("€"+""+addCartList.getCartTotal());
        finalCheckoutAmt.setText("€"+""+totalCheckout);
        totalVatAmt.setText("€"+""+vat);

        if (cartTot>minFrt){
            shippingText.setText("€"+""+0);
            finalCheckoutAmt.setText("€"+""+freeTotalCheckout);
        }else{
            shippingText.setText("€"+""+shipCost);
            finalCheckoutAmt.setText("€"+""+totalCheckout);
        }
    }


}
