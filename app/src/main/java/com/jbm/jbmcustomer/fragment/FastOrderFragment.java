package com.jbm.jbmcustomer.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jbm.jbmcustomer.DatabaseHelper.FastOrderHelper;
import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.activity.LoginActivity;
import com.jbm.jbmcustomer.activity.MainActivity;
import com.jbm.jbmcustomer.adapter.FastOrderAdapter;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.FastOrderModel;
import com.jbm.jbmcustomer.models.Fastjson;
import com.jbm.jbmcustomer.models.ProductDetail;
import com.jbm.jbmcustomer.sharedpreference.SessionManager;

import java.util.ArrayList;
import java.util.List;

import retrofit.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class FastOrderFragment extends Fragment implements RetrofitTaskListener<Object> {
    public FastOrderHelper helper;
    private EditText articleNo, qty, yourName, yourEmail, providersEmail;
    private Button add, sendEmail, login;
    private RecyclerView recyclerView;
    private FastOrderAdapter adapter;
    private RadioGroup sendTo;
    private RadioButton sendToJBM, sendToProvider;
    private ProgressDialog progressDialog;
    private static final String STARTING_TEXT = "Four Buttons Bottom Navigation";
    private String articleNumber,url;
    private TextView availability;
    int flag = 1;
    public List<FastOrderModel> list;
    private String urEmail, urName, providerEmail;
    private SessionManager session;
    public static FastOrderHelper fastOrderHelper;
    public static List<FastOrderModel> fastList;
    private ScrollView scrollView;
    private ProductDetail abc;
    Double stk;
    private RelativeLayout relativeLayout;
    Double stock;
    private String idNum;
    private SharedPreferences sharedPreferences;
    private String blockCharacterSet = "@#₹_&-+()/*\"':;!?.~`|•√π÷×¶∆£€$¢^°={}\\%©®™℅[]><";

    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };


    public FastOrderFragment() {
        // Required empty public constructor
    }

    public static FastOrderFragment newInstance(int text) {
        Bundle args = new Bundle();
        args.putInt(STARTING_TEXT, text);

        FastOrderFragment sampleFragment = new FastOrderFragment();
        sampleFragment.setArguments(args);
        return sampleFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_fast_order, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        urEmail = urName = providerEmail = null;
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Fast Order");

        helper = new FastOrderHelper(getActivity());
        fastOrderHelper = helper;
        session = new SessionManager(getActivity());

        sharedPreferences = getActivity().getSharedPreferences("NAME", 0);
        idNum = sharedPreferences.getString("ID_NUMBER", "");

        login = (Button) view.findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.putExtra("fragment", 1);
                startActivity(intent);
            }
        });
        scrollView = (ScrollView) view.findViewById(R.id.scroll_view);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.btn_layout);

        if (session.isLoggedIn()) {
            relativeLayout.setVisibility(View.GONE);
        } else {
            scrollView.setVisibility(View.GONE);
            relativeLayout.setVisibility(View.VISIBLE);

        }

        availability = (TextView) view.findViewById(R.id.stock);
        recyclerView = (RecyclerView) view.findViewById(R.id.card_recycler_view);
        articleNo = (EditText) view.findViewById(R.id.enter_article);
        articleNo.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        articleNo.setFilters(new InputFilter[]{filter});

        qty = (EditText) view.findViewById(R.id.enter_qty);

        qty.setFilters(new InputFilter[]{filter,new InputFilter.LengthFilter(4)});



        yourName = (EditText) view.findViewById(R.id.your_name);
        yourEmail = (EditText) view.findViewById(R.id.your_email);
        providersEmail = (EditText) view.findViewById(R.id.provider_email);
        providersEmail.setVisibility(View.GONE);

        sendToJBM = (RadioButton) view.findViewById(R.id.jbm_radio);
        sendToProvider = (RadioButton) view.findViewById(R.id.provider_radio);

        sendEmail = (Button) view.findViewById(R.id.button_send);

        sendTo = (RadioGroup) view.findViewById(R.id.sendGroup);
        sendTo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {

                    case R.id.jbm_radio:
                        flag = 1;
                        yourName.setText("");
                        yourEmail.setText("");
                        providersEmail.setVisibility(View.GONE);

                        break;

                    case R.id.provider_radio:
                        flag = 0;
                        providersEmail.setText("");
                        yourName.setText("");
                        yourEmail.setText("");
                        providersEmail.setVisibility(View.VISIBLE);

                        break;
                }
            }
        });

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerFast);

        add = (Button) view.findViewById(R.id.add_article);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String artNo = articleNo.getText().toString().toUpperCase();
                String quantity = qty.getText().toString();
                if (artNo.isEmpty() || quantity.isEmpty()) {
                    if (artNo.isEmpty()) {
                        articleNo.setError("Enter Article No.");
                    } else if (quantity.isEmpty()) {
                        qty.setError("Quantity empty");
                    } else if (artNo.isEmpty() && quantity.isEmpty()) {
                        articleNo.setError("Enter Article No.");
                        qty.setError("Quantity empty");
                    }

                } else {
                    checkAvailibility(artNo, idNum);
                    articleNo.setError(null);
                    qty.setError(null);

                }
            }
        });
        showData();
        sendEmail.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View view) {

                                             if (list.isEmpty()) {
                                                 if (articleNo.getText().toString().isEmpty() && qty.getText().toString().isEmpty()) {
                                                     articleNo.setError("Add articles");
                                                 } else if (articleNo.getText().toString().isEmpty()) {
                                                     articleNo.setError("Add Article");
                                                 } else if (qty.getText().toString().isEmpty()) {
                                                     qty.setError("Add Quantity");
                                                 } else {
                                                     qty.setError(null);
                                                     articleNo.setError(null);
                                                     AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                                     builder.setMessage("Please add Article")
                                                             .setCancelable(false)
                                                             .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                                 public void onClick(DialogInterface dialog, int id) {
                                                                 }
                                                             });
                                                     AlertDialog alert = builder.create();
                                                     alert.show();
                                                 }

                                             } else {

                                                 if (session.isLoggedIn()) {
                                                     switch (flag) {
                                                         case 0:
                                                             urEmail = yourEmail.getText().toString().trim();
                                                             urName = yourName.getText().toString().trim();
                                                             providerEmail = providersEmail.getText().toString().trim();
                                                             if (urEmail.equals(providerEmail)) {
                                                                 AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                                                 builder.setMessage("Please enter different email id")
                                                                         .setCancelable(false)
                                                                         .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                                             public void onClick(DialogInterface dialog, int id) {
                                                                             }
                                                                         });
                                                                 AlertDialog alert = builder.create();
                                                                 alert.show();
                                                             } else {

                                                                 if (validate(0)) {
                                                                     yourEmail.setText("");
                                                                     providersEmail.setText("");
                                                                     yourName.setText("");
                                                                     String url = String.format(ServerConfigStage.GET_FAST_ORDER(), urName, urEmail, providerEmail);
                                                                     callLoginService(url);
                                                                 }
                                                             }
                                                             break;

                                                         case 1:
                                                             urEmail = yourEmail.getText().toString().trim();
                                                             urName = yourName.getText().toString().trim();
                                                             if (validate(1)) {
                                                                 yourEmail.setText("");
                                                                 yourName.setText("");
                                                                 String url = String.format(ServerConfigStage.GET_FAST_ORDER(), urName, urEmail, 1);
                                                                 callLoginService(url);
                                                             }
                                                             break;
                                                     }
                                                 } else {
                                                     AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                                     builder.setMessage("Please Login To Continue")
                                                             .setCancelable(false)
                                                             .setNegativeButton("NOT NOW", new DialogInterface.OnClickListener() {
                                                                 @Override
                                                                 public void onClick(DialogInterface dialogInterface, int i) {

                                                                 }
                                                             })
                                                             .setPositiveButton("LOGIN", new DialogInterface.OnClickListener() {
                                                                 public void onClick(DialogInterface dialog, int id) {
                                                                     Intent intent = new Intent(getActivity(), LoginActivity.class);
                                                                     intent.putExtra("fragment", 1);
                                                                     startActivity(intent);
                                                                 }
                                                             });
                                                     AlertDialog alert = builder.create();
                                                     alert.show();
                                                 }
                                             }

                                         }
                                     }

        );

        return view;
    }

    public boolean validate(int flag) {
        boolean valid = true;
        //urEmail = yourEmail.getText().toString().trim();
        //urName = yourName.getText().toString().trim();
        // providerEmail = providersEmail.getText().toString().trim();
        switch (flag) {
            case 0:
                if (urName.isEmpty() || (!urName.matches("[a-zA-Z.? ]*"))) {
                    yourName.setError("Please enter your name");
                    yourName.requestFocus();
                    valid = false;
                } else if (urEmail.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(urEmail).matches()) {
                    yourEmail.setError("Enter a valid email address");
                    yourEmail.requestFocus();
                    valid = false;
                } else if (providerEmail.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(providerEmail).matches()) {
                    providersEmail.setError("Enter a valid email address");
                    providersEmail.requestFocus();
                    valid = false;
                } else {

                    yourEmail.setError(null);
                    yourName.setError(null);
                    providersEmail.setError(null);
                    valid = true;
                }
                break;

            case 1:
                if (urName.isEmpty() || (!urName.matches("[a-zA-Z.? ]*"))) {
                    yourName.setError("Please enter your name");
                    yourName.requestFocus();
                    valid = false;
                } else if (urEmail.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(urEmail).matches()) {
                    yourEmail.setError("Enter a valid email address");
                    yourEmail.requestFocus();
                    valid = false;
                } else {
                    yourEmail.setError(null);
                    yourName.setError(null);
                    valid = true;
                }
                break;
        }
        return valid;
    }

    private void initViews() {
    }

    public void callLoginService(String url) {
        showProgreass();
        list = helper.readFromTable();
        ArrayList<Fastjson> and = new ArrayList<Fastjson>();
        Fastjson Fast = new Fastjson();


        for (int i = 0; i < list.size(); i++) {
             Fast.setArticle("Product");
            Fast.setArticleNo(list.get(i).getArticleNo());
            Fast.setQty(list.get(i).getQty());
            
            and.add(Fast);

        }

        Gson gson = new Gson();
   String jsonArray = gson.toJson(Fast);


        String jsonArray1 = gson.toJson(and);


        url = url.replace(" ", "%20");
        RetrofitTask task = new RetrofitTask<Object>(FastOrderFragment.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.FAST_ORDER, url, getActivity(), and);
        task.execute();


    }

    private void insert(Double stock) {
        String artNo = articleNo.getText().toString().toUpperCase().trim();
        String quantity = qty.getText().toString().trim();

//        if (!artNo.matches("[^A-Z0-9]*")) {
//
//            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//            builder.setMessage("Enter correct article no.")
//                    .setCancelable(false)
//                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//                            //do things
//                        }
//                    });
//            AlertDialog alert = builder.create();
//            alert.show();
//
//        } else
         if (artNo.isEmpty() || quantity.isEmpty()) {
            if (artNo.isEmpty() && quantity.isEmpty()) {
                articleNo.setError("Enter Article No.");
                qty.setError("Enter Quantity");
            } else if (quantity.isEmpty()) {
                qty.setError("Enter Quantity");
            } else {
                articleNo.setError("Enter Article No.");
            }

        } else {
            if (Integer.parseInt(quantity) == 0) {
                qty.setText("");
                qty.setError("0 cannot be added");
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Please add more than 0 items ")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //do things
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            } else {
                if (artNo.trim().equals(helper.getArticle())) {
                    articleNo.setText("");
                    qty.setText("");
                    articleNo.setError(null);
                    qty.setError(null);
                    checkAvailibility(artNo, idNum);
                    Log.d("SQLite", "article already exist so update it");
                    if (Long.valueOf(stock.intValue()) < Long.valueOf(quantity)) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setMessage("You can not add more than " + "" + stock.intValue() + "" + "units")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //do things
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();

                    } else {
                        long qtyUpdate = Long.valueOf(helper.getQuantity(artNo));
                        qtyUpdate += Long.valueOf(quantity);

                        Integer qtUp = (int) qtyUpdate;
                        if (qtUp <= stock.intValue()) {
                            long update = helper.updateRow(artNo, String.valueOf(qtyUpdate),url);
                            if (update > 0) {
                                Log.d("Insert", "Successfull");
                                articleNo.setError(null);
                                qty.setError(null);

                            } else {
                                Log.d("Insert", "Unsuccessfull");
                            }
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setMessage("You can add only " + stock.intValue() + " items")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            //do things
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();

                        }
                    }
                } else {
                    if (Integer.parseInt(quantity) <= stock.intValue()) {
                        FastOrderModel model = new FastOrderModel(artNo, quantity, stock,url);
                        Log.d("Article no", " " + artNo);
                        Log.d("quantity", " " + quantity);
                        Log.d("Stock", " " + stock);
                        articleNo.setText("");
                        qty.setText("");
                        articleNo.setError(null);
                        qty.setError(null);
                        checkAvailibility(artNo, idNum);
                        long insert = helper.insertInTable(model);
                        if (insert > 0) {
                            Log.d("Insert", "Successfull");
                        } else {
                            Log.d("Insert", "Unsuccessfull");
                        }
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setMessage("Already exists.Can add only " + stock.intValue() + " items")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //do things
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                }
            }

            showData();
        }
    }

    private void showData() {
        list = helper.readFromTable();
        fastList = list;
        adapter = new FastOrderAdapter(getActivity(), list);
        Log.d("List", "  " + list);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter.notifyDataSetChanged();
    }

    private void checkAvailibility(String itemId, String cid) {
        showProgreass();
        Log.e("Check", "Check Availibilty");
        String url = String.format(ServerConfigStage.GET_PRODUCT_DETAIL(), itemId, cid);
        RetrofitTask task = new RetrofitTask<Object>(FastOrderFragment.this, CommonUtility.HTTP_REQUEST_TYPE.GET,
                CommonUtility.CallerFunction.GET_PRODUCT_DETAIL, url, getActivity());
        articleNumber = itemId;
        task.execute();
    }


    public void showProgreass() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    @Override
    public void onRetrofitTaskComplete(Response<Object> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {
            if (response.body() != null) {
                if (_callerFunction == CommonUtility.CallerFunction.GET_PRODUCT_DETAIL) {
                    abc = (ProductDetail) (response.body());


                    if (abc.getStatus() > 0) {
                        if (abc.getStock() > 0) {
                            Gson gson = new Gson();
                            Double stock = abc.getStock();

                            url = abc.getURL();
                            Log.d("Stock", " " + stock);
                            stk = stock;
                            insert(stock);
                            articleNo.setError(null);
                            qty.setError(null);

                            String json = gson.toJson(response);

                            Log.e("Product status", " " + json);

                            Log.e("Check", "On Retrofit Result");


                            //availability.setText(stock);
                        } else {

                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setMessage("Sorry, not in stock.")

                                    .setCancelable(false)

                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                                        public void onClick(DialogInterface dialog, int id) {

                                            //do things

                                        }

                                    });
                            AlertDialog alert = builder.create();
                            articleNo.setText("");
                            qty.setText("");

                            alert.show();


                        }

                    } else {
                        Log.e("Check", "On Retrofit Result" + abc.getResponse());

                        AlertDialog.Builder builder = new AlertDialog.Builder(context);

                        builder.setMessage(abc.getResponse())

                                .setCancelable(false)

                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int id) {

                                        //do things

                                    }

                                });

                        AlertDialog alert = builder.create();
                        alert.show();
                        articleNo.setText("");
                        qty.setText("");


                        //Toast.makeText(context, response.body().get(0).getResponse(), Toast.LENGTH_SHORT).show();


                    }

                } else if (_callerFunction == CommonUtility.CallerFunction.FAST_ORDER) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);

                    builder.setMessage("Thanks for shopping with JBM !!")

                            .setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int id) {
                                    emptyList();
                                    startActivity(new Intent(getContext(), MainActivity.class));

                                }

                            });

                    AlertDialog alert = builder.create();
                    alert.show();
                }
            } else {
            }
        }
    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(getActivity(), "Fail to load Data", Toast.LENGTH_LONG).show();
        getActivity().finish();
    }

    public static void emptyList() {
        if (fastList != null) {
            fastList.clear();
            fastOrderHelper.deleteAll();
        } else {

        }
    }


}
