package com.jbm.jbmcustomer.fragment;

/**
 * Created by hp1 on 1/27/2017.
 */

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.adapter.AdapterOrderDetail;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.OrderDetails;

import retrofit.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderFragment extends Fragment implements RetrofitTaskListener<OrderDetails> {

    ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    AdapterOrderDetail adapter;

    OrderDetails addCartList;

    String  email,orderId;

    private LinearLayoutManager linearLayoutManager;



    public OrderFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.order_fragment, container, false);
        setHasOptionsMenu(true);

        recyclerView = (RecyclerView) view.findViewById(R.id.card_recycler_view);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);



        Bundle arguments = getArguments();
        if (arguments != null) {
            email = getArguments().getString("email");
            orderId = getArguments().getString("doctype");
            callLoginService(email,orderId);


        }

        return view;

    }






    public void callLoginService(String email,String orderId){
        showProgreass();

        String url = String.format(ServerConfigStage.GET_ORDER_DETAIL(),email,orderId);
        RetrofitTask task = new RetrofitTask<OrderDetails>(com.jbm.jbmcustomer.fragment.OrderFragment.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.ORDER_DETAILS, url,getActivity());
        task.execute();


    }

    public void showProgreass(){
        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    public void stopProgress(){
        if(progressDialog!=null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    @Override
    public void onRetrofitTaskComplete(Response<OrderDetails> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {
            if (response.body() != null) {
                addCartList    =response.body();

                if(addCartList.getStatus()>0)
                {
                    if (adapter == null) {
                        adapter = new AdapterOrderDetail(context, R.layout.row_detail, addCartList);

                        recyclerView.setAdapter( adapter );;

                    }

                    adapter.notifyDataSetChanged();


                }
                else {

                    Toast.makeText(context, addCartList.getResponse(), Toast.LENGTH_SHORT).show();

                }
            }

        }

    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(getActivity(),"Fail to load Data",Toast.LENGTH_LONG).show();

        getActivity().finish();
    }




}
