package com.jbm.jbmcustomer.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.adapter.AdapterNewProduct;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.Nproduct;

import java.util.ArrayList;
import java.util.List;

import retrofit.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewProduct extends Fragment implements RetrofitTaskListener<List<Nproduct>> {

    ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    AdapterNewProduct adapter;
    private boolean loading = true;
    List<Nproduct> categoryList = new ArrayList<Nproduct>();
    int pageIndex, pastVisiblesItems, visibleItemCount, totalItemCount;
    private GridLayoutManager gridLayoutManager;
    private static final String STARTING_TEXT = "Four Buttons Bottom Navigation";
    private NewProduct newProduct;
    boolean mbool = false;
    private SharedPreferences sharedPreference;
    private SharedPreferences.Editor editor;


    public NewProduct() {
        // Required empty public constructor
    }
    public static NewProduct newInstance(int text) {
        Bundle args = new Bundle();
        args.putInt(STARTING_TEXT, text);

        NewProduct sampleFragment = new NewProduct();
        sampleFragment.setArguments(args);
        return sampleFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_new_product, container, false);

        pageIndex = 0;
        recyclerView = (RecyclerView)view.findViewById(R.id.card_recycler_view);
        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("New Products");

        initViews();
        callLoginService();

        return view;

    }


    private void initViews(){


        recyclerView.setLayoutManager(gridLayoutManager);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if(dy > 0) //check for scroll down
                {
                    final LinearLayoutManager mLayoutManager = (LinearLayoutManager) recyclerView
                            .getLayoutManager();
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            callLoginService();

                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });
    }
    public void callLoginService(){
         if (!mbool) {
            showProgreass();
             mbool=true;

        } else {
            // other time your app loads
        }

        pageIndex = pageIndex + 1;
        String url = String.format(ServerConfigStage.GET_NEW_PRODUCT(),pageIndex+"");
        RetrofitTask task = new RetrofitTask<List<Nproduct>>(NewProduct.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.NEW_PRODUCT, url,getActivity());
        task.execute();


    }

    public void showProgreass(){
        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    public void stopProgress(){
        if(progressDialog!=null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    @Override
    public void onRetrofitTaskComplete(Response<List<Nproduct>> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {
            if (response.body() != null) {
                if(response.body().get(0).getStatus()>0) {

                    if ( response.body().size() == 20  ) {
                        loading = true;
                    }
                    else {
                        loading = false;
                    }
                        categoryList.addAll(response.body());


                    if (adapter == null) {
                        adapter = new AdapterNewProduct(context, R.layout.row_new_product, categoryList);
                        recyclerView.setAdapter(adapter);
                    }
                    adapter.setLoadingMore(loading);

                    adapter.notifyDataSetChanged();


                }
                else {

                    Toast.makeText(context, response.body().get(0).getResponse(), Toast.LENGTH_SHORT).show();

                }
            }

        }
    }


    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(getActivity(),"Fail to load Data",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDestroy() {
        stopProgress();
        super.onDestroy();
    }


}
