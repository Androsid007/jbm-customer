package com.jbm.jbmcustomer.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.jbm.jbmcustomer.DatabaseHelper.SQLiteHandler;
import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.activity.HomeActivity;
import com.jbm.jbmcustomer.activity.LoginActivity;
import com.jbm.jbmcustomer.activity.MainActivity;
import com.jbm.jbmcustomer.activity.OrderPlacedActivity;
import com.jbm.jbmcustomer.adapter.RemoveItemCommunication;
import com.jbm.jbmcustomer.adapter.ShippingAddressAdapter;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.Address;
import com.jbm.jbmcustomer.models.SessionModel;
import com.jbm.jbmcustomer.sharedpreference.SessionManager;

import java.util.List;

import retrofit.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddressFragment extends AppCompatActivity implements RetrofitTaskListener<Address> {

    ProgressDialog progressDialog;
    private RecyclerView shiprecyclerView;
    private ShippingAddressAdapter shippingAddressAdapter;
    private Address addressList;
    private  String email1,addressId;
    int pageIndex;
    private LinearLayoutManager shiplinearLayoutManager;
    private static final String STARTING_TEXT = "Four Buttons Bottom Navigation";
    private boolean mHasMenu;
    private Button placeOrder;
    private Toolbar toolbar;
    private SessionManager session;
    private SharedPreferences sharedPreferences;



    public AddressFragment() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_address);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        supportInvalidateOptionsMenu();
        toolbar.setTitle("Address");
        toolbar.setTitleTextColor(Color.WHITE);


        session = new SessionManager(this);
       // setHasOptionsMenu(true);

        placeOrder = (Button)findViewById(R.id.place_order);
       placeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (session.isLoggedIn()){

                    String addresID = "";
                    for (int i = 0; i < addressList.getAddress().size(); i++) {
                       // addressList.getAddress().get(i).setmSelect(false);
                     if (addressList.getAddress().get(i).ismSelect() == true)
                     {
                         addresID = addressList.getAddress().get(i).getAddId();
                         break;
                     }
                        else{
                         /*AlertDialog.Builder builder = new AlertDialog.Builder(AddressFragment.this);
                         builder.setMessage("Please select address")
                                 .setCancelable(false)
                                 .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                     public void onClick(DialogInterface dialog, int id) {
                                         //do things
                                     }
                                 });
                         AlertDialog alert = builder.create();
                         alert.show();*/

                     }
                    }
                    Intent intent = new Intent(AddressFragment.this,OrderPlacedActivity.class);
                    intent.putExtra("AddresID",addresID);
                    startActivity(intent);
                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(AddressFragment.this);
                    builder.setMessage("Please login to continue")
                            .setCancelable(false)
                            .setNegativeButton("NOT NOW", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            })
                            .setPositiveButton("LOGIN", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent intent = new Intent(AddressFragment.this,LoginActivity.class);
                                    intent.putExtra("fragment", 7);
                                    startActivity(intent);
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                }

            }
        });



        pageIndex = 0;
        shiprecyclerView = (RecyclerView) findViewById(R.id.ship_recycler);

        shiplinearLayoutManager = new LinearLayoutManager(AddressFragment.this);
        shiprecyclerView.setLayoutManager(shiplinearLayoutManager);
        SQLiteHandler sqLiteHandler = new SQLiteHandler(AddressFragment.this);
        List<SessionModel> sessionModelList = sqLiteHandler.getUserDetails();

        for (SessionModel sm : sessionModelList){
            email1 = sm.getEmail();
        }

        callLoginService(email1);



    }

    public void sendAddId(){

    }

    public void callLoginService(String email){
        showProgreass();
        pageIndex = pageIndex + 1;
        String url = String.format(ServerConfigStage.GET_ADDRESS(),email);
        RetrofitTask task = new RetrofitTask<Address>(AddressFragment.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.GET_ADDRESS, url,this);
        task.execute();


    }

    public void showProgreass(){
        progressDialog=new ProgressDialog(AddressFragment.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    public void stopProgress(){
        if(progressDialog!=null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    @Override
    public void onRetrofitTaskComplete(Response<Address> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        int i=0;
        if (response.isSuccess()) {
            if (response.body() != null) {
                if(response.body().getStatus()>0) {

                   addressList = response.body();
                    if (addressList.getAddress().size()==0){
                        AlertDialog.Builder builder = new AlertDialog.Builder(AddressFragment.this);
                        builder.setMessage("No shipping address found.Please contact JBM")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent intent = new Intent(AddressFragment.this, HomeActivity.class);
                                        intent.putExtra("fragment",3);
                                        startActivity(intent);
                                        startActivity(intent);
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }else{
                        if (shippingAddressAdapter == null) {
                            shippingAddressAdapter = new ShippingAddressAdapter(context, R.layout.row_address_layout, addressList);
                            shiprecyclerView.setAdapter(shippingAddressAdapter);

                        }
                        shippingAddressAdapter.notifyDataSetChanged();

                    }
                }
                    else {

                        Toast.makeText(context, response.body().getResponse(), Toast.LENGTH_SHORT).show();

                }


            }

        }

    }


    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(AddressFragment.this,"Fail to load Data",Toast.LENGTH_LONG).show();

        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                // todo: goto back activity from here

                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }




}
