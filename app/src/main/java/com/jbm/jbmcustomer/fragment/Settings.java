package com.jbm.jbmcustomer.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jbm.jbmcustomer.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class Settings extends Fragment {
    private static final String STARTING_TEXT = "Four Buttons Bottom Navigation";


    public Settings() {
        // Required empty public constructor
    }

    public static Settings newInstance(int text) {
        Bundle args = new Bundle();
        args.putInt(STARTING_TEXT, text);

        Settings sampleFragment = new Settings();
        sampleFragment.setArguments(args);
        return sampleFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Settings");
        return view;

    }

}
