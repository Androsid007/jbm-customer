package com.jbm.jbmcustomer.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jbm.jbmcustomer.DatabaseHelper.SQLiteHandler;
import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.activity.HomeActivity;
import com.jbm.jbmcustomer.adapter.RemoveItemCommunication;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.Address;
import com.jbm.jbmcustomer.models.Checkout;
import com.jbm.jbmcustomer.models.SessionModel;
import com.jbm.jbmcustomer.sharedpreference.SessionManager;

import java.util.List;

import retrofit.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderPlacedFragment extends Fragment implements RetrofitTaskListener<Checkout> {

    ProgressDialog progressDialog;
    private Address addressList;
    private  String email1,addressId,orderNo,name;
    private static final String STARTING_TEXT = "Four Buttons Bottom Navigation";
    private boolean mHasMenu;

    private RemoveItemCommunication communication;
    private SessionManager session;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private TextView orderPlacedTxt,orderNoTxt;
    private Button continueBtn;



    public OrderPlacedFragment() {
        // Required empty public constructor
    }
    public static OrderPlacedFragment newInstance(int text) {
        Bundle args = new Bundle();
        args.putInt(STARTING_TEXT, text);

        OrderPlacedFragment orderPlacedFragment = new OrderPlacedFragment();
        orderPlacedFragment.setArguments(args);
        return orderPlacedFragment;
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order_placed, container, false);

        orderPlacedTxt = (TextView)view.findViewById(R.id.orderPlacedTxt);
        continueBtn=(Button)view.findViewById(R.id.continueBtn);
        orderNoTxt = (TextView)view.findViewById(R.id.orderNoTxt);

        sharedPreferences = getActivity().getSharedPreferences("CART",0);
        editor = sharedPreferences.edit();

        session = new SessionManager(getActivity());
        setHasOptionsMenu(true);
        addressId = getArguments().getString("AddresID");

        SQLiteHandler sqLiteHandler = new SQLiteHandler(getActivity());
        List<SessionModel> sessionModelList = sqLiteHandler.getUserDetails();

        for (SessionModel sm : sessionModelList){
            email1 = sm.getEmail();
            name = sm.getName();
        }

        getOrderPlaced(email1,addressId);

        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), HomeActivity.class);
                intent.putExtra("fragment",3);
                startActivity(intent);
            }
        });

        return view;

    }

    public void getOrderPlaced(String email,String addId){
        showProgreass();
        String url = String.format(ServerConfigStage.CHECKOUT(),email,addId);
        RetrofitTask task = new RetrofitTask<Checkout>(OrderPlacedFragment.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.CHECKOUT, url,getActivity());
        task.execute();


    }

    public void showProgreass(){
        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    public void stopProgress(){
        if(progressDialog!=null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    @Override
    public void onRetrofitTaskComplete(Response<Checkout> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        int i=0;
        if (response.isSuccess()) {
            if (response.body() != null) {
                if(response.body().getStatus()==0) {
               orderNo  = response.body().getAppOrderNo();
                    editor.putInt("CART_ITEMS",i);
                    editor.commit();
                    Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
                  orderPlacedTxt.setText("Hello"+" "+name+" "+"Thank you for your order.We'll send a confirmation when your order processed.We hope to see you again soon.Thanks");
                    orderNoTxt.setText("Your orderId is :"+" "+orderNo);

                }
                else {
                    orderPlacedTxt.setText(response.body().getResponse());

                    Toast.makeText(context, response.body().getResponse(), Toast.LENGTH_SHORT).show();

                }
            }

        }

    }


    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(getActivity(),"Fail to load Data",Toast.LENGTH_LONG).show();

        getActivity().finish();
    }




}
