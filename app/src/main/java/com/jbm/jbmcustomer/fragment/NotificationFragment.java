package com.jbm.jbmcustomer.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jbm.jbmcustomer.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment {

    private static final String STARTING_TEXT = "Four Buttons Bottom Navigation";


    public NotificationFragment() {
        // Required empty public constructor
    }

    public static NotificationFragment newInstance(int text) {
        Bundle args = new Bundle();
        args.putInt(STARTING_TEXT, text);

        NotificationFragment notificationFragment = new NotificationFragment();
        notificationFragment.setArguments(args);
        return notificationFragment;
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Notification");

        return inflater.inflate(R.layout.fragment_notification, container, false);
    }


}
