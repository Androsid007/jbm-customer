package com.jbm.jbmcustomer.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.adapter.AdapterPromotion;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.Promotion;

import java.util.ArrayList;
import java.util.List;

import retrofit.Response;

;

/**
 * A simple {@link Fragment} subclass.
 */
public class PromotionFragment extends Fragment implements RetrofitTaskListener<List<Promotion>> {

    ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    AdapterPromotion adapter;
    List<Promotion> categoryList = new ArrayList<Promotion>();
    private static final String STARTING_TEXT = "Four Buttons Bottom Navigation";

    public PromotionFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.promotion_layout, container, false);


        recyclerView = (RecyclerView) view.findViewById(R.id.card_recycler_view);



        initViews();
        //callLoginService();

        return view;
    }

    private void initViews() {
    }

    public void callLoginService() {
        showProgreass();

        String url = String.format(ServerConfigStage.GET_PROMOTION());
        RetrofitTask task = new RetrofitTask<List<Promotion>>(PromotionFragment.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.PROMOTION_PRODUCT, url, getActivity());
        task.execute();
    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    @Override
    public void onRetrofitTaskComplete(Response<List<Promotion>> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {
            if (response.body() != null) {
                if (response.body().get(0).getStatus() > 0) {

                    categoryList.addAll(response.body());
                    //JsonArray jsonArray = new JsonArray(categoryList);

                    if (adapter == null) {
                        adapter = new AdapterPromotion(context, R.layout.row_promotion, categoryList);
                        recyclerView.setAdapter(adapter);
                        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                        recyclerView.setLayoutManager(layoutManager);
                    }

                    adapter.notifyDataSetChanged();
                } else {

                    Toast.makeText(context, response.body().get(0).getResponse(), Toast.LENGTH_SHORT).show();

                }
            }
        }
    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(getActivity(), "Fail to load Data", Toast.LENGTH_LONG).show();
        getActivity().finish();
    }

    public static Fragment newInstance() {
        Fragment fragment = new PromotionFragment();
        return fragment;
    }
}
