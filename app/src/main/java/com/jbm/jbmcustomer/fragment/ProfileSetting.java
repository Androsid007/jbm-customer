package com.jbm.jbmcustomer.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jbm.jbmcustomer.DatabaseHelper.SQLiteHandler;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.activity.AddressActivity;
import com.jbm.jbmcustomer.activity.ChangePassword;
import com.jbm.jbmcustomer.activity.LoginActivity;
import com.jbm.jbmcustomer.activity.OrderHistory;
import com.jbm.jbmcustomer.activity.OrderHistoryHome;
import com.jbm.jbmcustomer.activity.OtpScreen;
import com.jbm.jbmcustomer.activity.SettingActivity;
import com.jbm.jbmcustomer.activity.SignUpActivity;
import com.jbm.jbmcustomer.activity.TrackOrderActivity;
import com.jbm.jbmcustomer.sharedpreference.SessionManager;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileSetting extends Fragment implements View.OnClickListener {
    private TextView editProfile, trackOrder, setting, address, orderHistory, login, register,nameTxt;
    private Intent intent;
    private LinearLayout linLayout;
    private SessionManager session;
    private SQLiteHandler db;
    private String name,idNum;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private FastOrderFragment fastOrderFragment = new FastOrderFragment();
    private ImageView logoutImg;


    public ProfileSetting() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile_setting, container, false);
        db = new SQLiteHandler(getActivity());

        session = new SessionManager(getActivity());
        sharedPreferences = getActivity().getSharedPreferences("NAME",0);
        linLayout = (LinearLayout) view.findViewById(R.id.linLayout);

        logoutImg = (ImageView)view.findViewById(R.id.profile_photo);


       name = sharedPreferences.getString("USER_NAME","");
        idNum = sharedPreferences.getString("ID_NUMBER","");
        editor = sharedPreferences.edit();

        nameTxt = (TextView)view.findViewById(R.id.hello_text);

        logoutImg.setOnClickListener(this);



            editProfile = (TextView) view.findViewById(R.id.edit_text);
            editProfile.setOnClickListener(this);



        trackOrder = (TextView) view.findViewById(R.id.track_text);
        trackOrder.setOnClickListener(this);



            setting = (TextView) view.findViewById(R.id.setting_text);
            setting.setOnClickListener(this);

            address = (TextView) view.findViewById(R.id.address_text);
            address.setOnClickListener(this);

            orderHistory = (TextView) view.findViewById(R.id.order_text);
            orderHistory.setOnClickListener(this);

            login = (TextView) view.findViewById(R.id.login_profile);
            login.setOnClickListener(this);

            register = (TextView) view.findViewById(R.id.create_profile);
            register.setOnClickListener(this);

        if (session.isLoggedIn()) {

            linLayout.setVisibility(view.GONE);
            orderHistory.setVisibility(View.VISIBLE);
            trackOrder.setVisibility(View.VISIBLE);
            address.setVisibility(View.VISIBLE);
            nameTxt.setText("Hello"+" "+name);
            logoutImg.setVisibility(View.VISIBLE);

        }else {

            logoutImg.setVisibility(View.GONE);
            linLayout.setVisibility(view.VISIBLE);
            nameTxt.setText("Hello!");
        }

        return view;
        }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.edit_text:
                if (!session.isLoggedIn()){
                    Intent intent1 = new Intent(getActivity(), LoginActivity.class);
                    intent1.putExtra("fragment",3);
                    startActivity(intent1);
                }else{
                Intent intent = new Intent(getActivity(), ChangePassword.class);
                startActivity(intent);
                }


                break;

            case R.id.order_text:
                if (!session.isLoggedIn()){
                    Intent intent1 = new Intent(getActivity(), LoginActivity.class);
                    intent1.putExtra("fragment",3);
                    startActivity(intent1);
                }else{
                    Intent intent1 = new Intent(getActivity(), OrderHistory.class);
                    startActivity(intent1);
                }

                break;
            case R.id.track_text:
                if (!session.isLoggedIn()){
                    Intent intent1 = new Intent(getActivity(), LoginActivity.class);
                    intent1.putExtra("fragment",3);
                    startActivity(intent1);
                }else{
                    Intent intent1 = new Intent(getActivity(), TrackOrderActivity.class);
                    startActivity(intent1);
                }
                break;
            case R.id.address_text:

                if (!session.isLoggedIn()){
                    Intent intent1 = new Intent(getActivity(), LoginActivity.class);
                    intent1.putExtra("fragment",3);
                    startActivity(intent1);
                }else{
                    Intent intent1 = new Intent(getActivity(), AddressActivity.class);
                    startActivity(intent1);
                }
                break;
            case R.id.setting_text:
                if (!session.isLoggedIn()){
                    Intent intent1 = new Intent(getActivity(), LoginActivity.class);
                    intent1.putExtra("fragment",3);
                    startActivity(intent1);
                }else{
                    Intent intent1 = new Intent(getActivity(), SettingActivity.class);
                    startActivity(intent1);
                }
                break;

            case R.id.login_profile:
                intent = new Intent(getContext(), LoginActivity.class);
                intent.putExtra("fragment", 3);
                startActivity(intent);
                break;

            case R.id.create_profile:
                intent = new Intent(getContext(), SignUpActivity.class);
                startActivity(intent);
                break;

            case R.id.profile_photo:
                logoutUser();
                break;
        }
    }

    private void logoutUser() {
        session.setLogin(false);

        db.deleteUsers();
        editor.putString("ID_NUMBER","0");
        editor.commit();
        fastOrderFragment.emptyList();

        // Launching the login activity
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.putExtra("fragment",3);  startActivity(intent);
        getActivity().finish();
    }


}
