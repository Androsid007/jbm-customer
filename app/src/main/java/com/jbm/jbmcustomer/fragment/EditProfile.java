package com.jbm.jbmcustomer.fragment;

/**
 * Created by hp1 on 1/27/2017.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jbm.jbmcustomer.DatabaseHelper.SQLiteHandler;
import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.activity.OtpScreen;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.Example;
import com.jbm.jbmcustomer.models.SessionModel;
import com.jbm.jbmcustomer.sharedpreference.SessionManager;

import java.util.List;

import retrofit.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfile extends Fragment implements RetrofitTaskListener<List<Example>> {

    private Button otp;
    ProgressDialog progressDialog;
    EditText email;
    private String confirmedEmail;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    Gson gson = new Gson();
    String matchEmail;
    private SessionManager session;


    private LinearLayoutManager linearLayoutManager;



    public EditProfile() {
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.change_password, container, false);
        setHasOptionsMenu(true);
        session = new SessionManager(getActivity());
        email = (EditText) view.findViewById(R.id.email);
        otp = (Button) view.findViewById(R.id.send_otp);

        SQLiteHandler sqLiteHandler = new SQLiteHandler(getActivity());
        List<SessionModel> sessionModelList = sqLiteHandler.getUserDetails();
        if (session.isLoggedIn()){
            for (SessionModel sm : sessionModelList) {
                matchEmail = sm.getEmail();
                email.setText(matchEmail);

                otp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        matchEmail = email.getText().toString();
                        if (android.util.Patterns.EMAIL_ADDRESS.matcher(matchEmail).matches() || !matchEmail.isEmpty()) {
                            confirmedEmail = matchEmail;

                            callLoginService(matchEmail);
                        } else {
                            email.setError("Enter valid email");

                        }
                    }
                });
            }
        }else{
            otp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    matchEmail = email.getText().toString();
                    if (android.util.Patterns.EMAIL_ADDRESS.matcher(matchEmail).matches() || !matchEmail.isEmpty()) {
                        confirmedEmail = matchEmail;

                        callLoginService(matchEmail);
                    } else {
                        email.setError("Enter valid email");

                    }
                }
            });
            }
        return view;

    }







    public void callLoginService(String email){
        showProgreass();
        String url = String.format(ServerConfigStage.GET_USER_INFO(),matchEmail);
        RetrofitTask task = new RetrofitTask<List<Example>>(EditProfile.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.GET_FORGET_DETAIL, url, getActivity());
        task.execute();


    }

    public void showProgreass(){
        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    public void stopProgress(){
        if(progressDialog!=null && progressDialog.isShowing())
            progressDialog.cancel();
    }


@Override
public void onRetrofitTaskComplete(Response<List<Example>> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {
        ;
        if (response.body() != null) {
        if(response.body().size()>0) {
        if(response.body().get(0).getStatus()>0)
        {

        String json = gson.toJson(response);
        System.out.println(json);
        Intent intent = new Intent(context, OtpScreen.class);
        intent.putExtra("otpNumber", response.body().get(0).getOTP());
        intent.putExtra("cardName", response.body().get(0).getCardName());
        intent.putExtra("cardCord", response.body().get(0).getCardcode());
        intent.putExtra("email", confirmedEmail);
        startActivity(intent);

        }
        else {
        String json = gson.toJson(response);
        System.out.println(json);

        Toast.makeText(context, response.body().get(0).getResposnse(), Toast.LENGTH_SHORT).show();

        }
        }

        }
        }

        }

@Override
public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(getActivity(),"Fail to load Data",Toast.LENGTH_LONG).show();

        getActivity().finish();
        }

        }
