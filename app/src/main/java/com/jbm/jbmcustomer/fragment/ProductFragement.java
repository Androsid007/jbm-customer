package com.jbm.jbmcustomer.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.adapter.AdapterProductMaster;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.Product;

import java.util.ArrayList;
import java.util.List;

import retrofit.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductFragement extends Fragment implements RetrofitTaskListener<List<Product>> {

    ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    AdapterProductMaster adapter;

    private boolean loading = true;
    List<Product> productList = new ArrayList<Product>();
    int pageIndex, pastVisiblesItems, visibleItemCount, totalItemCount;
    private GridLayoutManager gridLayoutManager;
    String categoryID;
    private GestureDetector mGestureDetector;
    private static final String STARTING_TEXT = "Four Buttons Bottom Navigation";
    private boolean mHasMenu;
    private Toolbar toolbar;
    boolean mboolean = false;
    private SharedPreferences sharedPreference;
    private SharedPreferences.Editor editor;





    public ProductFragement() {
        // Required empty public constructor
    }

    public static ProductFragement newInstance(int text) {
        Bundle args = new Bundle();
        args.putInt(STARTING_TEXT, text);

        ProductFragement sampleFragment = new ProductFragement();
        sampleFragment.setArguments(args);
        return sampleFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragement_product, container, false);

        setHasOptionsMenu(true);

        pageIndex = 0;
        recyclerView = (RecyclerView) view.findViewById(R.id.card_recycler_view);
        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        initViews();


        Bundle arguments = getArguments();
        if (arguments != null) {
            categoryID = getArguments().getString("categoryID");
            callLoginService(categoryID);


        }

        return view;

    }


    private void initViews() {


        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutManager(gridLayoutManager);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    final LinearLayoutManager mLayoutManager = (LinearLayoutManager) recyclerView
                            .getLayoutManager();
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            callLoginService(categoryID);

                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });


    }

    public void callLoginService(String categoryID) {
        SharedPreferences settings = getActivity().getSharedPreferences("PREFS_NAME", 0);
        mboolean = settings.getBoolean("FIRST_RUN", false);
        if (!mboolean) {
            showProgreass();
            // do the thing for the first time
            settings = getActivity().getSharedPreferences("PREFS_NAME", 0);
             editor = settings.edit();
            editor.putBoolean("FIRST_RUN", true);
            editor.commit();
        } else {
            // other time your app loads
        }


        pageIndex = pageIndex + 1;
        String url = String.format(ServerConfigStage.PRODUCT_MASTER_API(), pageIndex + "", categoryID);
        RetrofitTask task = new RetrofitTask<List<Product>>(ProductFragement.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.PRODUCT_ITEM_MASTER, url, getActivity());
        task.execute();


    }


    public void showProgreass() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    @Override
    public void onRetrofitTaskComplete(Response<List<Product>> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {
            if (response.body() != null) {
                if (response.body().get(0).getStatus() > 0) {

                    if (response.body().size() == 20) {
                        loading = true;
                    } else {
                        loading = false;
                    }


                    productList.addAll(response.body());


                  /*  if (adapter == null) {
                        adapter = new AdapterProductMaster(context, R.layout.row_news_message_board_card, productList);
                        recyclerView.setAdapter(adapter);


                    }*/

                    adapter.notifyDataSetChanged();



                } else {

                    Toast.makeText(context, response.body().get(0).getResponse(), Toast.LENGTH_SHORT).show();

                }
            }
            else{
                Toast.makeText(context, "No Product Found", Toast.LENGTH_SHORT).show();

            }

        }

    }




    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(getActivity(), "Fail to load Data", Toast.LENGTH_LONG).show();

        getActivity().finish();
    }


}
