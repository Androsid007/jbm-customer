package com.jbm.jbmcustomer.fragment;


import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.adapter.AdapterCategoryItem;
import com.jbm.jbmcustomer.adapter.AdapterCategorySearchItem;
import com.jbm.jbmcustomer.adapter.CategoryListAdapter;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.Category;

import java.util.ArrayList;
import java.util.List;

import retrofit.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements RetrofitTaskListener<List<Category>>{

    ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    AdapterCategoryItem adapter;
    AdapterCategorySearchItem searchAdapter;
    private CategoryListAdapter categoryListAdapter;
    private ListView categoryListView;
    private boolean loading = true;
    ArrayList<Category> categoryList = new ArrayList<Category>();
    ArrayList<Category> categoryListSearch = new ArrayList<Category>();
    int pageIndex, pastVisiblesItems, visibleItemCount, totalItemCount,searchPageIndex;
    private GridLayoutManager gridLayoutManager;
    private static final String STARTING_TEXT = "Four Buttons Bottom Navigation";
    private Context context;
    private SearchView searchView;
    private MenuItem searchMenuItem;
    private String sbmtQry;
    private String blockCharacterSet = "*qwertyuiopasdfghjklzxcvbnm1234567890QWERTYUIOPASDFGHJKLZXCVBNM";

    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && !blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };


    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance(Context context,int text) {
        Bundle args = new Bundle();
        args.putInt(STARTING_TEXT, text);
        HomeFragment sampleFragment = new HomeFragment();
        sampleFragment.setArguments(args);
        FragmentTransaction ft =((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
        ft.addToBackStack("fragHome");
        ft.commit();


        return sampleFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Products Category");
        categoryListView = (ListView)view.findViewById(R.id.friend_list);
        pageIndex = 0;
        searchPageIndex=0;
        recyclerView = (RecyclerView) view.findViewById(R.id.card_recycler_view);
        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        categoryListView.setVisibility(View.GONE);
     // String country = getActivity().getResources().getConfiguration().locale.getDisplayCountry();

    /*    String country = getActivity().getResources().getConfiguration().locale.getDisplayCountry();
        Toast.makeText(getActivity(),country,Toast.LENGTH_LONG).show();
*/
        initViews();
        callLoginService(20);

        return view;

    }

    @Override
    public void onStart(){
        super.onStart();
        categoryListView.setVisibility(View.GONE);
    }


    private void initViews() {


        recyclerView.setLayoutManager(gridLayoutManager);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    final LinearLayoutManager mLayoutManager = (LinearLayoutManager) recyclerView
                            .getLayoutManager();
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            callLoginService(20);

                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });




    }

    public void callLoginService(int size) {
        // showProgreass();
        pageIndex = pageIndex + 1;
        String url = String.format(ServerConfigStage.ITEM_CATEGORY_API(), pageIndex + "",size);
        RetrofitTask task = new RetrofitTask<List<Category>>(HomeFragment.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.GET_CATEGORY_ITEM, url, getActivity());
        task.execute();


    }

    public void callSearchCategory(String query) {
        // showProgreass();
        searchPageIndex = searchPageIndex + 1;
        String url = String.format(ServerConfigStage.ITEM_SEARCH_CATEGORY_API(), searchPageIndex + "",query);
        RetrofitTask task = new RetrofitTask<List<Category>>(HomeFragment.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.GET_SEARCH_CATEGORY_ITEM, url, getActivity());
        task.execute();


    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    @Override
    public void onRetrofitTaskComplete(Response<List<Category>> response, Context context, CommonUtility.CallerFunction _callerFunction) {

        stopProgress();
        if (response.isSuccess()) {
            if (response.body() != null) {
                if (_callerFunction == CommonUtility.CallerFunction.GET_CATEGORY_ITEM) {
                    if (response.body().get(0).getStatus() > 0) {

                        if (response.body().size() == 20) {
                            loading = true;
                        } else {
                            loading = false;
                            categoryListSearch.addAll(response.body());
                        }
                      if (categoryList.size()==1){
                            categoryList.clear();
                        }


                        categoryList.addAll(response.body());
//

                        if (adapter == null ) {
                            adapter = new AdapterCategoryItem(context, R.layout.row_news_message_board_card, categoryList);
                            recyclerView.setAdapter(adapter);

                        }

                        adapter.setLoadingMore(loading);

                        adapter.notifyDataSetChanged();


                    }else{
                        Toast.makeText(context, response.body().get(0).getResponse(), Toast.LENGTH_SHORT).show();
                    }
                } else if (_callerFunction == CommonUtility.CallerFunction.GET_SEARCH_CATEGORY_ITEM) {
                    if (response.body().get(0).getStatus() > 0) {

                        if (response.body().size()==20){
                            loading = true;
                        }else {
                            loading = false;
                        }

                        if(!categoryList.isEmpty()){
                            categoryList.clear();
                        }

                        categoryList.addAll(response.body());

                        if (adapter == null ) {
                            adapter = new AdapterCategoryItem(context, R.layout.row_adapter_home, categoryList);
                            recyclerView.setAdapter(adapter);

                        }

                        adapter.setLoadingMore(loading);

                        adapter.notifyDataSetChanged();


                    } else {
                        Toast.makeText(context, response.body().get(0).getResponse(), Toast.LENGTH_SHORT).show();
                    }

                }
            }
        }
    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {

        //stopProgress();
        Toast.makeText(getActivity(), "Fail to load Data", Toast.LENGTH_LONG).show();

        getActivity().finish();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_category, menu);
        super.onCreateOptionsMenu(menu,inflater);

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchItem.getActionView();
        final SearchView.SearchAutoComplete searchEditText = (SearchView.SearchAutoComplete) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchView.findViewById(android.support.v7.appcompat.R.id.search_plate)
                .setBackgroundColor(getResources().getColor(android.R.color.white));
        searchEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps(),new InputFilter.LengthFilter(10)});

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pageIndex=0;
                callLoginService(500);

            }
        });

        ImageView closeButton = (ImageView)searchView.findViewById(R.id.search_close_btn);
        closeButton.setBackgroundColor(getResources().getColor(R.color.bottombar));

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchAdapter=null;
                adapter=null;
                pageIndex=0;
                categoryListSearch.clear();
                categoryList.clear();
                callLoginService(20);
                categoryListView.setVisibility(View.GONE);
                searchView.onActionViewCollapsed();
            }
        });


        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                categoryListView.setVisibility(View.GONE);
                searchPageIndex=0;
                callSearchCategory(query);
               //searchView.onActionViewCollapsed();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                sbmtQry = newText;
                if (newText.contains("*")){
                    sbmtQry = "*";
                }

                if (newText.equals("")){
                    categoryListView.setVisibility(View.GONE);
                }else {

                    categoryListAdapter.getFilter().filter(newText);
                    categoryListView.setVisibility(View.VISIBLE);
                }
                return true;
            }
        });
        categoryListAdapter = new CategoryListAdapter(getActivity(), categoryListSearch);
        categoryListView.setAdapter(categoryListAdapter);
        categoryListView.setTextFilterEnabled(false);

        categoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                categoryListView.setVisibility(View.GONE);
                categoryListSearch.clear();
                searchPageIndex=0;
                callSearchCategory(sbmtQry);

            }
        });
    }



    @Override
    public void onDestroy() {
        stopProgress();
        super.onDestroy();
    }
}