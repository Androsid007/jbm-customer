package com.jbm.jbmcustomer.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.jbm.jbmcustomer.DatabaseHelper.SQLiteHandler;
import com.jbm.jbmcustomer.NetworkService.RetrofitTask;
import com.jbm.jbmcustomer.NetworkService.RetrofitTaskListener;
import com.jbm.jbmcustomer.R;
import com.jbm.jbmcustomer.activity.HomeActivity;
import com.jbm.jbmcustomer.activity.Profile;
import com.jbm.jbmcustomer.adapter.BillingAddressAdapter;
import com.jbm.jbmcustomer.adapter.ShippingAddressAdapter;
import com.jbm.jbmcustomer.common.CommonUtility;
import com.jbm.jbmcustomer.common.ServerConfigStage;
import com.jbm.jbmcustomer.models.Address;
import com.jbm.jbmcustomer.models.SessionModel;
import com.jbm.jbmcustomer.sharedpreference.SessionManager;

import java.util.List;

import retrofit.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileAddressFragment extends Fragment implements RetrofitTaskListener<Address> {

    ProgressDialog progressDialog;
    private RecyclerView shiprecyclerView;
    private BillingAddressAdapter billingAddressAdapter;
    private Address addressList;
    private  String email1,addressId;
    int pageIndex;
    private LinearLayoutManager shiplinearLayoutManager;
    private static final String STARTING_TEXT = "Four Buttons Bottom Navigation";
    private SessionManager session;




    public ProfileAddressFragment() {
        // Required empty public constructor
    }
    public static ProfileAddressFragment newInstance(int text) {
        Bundle args = new Bundle();
        args.putInt(STARTING_TEXT, text);

        ProfileAddressFragment sampleFragment = new ProfileAddressFragment();
        sampleFragment.setArguments(args);
        return sampleFragment;
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile_address, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        session = new SessionManager(getActivity());


        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Address");

        pageIndex = 0;
        shiprecyclerView = (RecyclerView) view.findViewById(R.id.ship_recycler);

        shiplinearLayoutManager = new LinearLayoutManager(getActivity());
        shiprecyclerView.setLayoutManager(shiplinearLayoutManager);
        SQLiteHandler sqLiteHandler = new SQLiteHandler(getActivity());
        List<SessionModel> sessionModelList = sqLiteHandler.getUserDetails();

        for (SessionModel sm : sessionModelList){
            email1 = sm.getEmail();
        }

        callLoginService(email1);

        return view;

    }

    public void sendAddId(){

    }

    public void callLoginService(String email){
        showProgreass();
        pageIndex = pageIndex + 1;
        String url = String.format(ServerConfigStage.GET_ADDRESS(),email);
        RetrofitTask task = new RetrofitTask<Address>(ProfileAddressFragment.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.GET_ADDRESS, url,getActivity());
        task.execute();


    }

    public void showProgreass(){
        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    public void stopProgress(){
        if(progressDialog!=null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    @Override
    public void onRetrofitTaskComplete(Response<Address> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        int i=0;
        if (response.isSuccess()) {
            if (response.body() != null) {
                if(response.body().getStatus()>0) {

                    addressList = response.body();
                    if (addressList.getAddress().size()==0){
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("No shipping address found.Please contact JBM")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent intent = new Intent(getActivity(), HomeActivity.class);
                                        intent.putExtra("fragment",3);
                                        startActivity(intent);
                                        startActivity(intent);
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }else{

                    if (billingAddressAdapter == null) {
                        billingAddressAdapter = new BillingAddressAdapter(context, R.layout.row_address_profile_layout, addressList);
                        shiprecyclerView.setAdapter(billingAddressAdapter);

                    }
                    billingAddressAdapter.notifyDataSetChanged();


                }
                }
                else {

                    Toast.makeText(context, response.body().getResponse(), Toast.LENGTH_SHORT).show();

                }
            }

        }

    }


    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(getActivity(),"Fail to load Data",Toast.LENGTH_LONG).show();

        getActivity().finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here

             /*   Intent intent = new Intent(getActivity(), Profile.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                getActivity().finish();
                return true;*/

                Intent intent = new Intent(getActivity(), Profile.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                getActivity().finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }


}
