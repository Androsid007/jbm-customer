
package com.jbm.jbmcustomer.DatabaseHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.jbm.jbmcustomer.models.FastOrderModel;

import java.util.ArrayList;
import java.util.List;



public class FastOrderHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "fast_order_db";
    private static final String TABLE_NAME = "fast_order_table";
    private static final String ARTICLE_NO = "article_no";
    private static final String QTY = "quantity";
    private static final String STOCK = "stock";
    private static final String URL = "url";
    private static final String CREATE_TABLE = " CREATE TABLE " + TABLE_NAME + " ("
            + ARTICLE_NO + " VARCHAR(20) PRIMARY KEY  , "
            + QTY + " VARCHAR(20) ," + STOCK + " VARCHAR(20), " + URL + " VARCHAR(20)) ;";
    private static final int VERSION = 4;
    private Context context;
    private SQLiteDatabase db;

    public FastOrderHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
        Log.d("FastOrder", " Constructor called");
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TABLE);
        Log.d("Fast Order Database", "onCreate() called");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    //CRUD Operations on Database

    //Get Quantity
    public String getQuantity(String articleNo) {
        db = this.getReadableDatabase();
        String qty = null;
        String query = " SELECT * FROM " + TABLE_NAME + " WHERE " + ARTICLE_NO + " = '" + articleNo + "';";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                qty = cursor.getString(1);

            } while (cursor.moveToNext());


        }
        return qty;

    }

    //Get Article No
    public String getArticle() {
        db = this.getReadableDatabase();
        String artNo = null;
        String query = " SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                artNo = cursor.getString(0);
            } while (cursor.moveToNext());


        }
        return artNo;

    }

    public String getUrl() {
        db = this.getReadableDatabase();
        String url = null;
        String query = " SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                url = cursor.getString(3);
            } while (cursor.moveToNext());


        }
        return url;

    }

    //Read full Table
    public List<FastOrderModel> readFromTable() {
        db = this.getReadableDatabase();
        List<FastOrderModel> list = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                FastOrderModel model = new FastOrderModel();
                model.setArticleNo(cursor.getString(0));
                model.setQty(cursor.getString(1));
                model.setStock(cursor.getDouble(2));
                model.setUrl(cursor.getString(3));
                list.add(model);
            } while (cursor.moveToNext());
        }
        return list;
    }

    //Delete Row
    public void deleteRow(String article) {
        db = this.getWritableDatabase();
        //  String where = ARTICLE_NO + " = ?";
        // String[] args = new String[]{article};

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_NAME + " WHERE " + ARTICLE_NO + "='" + article + "'");
        Log.d("SQL", "Deleted" + article);
        db.close();
        // db.rawQuery(query, null);
        //db.delete(TABLE_NAME, where, args);
        db.close();
    }

    //Delete All
    public void deleteAll() {
        db = this.getWritableDatabase();
        // String query = " DELETE * FROM " + TABLE_NAME;
        Log.d("SQL", "Deleted All");
        db.delete(TABLE_NAME, null, null);
        db.close();
    }

    //Update Row
    public long updateRow(String articleNo, String qty,String url) {
        db = this.getWritableDatabase();
        String where = ARTICLE_NO + " = ? ";
        String[] args = new String[]{articleNo};
        ContentValues cv = new ContentValues();
        cv.put(ARTICLE_NO, articleNo);
        cv.put(QTY, qty);
        cv.put(URL,url);
        long id = db.update(TABLE_NAME, cv, where, args);

        return id;
    }

    //Insert Row
    public long insertInTable(FastOrderModel model) {
        db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ARTICLE_NO, model.getArticleNo());
        contentValues.put(QTY, model.getQty());
        contentValues.put(STOCK, model.getStock());
        contentValues.put(URL, model.getUrl());
        long insert = db.insert(TABLE_NAME, null, contentValues);
        return insert;


    }
}
